var contesto = {
  "categorie" : "0",
  "tag" : "0",
  "stringFilter" : ""
}

var COD_CATEGORIA_MENU = 1;
var COD_CATEGORIA_TAGS = 2;
var COD_CATEGORIA_MENU_PAGAMENTI = "1_5";
var COD_FAM_RICARICHE = 1;
var COD_FAM_PIN = 2;
var COD_FAM_PRODOTTI_FINANZIARI = 3;
var COD_FAM_GIFT_CARDS = 4;
var COD_FAM_RICARICHE_SOLIDALI = 5;
var COD_FAM_ATTIVAZIONE_SIM = 6;
var COD_FAM_PRODOTTI_CAMERALI = 7;
var COD_FAM_RICARICA_WALLET = 8;
var COD_FAM_PAGAMENTO_VOCUHER_CONTO_GIOCO = 500005;
var ID_PROD_SNAI_PIN = 2;
var ID_PROD_PAYSAFE_PIN = 3;
var ID_PROD_MAV_RAV = 67;
var ID_PROD_BOLLETTINI_OLD = 7;
var ID_PROD_BOLLETTINI = 65;
var ID_PROD_CBILL = 23;
var ID_PROD_TRANSFERTO = 25;
var ID_PROD_POSTEPAY = 26;
var ID_PROD_PAYSAFE_DIRECT = -1; //53;
var ID_PROD_PAYSAFE_CASH = -2; //54;
var ID_PROD_SNAI_VOUCHER = -3; //55;
var ID_PROD_IPAGOO = -4; //56;
var ID_PROD_PAGOPA = 68;
var ID_PROD_BOLLOAUTO = 69;
var ID_PROD_BOLLETTINO_FRECCIA = 113;
var ID_PROD_ATTIVAZIONE_SIM_ILIAD = 110;
var ID_PROD_AMAZON_WALLET = 123;
var ID_PROD_PAGAMENTO_VOCUHER_CONTO_GIOCO = 500005;


var ID_PROD_RICERCA_PERSONA = 122;
var ID_PROD_RICERCA_AZIENDA = 114;
var ID_PROD_REPORT_PERSONA = 121;
var ID_PROD_REPORT_AZIENDA = 119;
var ID_PROD_VISURA_ORDINARIA = 115;
var ID_PROD_VISURA_STORICA = 116;
var ID_PROD_COMPANY_CARD_ORDINARIA = 117;
var ID_PROD_COMPANY_CARD_STORICA = 118;
var ID_PROD_REPORT_AZIENDA_CATASTO = 120;

var arrayOrMandatoryValidation = [
  ID_PROD_RICERCA_PERSONA,
  ID_PROD_RICERCA_AZIENDA,
  ID_PROD_COMPANY_CARD_ORDINARIA,
  ID_PROD_COMPANY_CARD_STORICA,
  ID_PROD_REPORT_AZIENDA,
  ID_PROD_REPORT_AZIENDA_CATASTO,
  ID_PROD_REPORT_PERSONA,
  ID_PROD_VISURA_ORDINARIA,
  ID_PROD_VISURA_STORICA];
//ID_PROD_RICERCA_PERSONA,ID_PROD_RICERCA_AZIENDA,ID_PROD_REPORT_PERSONA,ID_PROD_REPORT_AZIENDA,ID_PROD_VISURA_ORDINARIA,ID_PROD_VISURA_STORICA,ID_PROD_COMPANY_CARD_ORDINARIA,ID_PROD_COMPANY_CARD_STORICA,ID_PROD_REPORT_AZIENDA_CATASTO
var PRODOTTI_NO_TAGLI = [ID_PROD_TRANSFERTO, ID_PROD_POSTEPAY, ID_PROD_CBILL, ID_PROD_MAV_RAV, ID_PROD_PAYSAFE_CASH, ID_PROD_PAYSAFE_DIRECT, ID_PROD_SNAI_VOUCHER, ID_PROD_IPAGOO, ID_PROD_BOLLETTINI, ID_PROD_PAGOPA, ID_PROD_BOLLOAUTO,ID_PROD_BOLLETTINO_FRECCIA,ID_PROD_AMAZON_WALLET];
var PRODOTTI_LAST_VENDITA_OBJ = [ID_PROD_CBILL, ID_PROD_BOLLETTINI, ID_PROD_PAGOPA, ID_PROD_BOLLOAUTO, ID_PROD_ATTIVAZIONE_SIM_ILIAD, ID_PROD_BOLLETTINO_FRECCIA];

var MSG_ADEGUATA_VERIFICA_POSTEPAY = "<div id='adeguataVerificaText'> <strong class='evidenziato'>Hai eseguito le procedure di adeguata verifica?</strong> <br/>1.       Ho chiesto al Pagatore l’esibizione di un documento d’identità in corso di validità e non palesemente falso e/o contraffatto; <br/>2.       Ho chiesto al Pagatore l’esibizione del codice fiscale;<br/>3.       ho verificato:<br/><span class='indentato'/> a.       Che il Soggetto che esibisce il documento d’identità coincide con il titolare del documento stesso;<br/> <span class='indentato'/> b.       La corrispondenza dei dati contenuti nel documento di riconoscimento con quelli presenti nel codice fiscale.<br/>4.       Ho acquisito la copia del documento di identità e del codice fiscale;<br/> <strong class='evidenziato'> Se hai eseguito tutte le verifiche sopra elencate puoi completare l’operazione.<br/>Sono consapevole delle responsabilità (a mio carico e nei confronti di Snaitech S.p.A.) in caso di  erogazione del servizio in violazione delle procedure e delle disposizioni indicate dalla Società. </strong><br/></div>";
var maxFileSize = 500 * 1024; // 500k
var MSG_UPLOAD_POSTEPAY = "<strong>Per proseguire carica la copia fronte/retro del documento di identità e del codice fiscale dell’ordinante.</strong> <br/>Formati supportati <strong>jpg, png e pdf</strong>, con dimensioni massime di " + maxFileSize/1024 + "kB per ciascun file. <br/>L’erogazione del servizio in violazione delle procedure indicate causerà il blocco immediato del servizio stesso."
var lastVenditaObj = null;
var pathLoghi = "/sites/default/files/adm_pr_images/";
var tagliPaysafeDirect;

var regexLettereNumeri = new RegExp("^[a-zA-Z0-9]+$");
var regexNumeri = new RegExp("^[0-9]+$");
var regexCausaleBollettino =  new RegExp("^[a-zA-Z0-9\-_?()+ ,;'=*$%@\.:\/]*$");
var regexData = new RegExp("^[0-9\/]*$");
var regexDDMMAA = new RegExp("^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)([0-9][0-9])$");
var regexDDMMAAAA = new RegExp("^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)([1-2][0-9][0-9][0-9])$");
var codiceBollettinoChars = [];
var codiceBollettinoPressed = false;

var taglioColMd4Length = 18;
var taglioColMd6Length = 26;

var PREFISSO_PRODOTTO = "prodotto_";

var categorieGlobal = null;
var prodottiGlobal = null;
var brandGlobal = null;


var execProdottiRicaricaWallet = {}; //exec specifiche per i prodotti finanziari
execProdottiRicaricaWallet[PREFISSO_PRODOTTO + ID_PROD_AMAZON_WALLET] = "execRicaricaConto";


var execProdottiFinanziari = {}; //exec specifiche per i prodotti finanziari
execProdottiFinanziari[PREFISSO_PRODOTTO + ID_PROD_CBILL] = "execVenditaCbill";
execProdottiFinanziari[PREFISSO_PRODOTTO + ID_PROD_MAV_RAV] = "execVenditaMavRav";
execProdottiFinanziari[PREFISSO_PRODOTTO + ID_PROD_POSTEPAY] = "execRicaricaPostePay";
execProdottiFinanziari[PREFISSO_PRODOTTO + ID_PROD_PAYSAFE_DIRECT] = "execPagamentoPaymentCode";
execProdottiFinanziari[PREFISSO_PRODOTTO + ID_PROD_PAYSAFE_CASH] = "execPagamentoPaymentCode";
execProdottiFinanziari[PREFISSO_PRODOTTO + ID_PROD_SNAI_VOUCHER] = "execIncassoSnaiVoucher";
execProdottiFinanziari[PREFISSO_PRODOTTO + ID_PROD_IPAGOO] = "execPrelievoIpagoo";
execProdottiFinanziari[PREFISSO_PRODOTTO + ID_PROD_BOLLETTINI] = "execPagamentoBollettino";
execProdottiFinanziari[PREFISSO_PRODOTTO + ID_PROD_PAGOPA] = "execPagamentoPagoPa";
execProdottiFinanziari[PREFISSO_PRODOTTO + ID_PROD_BOLLOAUTO] = "execPagamentoBolloAuto";
execProdottiFinanziari[PREFISSO_PRODOTTO + ID_PROD_BOLLETTINO_FRECCIA] = "execPagamentoBollettinoFreccia";

var execProdottiCamerali = {}; //exec specifiche per i prodotti camerali
execProdottiCamerali[PREFISSO_PRODOTTO + ID_PROD_RICERCA_PERSONA] = "execRicercaPersona";
execProdottiCamerali[PREFISSO_PRODOTTO + ID_PROD_RICERCA_AZIENDA] = "execRicercaAzienda";
execProdottiCamerali[PREFISSO_PRODOTTO + ID_PROD_REPORT_PERSONA] = "execReportPersona";
execProdottiCamerali[PREFISSO_PRODOTTO + ID_PROD_REPORT_AZIENDA] = "execReportAzienda";
execProdottiCamerali[PREFISSO_PRODOTTO + ID_PROD_VISURA_ORDINARIA] = "execVisuraCamerale";
execProdottiCamerali[PREFISSO_PRODOTTO + ID_PROD_VISURA_STORICA] = "execVisuraCamerale";
execProdottiCamerali[PREFISSO_PRODOTTO + ID_PROD_COMPANY_CARD_ORDINARIA] = "execReportAzienda";
execProdottiCamerali[PREFISSO_PRODOTTO + ID_PROD_COMPANY_CARD_STORICA] = "execReportAzienda";
execProdottiCamerali[PREFISSO_PRODOTTO + ID_PROD_REPORT_AZIENDA_CATASTO] = "execReportAzienda";

var execAttivazioniSim = {}
execAttivazioniSim[PREFISSO_PRODOTTO + ID_PROD_ATTIVAZIONE_SIM_ILIAD] = "execAcquistoSim";

var annullaVenditaCustomActions = {}; //azioni custom in fase di annullamento vendita per prodotto
annullaVenditaCustomActions[PREFISSO_PRODOTTO + ID_PROD_BOLLETTINI] = annullaVenditaBollettini;
annullaVenditaCustomActions[PREFISSO_PRODOTTO + ID_PROD_BOLLETTINO_FRECCIA] = annullaVenditaBollettinoFreccia;
annullaVenditaCustomActions[PREFISSO_PRODOTTO + ID_PROD_MAV_RAV] = annullaVenditaMavRav;
annullaVenditaCustomActions[PREFISSO_PRODOTTO + ID_PROD_PAGOPA] = annullaVenditaPagoPa;
annullaVenditaCustomActions[PREFISSO_PRODOTTO + ID_PROD_BOLLOAUTO] = annullaVenditaBolloAuto;
annullaVenditaCustomActions[PREFISSO_PRODOTTO + ID_PROD_ATTIVAZIONE_SIM_ILIAD] = annullaVenditaSimIliad;

var isSearchFromVisuraCameraliOrd = false;
var isSearchFromVisuraCameraliSto = false;
var isSearchFromCompanyCardCameraliOrd = false;
var isSearchFromCompanyCardCameraliSto = false;
var PRODOTTI_CAMERALI_ANNULLA_VENDITA = {
  ID_PROD_RICERCA_PERSONA,
  ID_PROD_RICERCA_AZIENDA,
  ID_PROD_COMPANY_CARD_ORDINARIA,
  ID_PROD_COMPANY_CARD_STORICA,
  ID_PROD_REPORT_AZIENDA,
  ID_PROD_REPORT_AZIENDA_CATASTO,
  ID_PROD_REPORT_PERSONA,
  ID_PROD_VISURA_ORDINARIA,
  ID_PROD_VISURA_STORICA
};
for(var i=0; i < PRODOTTI_CAMERALI_ANNULLA_VENDITA.length; i++) annullaVenditaCustomActions[PREFISSO_PRODOTTO + PRODOTTI_CAMERALI_ANNULLA_VENDITA[i]] = annullaVenditaVisuraCamerali;

var isSepafinLogged = false;

// Mobile camera web socket
var mcWs; // la socket
var mcWsInRetry = false; //indica se siamo in fase di retry
var mcWsMaxRetryNumber = 5; // numero massimo di tentativi di retry
var mcWsReconnectCount = 0; // numero corrente di tentativi di retry
var mcWsReconnectionInterval = null; // indica l'interval impostato per i tentativi di reconnessione
var mcWsRetryInterval = 5000; // quanto attendere tra una retry e l'altra (impostato nel reconnection interval)

// Stat postepay (modalita' di acquisizione documenti)
var PP_MODALITA_FORM = 1;
var PP_MODALITA_MOBILE = 2;
var PP_MODALITA_WEBCAM = 3;

function venditaPaymat(){
  $("#section-content>.container").attr("id", "venditaPayMat");
  svuotaFrontMonitor();
  setBeforeUnload();
  getData();

//	catchWindowPrint();
}

function setBeforeUnload() {
  window.onbeforeunload = function (e) {
    svuotaFrontMonitor();
  }
}



function getData() {
  addBgFrame();
  customAjaxCall("/src/web/action/vendita/getData.php?action=prodotti", "prodotti", costruisciDomProdotti, null, null);
}

function costruisciDomProdotti(action, data) {
  logOnConsole("Prodotti");
  logOnConsole(data);
  prodottiGlobal = data.risultato;

  customAjaxCall("/src/web/action/vendita/getData.php?action=categorie-brand", "categorie-brand", costruisciDomCategorieBrand, null, null);
}

function costruisciDomCategorieBrand(action, data) {
  logOnConsole("Categorie e brand");
  logOnConsole(data);
  if(data.codice === 0) {
    categorieGlobal = data.dati.categorie;
    categorieGlobal.sort(compareCategorie);
    brandGlobal = data.dati.brand;
    sortBrand();
    costruisciCategorie();
    costruisciBrand();
  }
  removeBgFrame();
}


// Problemi di race condition su prodottiGlobal
//function getData(){
//	var actions = ["prodotti", "categorie-brand"];
//	for(var i=0;i<actions.length;i++){
//		var action = actions[i];
//		var reqUrl = "/src/web/action/vendita/getData.php?action="+action;
//		chiamaPhp(action, reqUrl);
//	}
//}
//function chiamaPhp(action, reqUrl){
//	customAjaxCall(reqUrl, action, costruisciDom, null, null);
//}
//function costruisciDom(action, data){
//	if(action == "prodotti") {
//		logOnConsole("Prodotti");
//		logOnConsole(data);
//		prodottiGlobal = data.risultato;
//	} else if(action == "categorie-brand") {
//		logOnConsole("Categorie e brand");
//		logOnConsole(data);
//		if(data.codice === 0) {
//			categorieGlobal = data.dati.categorie;
//			categorieGlobal.sort(compareCategorie);
//			brandGlobal = data.dati.brand;
//			sortBrand();
//			costruisciCategorie();
//			costruisciBrand();
//		}
//	}
//}

function sortBrand() {
  var sortedBrand = [];
  var addedBrandId = [];
  for(var i=0; i<categorieGlobal.length; i++) {
    if(!Array.isArray(categorieGlobal[i].brand)) categorieGlobal[i].brand = [categorieGlobal[i].brand];
    for(var j=0; j<categorieGlobal[i].brand.length; j++) {
      if(addedBrandId.indexOf(categorieGlobal[i].brand[j]) === -1) {
        var brand = getBrandByCodice(categorieGlobal[i].brand[j]);
        if(brand) {
          sortedBrand.push(brand);
          addedBrandId.push(brand.codiceBrand);
        }
      }
    }
  }
  brandGlobal = sortedBrand;
}

function compareCategorie(a,b) {
  if (a.priorita < b.priorita)
    return -1;
  if (a.priorita > b.priorita)
    return 1;
  return 0;
}

// Costruisce i due menu' a sinistra: categorie prodotto e tags
function costruisciCategorie() {
  if(categorieGlobal){
    categorieGlobal.splice(0, 0, {codice: 0, priorita: 0, descrizione:"Tutti i servizi", tipo: COD_CATEGORIA_MENU});
    categorieGlobal.splice(1, 0, {codice: 0, priorita: 0, descrizione:"Tutti", tipo: COD_CATEGORIA_TAGS});
    for(var i=0; i<categorieGlobal.length;i++){
      var action = categorieGlobal[i].tipo === COD_CATEGORIA_MENU ? "categorie" : "tag";
      var classeActive = categorieGlobal[i].codice === 0 ? "active" : "";
      var el = "<a class='pointer list-group-item "+classeActive+"' id='"+categorieGlobal[i].codice+"'>"+categorieGlobal[i].descrizione.charAt(0).toUpperCase() + categorieGlobal[i].descrizione.slice(1)+"</a>";
      $(".filtroPaymat#"+action).append(el);
    }
    abilitaAzioni($(".filtroPaymat#categorie"), true);
    abilitaAzioni($(".filtroPaymat#tag"), false);
  }
}

function getCategoriaByCodice(codice) {
  var categoria = null;
  for(var i=0; i<categorieGlobal.length;i++) {
    if(categorieGlobal[i].codice == codice) {
      categoria = categorieGlobal[i];
      break;
    }
  }
  if(!categoria) logOnConsole("[getCategoriaByCodice] Categoria " + codice + " non trovata");
  return categoria;
}

function getBrandByCodice(codice) {
  var brand = null;
  for(var i=0; i<brandGlobal.length; i++) {
    if(brandGlobal[i].codiceBrand == codice) {
      brand = brandGlobal[i];
      break;
    }
  }
  if(!brand) logOnConsole("[getBrandByCodice] Brand " + codice + " non trovato");
  return brand;
}

function abilitaAzioni(menu, setAzioniCustom){
  $(menu).find("a").click(function(){
    $(menu).find("a").removeClass("active");
    $(this).addClass("active");
    contesto[$(menu).attr("id")] = $(this).attr("id");

    if(setAzioniCustom && $(this).attr("id") != COD_CATEGORIA_MENU_PAGAMENTI) {
      $("#sepafin_login_panel").hide();
    }

    costruisciBrand();
    filterBrandByText();
    svuotaFrontMonitor();

    if(setAzioniCustom && $(this).attr("id") == COD_CATEGORIA_MENU_PAGAMENTI) {
      showSepafinLoginPanel();
    }
  })
}

function getTagli(idProdotto){
  var reqUrl = "/src/web/action/vendita/getData.php?action=tagli&idProdotto="+idProdotto;
  customAjaxCall(reqUrl, idProdotto, costruisciTagli, null, null);
}

function getTagliTransferTo(){
  var idProdotto = ID_PROD_TRANSFERTO;
  var el = $(".categoria-prodotto-item[id='prodotto_"+idProdotto+"']");
  if($(el).find("input[name='token']").val() != undefined && $(el).find("input[name='token']").val() != ""){
    $("#cercaTagliTransferTo").addClass("inCorso");
    $("#cercaTagliTransferTo").attr("onclick", "");
    var numeroTelefonico = $("#prodotto_25").find("input[name='cellulare']").val();
    var token = $("#prodotto_25").find("input[name='token']").val();
    var reqUrl = "/src/web/action/vendita/execInfoTagliRicariche.php?idProdotto="+idProdotto+"&token="+token+"&numeroTelefonico="+numeroTelefonico;

    customAjaxCall(reqUrl, idProdotto, costruisciTagliTransferTo, getTagliTransferToErroCallback, null);
  }
}
function getTagliTransferToErroCallback(){
  $("#cercaTagliTransferTo").removeClass("inCorso");
  $("#cercaTagliTransferTo").attr("onclick", "getTagliTransferTo()");
}


function costruisciTagliTransferTo(idProdotto, data){
  var el = $(".categoria-prodotto-item[id='prodotto_"+idProdotto+"']");
  $("#cercaTagliTransferTo").removeClass("inCorso");
  $("#cercaTagliTransferTo").attr("onclick", "getTagliTransferTo()");
  if(data.resultCode == 0){
    $(el).find(".dettaglio").empty();
    $(el).find(".dettaglio").append("<input name='taglio' type='hidden' required>");
    $(el).find(".dettaglio").append("<input name='importo' type='hidden' required>");
    $(el).find(".dettaglio").append("<input name='varReserve' type='hidden' required>");
    $(el).find(".dettaglio").append("<input name='varConfirm' type='hidden' required>");

    $(el).find(".dettaglio").append("<input name='operator' type='hidden'>");
    $(el).find(".dettaglio").append("<input name='destCurrency' type='hidden'>");
    $(el).find(".dettaglio").append("<input name='destPriceSent' type='hidden'>");
    $(el).find(".dettaglio").append("<input name='destPriceRec' type='hidden'>");
    var tagli = data.listaTagli.Taglio;
    tagli = ordinaTagli(tagli);
    var colmd = getTaglioColMdTransferTo(tagli);
    for(var i = 0; i< tagli.length; i++) {
      var taglio = tagli[i];
      var infos = taglio.Descrizione.split("|");
      var localCurrency = infos[0]; //valuta locale (euro)
      var localPriceEuro = infos[1]; //prezzo in euro
      var localPriceCent = infos[1] * 100; //prezzo in euro, convertito in centesimi
      var destCurrency = infos[2]; //valuta destinazione (es. IDR)
      var destPriceSent = infos[3]; //prezzo inviato in valuta destinazione
      var destPriceRec = infos[4]; //prezzo ricevuto in valuta destinazione
      var idProduct = infos[5]; //idProdotto
      var operator = infos.length > 6 ? infos[6] : "";
      $(el).find(".dettaglio").append("<div class='dettaglio-item col col-md-" + colmd + " col-sm-6' data-importo='"+localPriceCent+"'>" +
        "<span class='form-control btn btn-success taglio' onclick='onTaglioClick(this)' data-codice='"+taglio.codiceTaglio+"' data-var='"+idProduct+"' " +
        "data-dest-currency='" + destCurrency + "' data-dest-price-sent='" + destPriceSent + "' data-dest-price-rec='" + destPriceRec + "' data-operator='" + encodeURI(operator) + "'" + ">" +
        "<span class='descrizioneTaglio'>"+(localCurrency+" "+localPriceEuro).trim()+"</span>" +
        "<span class='valoreTaglio'>"+(destCurrency+" "+destPriceRec).trim()+"</span>" +
        "</span></div>");
    }
  }

}

function setTagliPaysafeDirect(data) {
  if(data && data.esito == 0 && data.risultato && data.risultato.esito == 0 && data.risultato.RS1) {
    tagliPaysafeDirect = {};
    for(var i=0; i<data.risultato.RS1.length; i++) {
      var taglio = data.risultato.RS1[i];
      tagliPaysafeDirect[taglio.eancode] = taglio.value;
    }
  }

}

function costruisciTagli(idProdotto, data){
  logOnConsole("Tagli - " + idProdotto);
  logOnConsole(data);
  if(data && data.resultCode === 0){
    var el = $(".categoria-prodotto-item[id='prodotto_"+idProdotto+"']");
    if($(el).find(".dettaglio").html() == ""){
      var tagli = data.tagli ? data.tagli : [];
      var isActive = tagli.length == 1; // seleziono automaticamente se il taglio e' uno
      // tagli = ordinaTagli(tagli);
      $(el).find(".dettaglio").append("<input name='taglio' type='hidden' required " + (isActive ? "value='" + tagli[0].codiceTaglio + "'" : "") + ">");
      $(el).find(".dettaglio").append("<input name='importo' type='hidden' required " + (isActive ? "value='" + tagli[0].importo + "'" : "") + ">");
      var colmd = getTaglioColMd(tagli);
      for(var i = 0; i< tagli.length; i++){
        var importo = tagli[i].importo;
        var importoIva = tagli[i].ImportoIva;
        var isIva = false;
        //logOnConsole("Importo prima - " + importo);
        //logOnConsole("importoIva - " + importoIva);
        var importoFormattedPreIva = fromCentsToEuro(importo);
        if(importoIva && parseInt(importoIva) > 0) {
          importo = parseInt(importo) + parseInt(importoIva);
          isIva = true;
        }
        //logOnConsole("Importo dopo - " + importo);
        //logOnConsole("descrizione prima - " + tagli[i].Descrizione);
        var importoFormatted = fromCentsToEuro(importo);
        if(!tagli[i].Descrizione) tagli[i].Descrizione = "";
        var descrizione = isIva ?
          tagli[i].Descrizione.replace(" " + importoFormattedPreIva, " " + DESCRIZIONE_SUFFISSO_IVA) :
          tagli[i].Descrizione.replace(" " + importoFormattedPreIva, "");
        tagli[i].Descrizione = descrizione;
        if(isIva) colmd = getTaglioColMd(tagli);
        //logOnConsole("descrizione dopo - " + tagli[i].Descrizione);
        //logOnConsole("descrizione - " + descrizione);

        $(el).find(".dettaglio").append("<div class='dettaglio-item col col-md-"+colmd +" col-sm-6' data-importo='"+ tagli[i].importo +"'><span class='form-control btn btn-success taglio" + (isActive ? " active" : "") + "' data-codice="+tagli[i].codiceTaglio+" onclick='onTaglioClick(this)'><span class='descrizioneTaglio'>"+descrizione+"</span><span class='valoreTaglio'>"+importoFormatted+"</span></span></div>");
      }
      if(isSmartSolution && isActive)	aggiornaFrontMonitor(el);
    } else if($(el).find(".btn.taglio").length == 1) {
      // se presente un solo taglio, viene selezionato automaticamente
      onTaglioClick($(el).find(".btn.taglio").eq(0));
      if(isSmartSolution)	aggiornaFrontMonitor(el);
    }
  }
}

function ordinaTagli(tagli){
  for(var i=0; i<tagli.length;i++){
    tagli.sort(compareTagli);
  }
  return tagli;
}

function compareTagli(a,b) {
  if (a.valoredb < b.valoredb)
    return -1;
  if (a.valoredb > b.valoredb)
    return 1;
  return 0;
}

function getTaglioColMd(tagli) {
  var colmd = 3; // 4 per riga
  for(var i = 0; i< tagli.length; i++) {
    if(colmd < 6 && tagli[i].Descrizione && tagli[i].Descrizione.length > taglioColMd6Length)
      colmd = 6; // 2 per riga
    if(colmd < 4 && tagli[i].Descrizione && tagli[i].Descrizione.length > taglioColMd4Length)
      colmd = 4; // 3 per riga
  }
  return colmd;
}

function getTaglioColMdTransferTo(tagli) {
  var colmd = 3; // 4 per riga
  for(var i = 0; i< tagli.length; i++) {
    var taglio = tagli[i];
    var infos = taglio.Descrizione.split("|");
    var localCurrency = infos[0].trim(); //valuta locale (euro)
    var localPriceEuro = infos[1].trim(); //prezzo in euro
    var destCurrency = infos[2].trim(); //valuta destinazione (es. IDR)
    var destPriceRec = infos[4].trim(); //prezzo ricevuto in valuta destinazione

    if(colmd < 6 && (localCurrency+" "+localPriceEuro).length + (destCurrency + " " + destPriceRec).length > taglioColMd6Length)
      colmd = 6; // 2 per riga
    if(colmd < 4 && (localCurrency+" "+localPriceEuro).length + (destCurrency + " " + destPriceRec).length > taglioColMd4Length)
      colmd = 4; // 3 per riga
  }
  return colmd;
}

function onTaglioClick(btn){
  $(btn).parents(".dettaglio").find(".dettaglio-item .btn").removeClass("active");
  $(btn).addClass("active");
  $(btn).parents(".dettaglio").find("input[name=taglio]").val($(btn).attr("data-codice"));
  $(btn).parents(".dettaglio").find("input[name=importo]").val($(btn).parents(".dettaglio-item").attr("data-importo"));
  $(btn).parents(".dettaglio").removeClass("error")
  if($(btn).parents("form").attr("id") == "venditaForm_"+ID_PROD_TRANSFERTO){
    $(btn).parents(".dettaglio").find("input[name=varReserve]").val($(btn).attr("data-var"));
    $(btn).parents(".dettaglio").find("input[name=varConfirm]").val($(btn).attr("data-var"));
    $(btn).parents(".dettaglio").find("input[name=destCurrency]").val($(btn).attr("data-dest-currency"));
    $(btn).parents(".dettaglio").find("input[name=destPriceSent]").val($(btn).attr("data-dest-price-sent"));
    $(btn).parents(".dettaglio").find("input[name=destPriceRec]").val($(btn).attr("data-dest-price-rec"));
    $(btn).parents(".dettaglio").find("input[name=operator]").val($(btn).attr("data-operator"));
  }

  aggiornaFrontMonitor($(btn).closest("div.categoria-prodotto-item"));
}
function setStringFilter(){
  contesto.stringFilter = $("#cercaProdotti").val().toLowerCase();
  filterBrandByText();
  fireSearchFromVisuraOrCompanyCardCamerali(false);
}
function clearInput(){
  $("#cercaProdotti").val("");
  setStringFilter();
  fireSearchFromVisuraOrCompanyCardCamerali(true);
}

function filterBrandByText(){
  var matchedItems = [];
  $(".categoria-prodotto-item").each(function(){
    var string = $(this).find(".searchTarget").html().toLowerCase();
    if((contesto.stringFilter == "" || string.indexOf(contesto.stringFilter) > -1)){
      matchedItems.push(this);
    }
  })
  $(".categoria-prodotto-item").hide();
  $(matchedItems).show();
  if(matchedItems.length == 0 && $("#user_msg").length == 0){
    var msg = "<div id='user_msg' class='alert alert-warning'>Spiacenti, non è presente nessun prodotto corrispondente alla ricerca</div>"
    $("#vendita-container").append(msg);
  }else{
    $("#user_msg").remove();
  }

  scrollToElement("body", 0, 500);
}

function getSortedBrandList() {
  var brandList;
  // Dipende dal contesto di selezione
  if(contesto.categorie == "0" && contesto.tag == "0") {
    brandList = brandGlobal; //sorted in fase di load
  } else if(contesto.categorie != "0" && contesto.tag != "0") {
    brandList = [];
    var categoriaMenu = getCategoriaByCodice(contesto.categorie);
    var categoriaTag = getCategoriaByCodice(contesto.tag);
    var addedBrandIds = [];
    // nel caso di doppia selezione ha la precedenza l'ordine del menu rispetto ai tags
    for(var i=0; i<categoriaMenu.brand.length; i++) {
      var idBrand = categoriaMenu.brand[i];
      if(addedBrandIds.indexOf(idBrand) === -1 && categoriaTag.brand.indexOf(idBrand) != -1) {
        var brand = getBrandByCodice(idBrand);
        if(brand) {
          brandList.push(brand);
          addedBrandIds.push(idBrand);
        }
      }
    }
  } else {
    var categoria = getCategoriaByCodice(contesto.categorie != "0" ? contesto.categorie : contesto.tag);
    brandList = getCategoriaSortedBrand(categoria);
  }

  return brandList;
}

function getCategoriaSortedBrand(categoria) {
  var sortedBrand = null;
  if(categoria) {
    if(!categoria.sortedBrand) {
      sortedBrand = [];
      if(!Array.isArray(categoria.brand)) categoria.brand = [categoria.brand];
      for(var i=0; i<categoria.brand.length; i++) {
        var brand = getBrandByCodice(categoria.brand[i]);
        if(brand) sortedBrand.push(brand);
      }
      categoria.sortedBrand = sortedBrand;
    }
    sortedBrand = categoria.sortedBrand
  }
  return sortedBrand;
}

function costruisciBrand() {
  var products = {esito: 0, risultato: {esito: 0, RS1: [] }};

  var brandList = getSortedBrandList();

  for(var i=0; i<brandList.length; i++) {
    var brand = brandList[i];

    var descrizioneFamiglia = "";
    var categorieBrand = brand.categorie;
    if(!Array.isArray(categorieBrand)) categorieBrand = [categorieBrand];

    // Recupero descrizione famiglia (da mostrare nel box)
    for(var j=0; j<categorieGlobal.length; j++) {
      if(categorieBrand.indexOf(categorieGlobal[j].codice) != -1 && categorieGlobal[j].tipo == COD_CATEGORIA_MENU) {
        descrizioneFamiglia = categorieGlobal[j].descrizione.charAt(0).toUpperCase() + categorieGlobal[j].descrizione.slice(1);
        break;
      }
    }

    // Recupero codice famiglia (per logica) e company (per l'immagine del logo)
    var codFamiglia = -1;
    var company = brand.descrizione;
    for (var j=0; j<prodottiGlobal.RS1.length; j++) {
      if(brand.codiceBrand === prodottiGlobal.RS1[j].CodProdoto) {
        codFamiglia = prodottiGlobal.RS1[j].CodFamiglia;
        company = prodottiGlobal.RS1[j].Company
        break;
      }
    }

    products.risultato.RS1.push({CodProdoto: brand.codiceBrand, DescrizioneProdotto: brand.descrizione, Company: company, DescrizioneFamiglia: descrizioneFamiglia, CodFamiglia: codFamiglia, categorie: categorieBrand.join(";") });

  }

  //logOnConsole(products);
  costruisciProdotti(products);
}

function costruisciProdotti(data){
  fireSearchFromVisuraOrCompanyCardCamerali(false);
  $("#vendita-container").html("");

  var isPanelLoginSepafinBuilt = false;

  var items = data.risultato.RS1;

  var animatedClass = "animated";
  if(browserAgent == "isIE"){
    animatedClass = "";
  }

  var html ="";

  var isSubmitted = "<input id='venditaPaymatInCorso' type='hidden' value='0'>";
  $("body").append(isSubmitted);

  for (var i=0;i<items.length;i++){
    var prodotto = items[i];

    var idProdotto = prodotto.CodProdoto;
    var descProdotto = prodotto.DescrizioneProdotto;

    var codiceFamiglia = prodotto.CodFamiglia;
    var famiglia = prodotto.DescrizioneFamiglia;

    var company = prodotto.Company;
    var categorieBrand = prodotto.categorie;


    var ricDom = "";

    // se si sta per costruire il primo prodotto dei pagamenti, viene anteposto il pannello di login sepafin nascosto
    if(!isPanelLoginSepafinBuilt && codiceFamiglia == COD_FAM_PRODOTTI_FINANZIARI) {
      ricDom += "<div id='sepafin_login_panel' data-index='0' class='"+animatedClass+" zoomIn text-center col-xs-12 active' style='display:none'>";
      ricDom += "<div class='panel panel-default content pointer'>";
      ricDom += "<div class='panel-heading'></div>";
      ricDom += "<div class='panel-body'>";
      //ricDom += "<form id='sepafin_login_form' class='venditaForm' autocomplete='off' action='/src/web/action/vendita/" + ".php'>";
      ricDom += "<div class='formProdotto row'>";
      //ricDom += inserisciToken();
      ricDom += buildFormGroup(12, "<div>Per utilizzare tutti i servizi di pagamento e ricariche devi autenticarti con le credenziali Yappay/Sepafin</div>");
      ricDom += buildFormGroup(12, "<div class='btn btn-primary' onClick='openSepafinAuth()'>Autentica</div>");
      ricDom += "</div>";
      //ricDom += "</form>";
      ricDom += "</div>";
      ricDom += "</div>";
      ricDom += "</div>";

      isPanelLoginSepafinBuilt = true;
    }

    var disabledBoxClass = codiceFamiglia == COD_FAM_PRODOTTI_FINANZIARI && !isSepafinLogged ? " disabled-box" : "";

    ricDom += "<div id='prodotto_"+idProdotto+"' data-index='"+i+"' class='"+animatedClass+" zoomIn categoria-prodotto-item col col-xs-6 col-sm-3" + disabledBoxClass + "' data-codiceFamiglia='"+codiceFamiglia+"' data-categorieBrand='" + categorieBrand + "'>";
    ricDom+= "<div class='panel panel-default content pointer'>";


//			var logosrc = company.replace(/\s/g,'').replace(/\//g,'').toLowerCase();
    var logosrc = normalizeLogoName(company);

    ricDom += "<div class='panel-heading'><img src='"+pathLoghi+logosrc+".png'/></div>";

    ricDom += "<div class='panel-body'>";

    var exec = getExec(codiceFamiglia, idProdotto);

    ricDom+="<form id='venditaForm_"+idProdotto+"' class='venditaForm' autocomplete='off' action='/src/web/action/vendita/" + exec + ".php'>";
    ricDom += "<input name='idProdotto' type='hidden' required value='"+ idProdotto +"'>";
    ricDom += "<input name='requestId' type='hidden' required>";
    ricDom += "<span class='searchTarget'>"+descProdotto+"</span>";
    ricDom += "<div class='descrizioneProdotto'>"+famiglia+"</div>";

    ricDom += "<div class='formProdotto row'>";

    //WORKAROUND BUG CHROME AUTOCOMPLETE OFF
    ricDom += '<input type="text" style="display:none" />';
    ricDom += '<input type="password" style="display:none">';
    //

    var notifica = false;
    if(codiceFamiglia == COD_FAM_RICARICHE){
      if(idProdotto == ID_PROD_TRANSFERTO){
        // transferto
        ricDom += inserisciToken();
        if(isSmartSolution) // numero di telefono transferto su front monitor
          ricDom += "<div class='col col-md-4 form-group cellulare'><div class='input-group'><input type='number' onblur='getTagliTransferTo()' name='cellulare' class='form-control numero' required placeholder='Cellulare' " + buildShowFmHiddenFieldKeyEvents("cellulare", idProdotto) + "/>" + buildShowFmHiddenFieldButton("cellulare", idProdotto) + "<span class='input-group-addon' id='cercaTagliTransferTo' onclick='getTagliTransferTo()'><span class='glyphicon glyphicon-search'></span></span></div></div>";
        else // numero di telefono transferto senza nulla in aggiunta
          ricDom += "<div class='col col-md-4 form-group cellulare'><div class='input-group'><input type='number' onblur='getTagliTransferTo()' name='cellulare' class='form-control numero' required placeholder='Cellulare'/><span class='input-group-addon' id='cercaTagliTransferTo' onclick='getTagliTransferTo()'><span class='glyphicon glyphicon-search'></span></span></div></div>";
      } else { // tutte le ricariche
        ricDom += inserisciToken();
        if(isSmartSolution) // numero di telefono con proiezione su front monitor
          ricDom += "<div class='col col-md-4 form-group cellulare'><div class='input-group'><input name='cellulare' " + buildShowFmHiddenFieldKeyEvents("cellulare", idProdotto) + " name='cellulare' type='number' class='form-control numero' required placeholder='Cellulare'/>" + buildShowFmHiddenFieldButton("cellulare", idProdotto) + "</div></div>";
        else // numero di telefono senza nulla in aggiunta
          ricDom += "<div class='col col-md-4 form-group cellulare'><input name='cellulare' onkeyup='checkIfNumber(event)' name='cellulare' type='number' class='form-control numero' required placeholder='Cellulare'/></div>";
      }
    }else if(codiceFamiglia == COD_FAM_PIN || codiceFamiglia == COD_FAM_GIFT_CARDS || codiceFamiglia == COD_FAM_RICARICHE_SOLIDALI){
      ricDom += inserisciToken();
    }else{

      if(idProdotto == ID_PROD_RICERCA_PERSONA) {
        ricDom += inserisciToken();
        //ricDom += buildFormGroup(12, "<div>Dati persona da ricercare</div>");
        ricDom += buildFormGroup(4, "<input name='nome' class='form-control mandatoryGroup' placeholder='Nome' />");
        ricDom += buildFormGroup(4, "<input name='cognome' class='form-control mandatoryGroup' placeholder='Cognome' />");
        ricDom += buildFormGroup(4, "<div class='input-group'><input name='cf' class='form-control mandatoryGroup' placeholder='Codice Fiscale della persona fisica' onblur='this.value=this.value.toUpperCase()' />" + getSpanInfoCodiceFiscale() + "</div>");
        ricDom += buildFormGroup(4,"<input name='dataNascita' class='form-control mandatoryGroup validateData' placeholder='Data di nascita' />");
        ricDom += buildFormGroup(4, "<input name='cittaNascita' class='form-control mandatoryGroup' placeholder='Citt&agrave; di nascita' />");
        ricDom += buildFormGroup(4, "<input name='provinciaNascita' class='form-control mandatoryGroup' placeholder='Provincia di nascita' />");
        ricDom += buildFormGroup(4, "<select name='sesso' class='form-control mandatoryGroup' placeholder='Sesso'><option value='' selected>--Sesso--</option><option value='M'>Maschio</option><option value='F'>Femmina</option></select>");
        ricDom += buildFormGroup(4, "<div class='input-group'><input name='email' class='form-control' placeholder='Email destinatario' /> " + getSpanEmailDestinatario() + "</div>");
        ricDom += "<div class=\"col col-xs-12 form-group\">";
        ricDom += "<div style=\"text-align:left; !important\" class=\"col col-xs-12\">&Eacute; la ricerca anagrafica tradizionale, disponibile per persone fisiche.</div>";
        ricDom += "<div style=\"text-align:left; !important\" class=\"col col-xs-12\">Per effettuare correttamente la ricerca &egrave; necessario compilare i dati almeno seguendo una delle combinazioni sotto elencate: </div>";
        ricDom += "<ul style='text-align:left;'>";
        ricDom += "<li>1. Nome e Cognome</li>";
        ricDom += "<li>2. Codice Fiscale della persona fisica</li>";
        ricDom += "</ul>";
        ricDom +=  "<div style=\"text-align:left; !important\" class=\"col col-xs-12\">eventuali ulteriori campi opzionali inseriti consentiranno il restringimento del numero dei risultati prodotti dalla ricerca.</div>";
        ricDom += "<br/>";
        ricDom += "<br/>";
        ricDom += "<div style=\"text-align:left; !important\" class=\"col col-xs-12\"><strong>Servizio soggetto ad IVA per il quale &egrave; pertanto previsto l'obbligo di fatturazione, laddove richiesta, o di emissione di scontrino o ricevuta fiscale</strong></div>";
        ricDom += "</div>";
      }

      else if(idProdotto == ID_PROD_RICERCA_AZIENDA) {
        ricDom += inserisciToken();
        //ricDom += buildFormGroup(12, "<div>Dati Azienda da ricercare</div>");
        ricDom += buildFormGroup(4, "<input name='denominazione' class='form-control mandatoryGroup' placeholder='Denominazione'/>");
        ricDom += buildFormGroup(4, "<div class='input-group'><input name='cf' class='form-control mandatoryGroup' placeholder='Codice fiscale' onblur='this.value=this.value.toUpperCase()' />" + getSpanInfoCodiceFiscale() + "</div>");
        ricDom += buildFormGroup(4, "<input name='partitaIva' class='form-control mandatoryGroup' placeholder='Partita IVA' />");
        ricDom += buildFormGroup(4, "<input name='codiceRea' class='form-control mandatoryGroup' placeholder='Numero Rea' onkeypress='onlyNumbers(event)' onkeyup='onlyNumbers(event)' onkeydown='onlyNumbers(event)' onpaste='onlyNumbers(event)' />");
        ricDom += buildFormGroup(4, "<input name='provinciaCciaa' class='form-control mandatoryGroup' placeholder='Provincia Cciaa' onblur='this.value=this.value.toUpperCase()'  />");
        //ricDom += buildFormGroup(4, "<input name='numeroCrif' class='form-control mandatoryGroup' placeholder='Numero Crif (Centrale Rischi di Intermediazione finanziaria)' />");
        ricDom += buildFormGroup(4, "<input name='formaGiuridica' class='form-control mandatoryGroup' placeholder='Tipo forma giuridica' />");
        ricDom += buildFormGroup(4, "<input name='indirizzo' class='form-control mandatoryGroup' placeholder='Indirizzo' />");
        ricDom += buildFormGroup(4, "<input name='nazione' class='form-control mandatoryGroup' placeholder='Nazione' />");
        ricDom += buildFormGroup(4, "<input name='provincia' class='form-control mandatoryGroup' placeholder='Provincia' />");
        ricDom += buildFormGroup(4, "<input name='citta' class='form-control mandatoryGroup' placeholder='Citt&agrave;' />");
        ricDom += buildFormGroup(4, "<input name='cap' class='form-control mandatoryGroup' placeholder='CAP' />");
        ricDom += buildFormGroup(4, "<div class='input-group'><input name='email' class='form-control' placeholder='Email destinatario' /> " + getSpanEmailDestinatario() + "</div>");

        ricDom += "<div class=\"col col-xs-12 form-group\">";
        ricDom += "<div style=\"text-align:left; !important\" class=\"col col-xs-12\"><b>&Eacute; la ricerca anagrafica tradizionale, disponibile per persone giuridiche iscritte nel Registro Imprese.</b></br>Per effettuare correttamente la ricerca &egrave; necessario compilare i dati almeno seguendo una delle combinazioni sotto elencate: </div>";
        ricDom += "<ul style='text-align:left;'>";
        ricDom += "<li>1. Denominazione dell'azienda</li>";
        ricDom += "<li>2. Partita Iva Azienda o Codice Fiscale</li>";
        ricDom += "<li>3. Codice Rea Impresa e sigla della provincia di riferimento (Cciaa) </li>";
        ricDom += "</ul>";
        ricDom +=  "<div style=\"text-align:left; !important\" class=\"col col-xs-12\">eventuali ulteriori campi opzionali inseriti consentiranno il restringimento del numero dei risultati prodotti dalla ricerca.</div>";
        ricDom += "<br/>";
        ricDom += "<br/>";
        ricDom += "<div style=\"text-align:left; !important\" class=\"col col-xs-12\"><strong>Servizio soggetto ad IVA per il quale &egrave; pertanto previsto l'obbligo di fatturazione, laddove richiesta, o di emissione di scontrino o ricevuta fiscale</strong></div>";
        ricDom += "</div>";
      }

      else if(idProdotto == ID_PROD_REPORT_PERSONA) {

        var testoSpiegazione = "il Report Persona contiene informazioni sulle persone fisiche che ricoprono cariche sociali in imprese e consente di verificarne l'attivit&agrave; e la posizione al momento della richiesta ed i dati storici. Permette l'identificazione anagrafica delle imprese in cui il soggetto ha carica attualmente e in passato.";

        ricDom += inserisciToken();
        //ricDom += buildFormGroup(12, "<div>Dati report persona</div>");
        ricDom += buildFormGroup(4, "<div class='input-group'><input name='cf' class='form-control mandatoryGroup' placeholder='Codice fiscale della persona fisica' onblur='this.value=this.value.toUpperCase()' />" + getSpanInfoCodiceFiscale() + "</div>");
        ricDom += buildFormGroup(4, "<input name='nome' class='form-control mandatoryGroup' placeholder='Nome' />");
        ricDom += buildFormGroup(4, "<input name='cognome' class='form-control mandatoryGroup' placeholder='Cognome' />");
        ricDom += buildFormGroup(4,"<input name='dataNascita' class='form-control mandatoryGroup validateData' placeholder='Data di nascita' />");
        ricDom += buildFormGroup(4, "<input name='luogoNascita' class='form-control mandatoryGroup' placeholder='Luogo di nascita' />");
        ricDom += buildFormGroup(4, "<select name='sesso' class='form-control mandatoryGroup' placeholder='Sesso'><option value='' selected>--Sesso--</option><option value='M'>Maschio</option><option value='F'>Femmina</option></select>");
        ricDom += buildFormGroup(4, "<input name='provinciaNascita' class='form-control mandatoryGroup' placeholder='Provincia di nascita' />");
        ricDom += buildFormGroup(4, "<div class='input-group'><input name='email' class='form-control' placeholder='Email destinatario' /> " + getSpanEmailDestinatario() + "</div>");

        ricDom += "<div class=\"col col-xs-12 form-group\">";
        ricDom += "<div class=\"col col-xs-12\"></div>";
        ricDom += "</div>";
        ricDom += "<div class=\"col col-xs-12\">";
        ricDom += "<div style=\"text-align:left; !important\" class=\"col col-xs-12\">" + testoSpiegazione + "</div>";
        ricDom += "<div style=\"text-align:left; !important\" class=\"col col-xs-12\">Per effettuare correttamente l'ottenimento del report &egrave; necessario compilare i dati almeno seguendo una delle combinazioni sotto elencate: </div>";
        ricDom += "<ul style='text-align:left;'>";
        ricDom += "<li>1. Codice Fiscale della persona fisica</li>";
        ricDom += "<li>2. Nome, Cognome, Data di Nascita, Luogo di nascita, Provincia di nascita e Sesso</li>";
        ricDom += "</ul>";
        ricDom +=  "<div style=\"text-align:left; !important\" class=\"col col-xs-12\">eventuali ulteriori campi opzionali inseriti consentiranno maggiore accuratezza per la ricerca.</div>";
        ricDom += "<br/>";
        ricDom += "<br/>";
        ricDom += "<div style=\"text-align:left; !important\" class=\"col col-xs-12\"><strong>Servizio soggetto ad IVA per il quale &egrave; pertanto previsto l'obbligo di fatturazione, laddove richiesta, o di emissione di scontrino o ricevuta fiscale</strong></div>";
        ricDom += "</div>";
      }

      else if(idProdotto == ID_PROD_REPORT_AZIENDA || idProdotto == ID_PROD_COMPANY_CARD_ORDINARIA || idProdotto == ID_PROD_COMPANY_CARD_STORICA || idProdotto == ID_PROD_REPORT_AZIENDA_CATASTO) {

        var testoSpiegazione = "";
        if(idProdotto == ID_PROD_COMPANY_CARD_ORDINARIA) {
          testoSpiegazione = "La Company Card aggrega informazioni provenienti da Visura Ordinaria, Elenco Soci, Sintesi di bilancio ove appropriato. La codifica dell'attivit&agrave; &egrave; completata dalla presenza del SAE/RAE. Nel dettaglio, la company card di una Societ&agrave; di capitali e di una Societ&agrave; di persone contiene i seguenti blocchi informativi.";
        }
        else if(idProdotto == ID_PROD_COMPANY_CARD_STORICA) {
          testoSpiegazione = "La Company Card Storica aggrega le informazioni provenienti da Visura Storica, Elenco Soci, Sintesi di bilancio ove appropriato. La codifica dell'attivit&agrave; &egrave; completata dalla presenza del SAE/RAE.";
        }
        else if(idProdotto == ID_PROD_REPORT_AZIENDA) {
          testoSpiegazione = "Il Report Azienda, oltre ad includere i dati della Company card, si caratterizza per la presenza di: dati ufficiali di sintesi dell'azienda; eventi negativi su azienda, esponenti, societ&agrave; collegate, soci e partecipate; eventi legali; Esponenti e consiglio di amministrazione; Soci; Legami Societari e Partecipazioni attuali; Attivit&agrave; aziendale e unit&agrave; locali; 2 Annualit&agrave; di Bilancio.";
        }
        else if(idProdotto == ID_PROD_REPORT_AZIENDA_CATASTO) {
          testoSpiegazione = "Contiene le stesse informazioni del Report Azienda con l’aggiunta dei dati relativi alle propriet&agrave; immobiliari come disponibili al Catasto Nazionale. Questo report contiene la Visura Catastale per tutte le propriet&agrave; individuate per il soggetto.";
        }

        var eventoCallRicerca = "";
        if(idProdotto == ID_PROD_COMPANY_CARD_ORDINARIA || idProdotto == ID_PROD_COMPANY_CARD_STORICA) {
          eventoCallRicerca = idProdotto == ID_PROD_COMPANY_CARD_ORDINARIA ?
            "onclick='event.stopPropagation();goToRicercaFromVisuraOrCompanyCard(" + ID_PROD_COMPANY_CARD_ORDINARIA + ")'"
            : "onclick='event.stopPropagation();goToRicercaFromVisuraOrCompanyCard(" + ID_PROD_COMPANY_CARD_STORICA + ")'";
          eventoCallRicerca += " onkeypress='return false' onkeyup='return false' onkeydown='return false' onpaste='return false' ";
        }

        ricDom += inserisciToken();
        //ricDom += buildFormGroup(12, "<div>Dati report Azienda</div>");
        ricDom += buildFormGroup(4, "<input name='cciaa' class='form-control mandatoryGroup' placeholder='Provincia Cciaa' onblur='this.value=this.value.toUpperCase()' " + eventoCallRicerca + "/>");
        if(idProdotto == ID_PROD_COMPANY_CARD_ORDINARIA || idProdotto == ID_PROD_COMPANY_CARD_STORICA) {
          ricDom += buildFormGroup(4, "<input name='nrea' class='form-control mandatoryGroup' placeholder='Numero Rea' " + eventoCallRicerca + "/>");
        } else {
          ricDom += buildFormGroup(4, "<input name='nrea' class='form-control mandatoryGroup' placeholder='Numero Rea' onkeypress='limitaNumeriEnableDefaultChars(event)' onkeyup='onlyNumbers(event)' onkeydown='onlyNumbers(event)' onpaste='onlyNumbers(event)' />");
        }

        if(idProdotto == ID_PROD_COMPANY_CARD_ORDINARIA || idProdotto == ID_PROD_COMPANY_CARD_STORICA) {
          ricDom += buildFormGroup(4, "<div class='input-group'><input name='cf' class='form-control mandatoryGroup' placeholder='Codice Fiscale' " + eventoCallRicerca + "/>" + getSpanInfoCodiceFiscale() + "</div>");
        } else {
          ricDom += buildFormGroup(4, "<div class='input-group'><input name='cf' class='form-control mandatoryGroup' placeholder='Codice Fiscale o Partita Iva' onblur='this.value=this.value.toUpperCase()' />" + getSpanInfoCodiceFiscale() + "</div>");
        }

        //ricDom += buildFormGroup(4, "<input name='numeroCrif' class='form-control mandatoryGroup' placeholder='Numero Crif (Centrale Rischi di Intermediazione finanziaria)' />");
        ricDom += buildFormGroup(4, "<div class='input-group'><input name='email' class='form-control' placeholder='Email destinatario' /> " + getSpanEmailDestinatario() + "</div>");

        //Company card = 0
        //History Company Card = 1
        //Report Snaitech = 2
        //Report Snaitech Estate = 3
        if(idProdotto == ID_PROD_REPORT_AZIENDA) {
          //ricDom += buildFormGroup(6, "<select name='tipoReport' class='form-control' required><option selected>tipo di report che si vuole chiedere</option><option value='0'>Company card</option><option value='1'>History Company Card</option><option value='2'>Report Snaitech</option><option value='3'>Report Snaitech Estate</option></select>");
          ricDom += buildFormGroup(4, "<input name='tipoReport' type='hidden' value='2'/>");
        } else if(idProdotto == ID_PROD_COMPANY_CARD_ORDINARIA) {
          ricDom += buildFormGroup(4, "<input name='tipoReport' type='hidden' value='0'/>");
        } else if(idProdotto == ID_PROD_COMPANY_CARD_STORICA) {
          ricDom += buildFormGroup(4, "<input name='tipoReport' type='hidden' value='1'/>");
        } else if(idProdotto == ID_PROD_REPORT_AZIENDA_CATASTO) {
          ricDom += buildFormGroup(4, "<input name='tipoReport' type='hidden' value='3'/>");
        }

        ricDom += "<div class=\"col col-xs-12 form-group\">";
        if(idProdotto == ID_PROD_COMPANY_CARD_ORDINARIA || idProdotto == ID_PROD_COMPANY_CARD_STORICA) {
          ricDom += "<div style=\"text-align:left; !important\" class=\"col col-xs-12\"><b>" + testoSpiegazione +"</b> <br> Per procedere con l’acquisto del prodotto &egrave; necessario effettuare la <a href='javascript:void(0)' onclick='confermaGoToRicerca(" + idProdotto + ")' style='text-color:'#f76f00;'>Ricerca Impresa</a></div>";
        }
        else {
          ricDom += "<div style=\"text-align:left; !important\" class=\"col col-xs-12\"><b>" + testoSpiegazione +"</b> <br> Per effettuare correttamente l'acquisto del prodotto &egrave; necessario compilare i dati con una delle combinazioni sotto elencate: </div>";
          ricDom += "<ul style='text-align:left;'>";
          ricDom += "<li>1. Partita Iva Azienda o Codice Fiscale</li>";
          ricDom += "<li>2. Codice Rea Impresa e sigla della provincia di riferimento (Cciaa) </li>";
          ricDom += "</ul>";
          ricDom +=  "<div style=\"text-align:left; !important\" class=\"col col-xs-12\">eventuali ulteriori campi opzionali inseriti consentiranno il restringimento del numero dei risultati prodotti dalla ricerca.</div>";
        }
        ricDom += "<br/>";
        ricDom += "<br/>";
        ricDom += "<div style=\"text-align:left; !important\" class=\"col col-xs-12\"><strong>Servizio soggetto ad IVA per il quale &egrave; pertanto previsto l'obbligo di fatturazione, laddove richiesta, o di emissione di scontrino o ricevuta fiscale</strong></div>";
        ricDom += "</div>";
      }

      else if(idProdotto == ID_PROD_VISURA_ORDINARIA) {

        var eventoCallRicerca = "onclick='event.stopPropagation();goToRicercaFromVisuraOrCompanyCard(" + ID_PROD_VISURA_ORDINARIA + ")'";

        ricDom += inserisciToken();
        //ricDom += buildFormGroup(6, "<input name='var' class='form-control' placeholder='Var' />");
        ricDom += buildFormGroup(4, "<input name='cciaa' class='form-control mandatoryGroup' placeholder='Provincia Cciaa' " + eventoCallRicerca + " onkeypress='return false' onkeyup='return false' onkeydown='return false' onpaste='return false' />");
        ricDom += buildFormGroup(4, "<input name='nrea' class='form-control mandatoryGroup' placeholder='Numero Rea' " + eventoCallRicerca + " onkeypress='return false' onkeyup='return false' onkeydown='return false' onpaste='return false' />");
        ricDom += buildFormGroup(4, "<div class='input-group'><input name='codiceFiscale' class='form-control mandatoryGroup' placeholder='Codice fiscale' " + eventoCallRicerca + " onkeypress='return false' onkeyup='return false' onkeydown='return false' onpaste='return false' />" + getSpanInfoCodiceFiscale() + "</div>");
        ricDom += buildFormGroup(4, "<div class='input-group'><input name='email' class='form-control' placeholder='Email destinatario' /> " + getSpanEmailDestinatario() + "</div>");
        ricDom += buildFormGroup(4, "<input name='tipoVisura' type='hidden' value='0'/>");
        ricDom += "<div class=\"col col-xs-12 form-group\">";
        ricDom += "<div style=\"text-align:left; !important\" class=\"col col-xs-12\"><b>La Visura Ordinaria contiene le informazioni legali dell'impresa e le informazioni presenti nell'archivio del Registro delle Imprese e nel REA (Repertorio Economico Amministrativo).</b> <br>Per procedere con l’acquisto del prodotto &egrave; necessario effettuare la <a href='javascript:void(0)' onclick='confermaGoToRicerca(" + idProdotto + ")' style='text-color:'#f76f00;'>Ricerca Impresa</a></div>";
        ricDom += "<br/>";
        ricDom += "<br/>";
        ricDom += "<div style=\"text-align:left; !important\" class=\"col col-xs-12\"><strong>Servizio soggetto ad IVA per il quale &egrave; pertanto previsto l'obbligo di fatturazione, laddove richiesta, o di emissione di scontrino o ricevuta fiscale</strong></div>";
        ricDom += "</div>";
      }

      else if(idProdotto == ID_PROD_VISURA_STORICA) {

        var eventoCallRicerca = "onclick='event.stopPropagation();goToRicercaFromVisuraOrCompanyCard(" + ID_PROD_VISURA_STORICA + ")'";

        ricDom += inserisciToken();
        //ricDom += buildFormGroup(6, "<input name='var' class='form-control' placeholder='Var' />");
        ricDom += buildFormGroup(4, "<input name='cciaa' class='form-control mandatoryGroup' placeholder='Provincia Cciaa' " + eventoCallRicerca + " onkeypress='return false' onkeyup='return false' onkeydown='return false' onpaste='return false' />");
        ricDom += buildFormGroup(4, "<input name='nrea' class='form-control mandatoryGroup' placeholder='Numero Rea' " + eventoCallRicerca + " onkeypress='return false' onkeyup='return false' onkeydown='return false' onpaste='return false' />");
        ricDom += buildFormGroup(4, "<div class='input-group'><input name='codiceFiscale' class='form-control mandatoryGroup' placeholder='Codice fiscale' " + eventoCallRicerca + " onkeypress='return false' onkeyup='return false' onkeydown='return false' onpaste='return false' />" + getSpanInfoCodiceFiscale() + "</div>");
        ricDom += buildFormGroup(4, "<div class='input-group'><input name='email' class='form-control' placeholder='Email destinatario' /> " + getSpanEmailDestinatario() + "</div>");
        ricDom += buildFormGroup(4, "<input name='tipoVisura' type='hidden' value='1'/>");
        ricDom += "<div class=\"col col-xs-12 form-group\">";
        ricDom += "<div style=\"text-align:left; !important\" class=\"col col-xs-12\"><b>La Visura Storica contiene le informazioni della Visura Ordinaria con l'aggiunta delle trascrizioni degli atti, delle informazioni di natura storica estratte dal Registro Ditte e le informazioni successive all'iscrizione nel Registro Imprese.</b> <br>Per procedere con l’acquisto del prodotto &egrave; necessario effettuare la <a href='javascript:void(0)' onclick='confermaGoToRicerca(" + idProdotto + ")' style='text-color:'#f76f00;'>Ricerca Impresa</a></div>";
        ricDom += "<br/>";
        ricDom += "<br/>";
        ricDom += "<div style=\"text-align:left; !important\" class=\"col col-xs-12\"><strong>Servizio soggetto ad IVA per il quale &egrave; pertanto previsto l'obbligo di fatturazione, laddove richiesta, o di emissione di scontrino o ricevuta fiscale</strong></div>";
        ricDom += "</div>";
      }

      else if(idProdotto == ID_PROD_ATTIVAZIONE_SIM_ILIAD) {

        ricDom += inserisciToken();
        ricDom += buildFormGroup(4, "<div class='input-group'><input name='codiceSim' class='form-control' placeholder='Codice SIM' required/><span class='input-group-addon form-info-field' onclick='showInfoSIMBarcode()'><span class='glyphicon glyphicon-info-sign' aria-hidden='true'></span></span></div>");
        ricDom += buildFormGroup(12, "<div>Dati personali cliente</div>");
        ricDom += buildFormGroup(6, "<input name='nome' class='form-control' placeholder='Nome' required />");
        ricDom += buildFormGroup(6, "<input name='cognome' class='form-control' placeholder='Cognome' required />");
        ricDom += buildFormGroup(6, "<div class='input-group'><input name='cf' class='form-control uppercased codiceFiscale' placeholder='Codice Fiscale' required />" + getSpanInfoCodiceFiscale() + "</div>");
        ricDom += buildFormGroup(6, "<div class='input-group'><span class='input-group-addon'>+39</span><input name='telefono' class='form-control' placeholder='Telefono' onkeyup='onlyNumbers(event)' maxlength='10' required /></div>");
        ricDom += buildFormGroup(12, "<div>Indirizzo di residenza del cliente</div>");
        ricDom += buildFormGroup(2, "<input name='toponimo' class='form-control' placeholder='Via/Piazza' required />");
        ricDom += buildFormGroup(8, "<input name='nomestrada' class='form-control' placeholder='Indirizzo' required />");
        ricDom += buildFormGroup(2, "<input name='civico' class='form-control' placeholder='Civico' required />");
        ricDom += buildFormGroup(2, "<input name='cap' class='form-control' placeholder='CAP' required maxlength='5' onkeyup='onlyNumbers(event)' />");
        ricDom += buildFormGroup(8, "<input name='comune' class='form-control' placeholder='Comune' required />");
        ricDom += buildFormGroup(2, "<input name='provincia' class='form-control' placeholder='Provincia' required maxlength='2' />");
        ricDom += buildFormGroup(4, "<input name='frazione' class='form-control' placeholder='Frazione' />");
        ricDom += buildFormGroup(8, "<input name='infoaggiuntiveindirizzo' class='form-control' placeholder='Info aggiuntive (scala/piano)' />");

        ricDom += "<input name='operazione' type='hidden'>";
        ricDom += "<input name='reserveRequestIdAcquistoSimIliad' type='hidden'>";

        ricDom += "<input name='operazione' type='hidden'>";
        if(isSmartSolution) {
          ricDom += "<input name='fmAttivazioneSimIliadImporto' class='fmField importoText' data-placeholder='Importo' type='hidden'>";
        }

        ricDom += "<div class='col col-md-12 form-group accettazioneConsensoContainer'><label for='adeguataVerifica'>Adeguata verifica</label><input required type='hidden' class='inputHelper' name='adeguataVerifica' value='0'><input id='adeguataVerifica' type='checkbox' onclick='this.previousSibling.value=1-this.previousSibling.value' onchange='openDialogAdeguataVerificaAttivazioneSimIliad(event)'></div>";

        ricDom += "<div class='col col-md-12' id='filesUpload'>";
        ricDom += "<div class='col col-md-12' id='msgUploadPostepay'><strong>Per proseguire carica la copia fronte/retro del documento di identità.</strong> <br/>Formati supportati <strong>jpg e png</strong>, con dimensioni massime di " + maxFileSize/1024 + "kB per ciascun file. <br/>L'erogazione del servizio in violazione delle procedure indicate causerà il blocco immediato del servizio stesso.</div>";

        if(!(isSmartSolution && operatingSystem == "isLinux") && ATTIVAZIONE_SIM_ILIAD_SHOW_UPLOAD) {
          ricDom += "<ul id='ppMethodTab' class='nav nav-tabs nav-justified'>" +
            "<li class='active'><a data-toggle='tab' href='#ppMobile'>Usa lo smartphone</a></li>" +
            "<li><a data-toggle='tab' href='#ppUpload'>Carica file</a></li>" +
            //									"<li><a data-toggle='tab' href='#ppWebcam'>Usa la webcam</a></li>" +
            "</ul>";
        }

        var uploadMobile = "<div id='qrCodeRow' class='col col-md-12'><div id='ppQrCode' class='qrCode col col-md-4'></div><div class='col col-md-8 text-justify'>Scansiona il QR code: potrai  caricare i documenti direttamente dal tuo smartphone!<br />Per la scansione dei documenti con un telefono ANDROID consigliamo l'uso dell'APP <strong>LETTORE QR</strong>. Per gli IPHONE basta inquadrare il qr code con la fotocamera del telefono.<br />Per ragioni di sicurezza il QR code sar&agrave; rigenerato ad ogni ricarica, pertanto non potr&agrave; essere utilizzato quello di una ricarica precedente. Qualora dovessero presentarsi problemi di invio, provare a chiudere e riaprire questa sezione con il tasto X in alto a destra e ad effettuare una nuova scansione.<br /><strong>Si ricorda di inquadrare esclusivamente la documentazione richiesta.</strong></div></div>";
        var uploadForm = "<div class='form-inline'><div class='form-group'><input style='margin-bottom:10px;' type='file' accept='.png, .jpg, .jpeg' name='files[]' id='js-upload-files' multiple></div></div><div>Oppure trascina i file nello spazio sottostante (formati supportati: .jpg, .png)</div><div class='upload-drop-zone' id='drop-zone'>Trascina i files qui</div>";
//						var uploadWebCam = "<div class='camera-app'><div id='ppNotIE' style='display:none; text-align:center'>Funzione non supportata per i browser Internet Explorer ed Edge. </div><div id='ppMissingWebcam' style='display: none; text-align:center'>Webcam non trovata. Verificare il collegamento e premere il tasto Refresh.</div><video autoplay id='camera-stream'></video><img id='snap' style='max-width: 100% !important; max-height: 100% !important' src='data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=='><canvas id='snapshot' class='hidden'></canvas><canvas id='resizer' class='hidden'></canvas></div><div class='camera-controls'><div id='take-photo' class='btn btn-primary'>Scatta</div><div id='send-photo' class='btn btn-primary'>Conferma</div><div id='delete-photo' class='btn btn-primary'>Nuovo scatto</div><div id='refresh-webcam' class='btn btn-primary'>Refresh</div></div><strong>Si ricorda di inquadrare esclusivamente la documentazione richiesta.</strong>";

        if(!(isSmartSolution && operatingSystem == "isLinux") && ATTIVAZIONE_SIM_ILIAD_SHOW_UPLOAD) {
          ricDom += "<div class='tab-content'>";
          ricDom += "<div id='ppMobile' class='tab-pane active'>";
          ricDom += uploadMobile;
          ricDom += "</div>";
          ricDom += "<div id='ppUpload' class='tab-pane'>";
          ricDom += uploadForm;
          ricDom += "</div>";
//								ricDom += "<div id='ppWebcam' class='tab-pane'>";
//									ricDom += uploadWebCam;
//								ricDom += "</div>";
          ricDom += "</div>";
        } else {
          ricDom += "<div id='ppMobile' class='tab-pane'>";
          ricDom += uploadMobile;
          ricDom += "</div>";
          ricDom += "<div id='ppUpload' class='tab-pane hidden'>";
          ricDom += uploadForm;
          ricDom += "</div>";
        }

        ricDom += "<div class='col col-md-12' id='uploadedFiles'><div class='list-group'></div></div>";
        ricDom += "<canvas id='resizer' class='hidden'></canvas>";
        ricDom += "</div>";


      } else if(idProdotto == ID_PROD_POSTEPAY){
        // POSTEPAY
        notifica = false;
        ricDom += inserisciToken();
        ricDom += "<div class='col col-md-4 form-group numeroCartaPostePay'><input class='form-control' onkeyup='limitaLunghezzaStringa(event, 16)' required name='numeroPostePay' placeholder='Numero Carta'/></div>";
        ricDom += "<div class='col col-md-4 form-group importoContainer'><div class='input-group'><input name='importo' onkeyup='limitaPostePay(event)' class='importo' type='number' required placeholder='00'/><span class='comma'>,</span><input class='decimali' maxlength=2 onkeyup='onlyNumbers(event)' onblur='formattaDecimali(event)' placeholder='00' type='tel'></div></div>";

        ricDom += "<div class='col col-md-12'>Titolare</div>";

        ricDom += "<div class='col col-md-4 form-group nome'><input name='nomeTitolare' class='form-control' required placeholder='Nome'/></div>";
        ricDom += "<div class='col col-md-4 form-group cognome'><input name='cognomeTitolare' class='form-control' required placeholder='Cognome'/></div>";
        ricDom += "<div class='col col-md-4 form-group codiceFiscale'><input name='codFiscaleTitolare' class='form-control uppercased codiceFiscale' required placeholder='Codice Fiscale'/></div>";

//						ricDom += "<div class='col col-md-4 form-group notificaMail'><input class='form-control' name='notifica' placeholder='Notifica Mail (opzionale)'/></div>";
        ricDom += "<div class='col col-md-4 form-group destinatarioFlagContainer'><label for='destinatarioFlag'>L'ordinante non coincide con l'intestatario</label>";
//						ricDom += "<input name='destinatarioFlag' name='destinatarioFlag' id='destinatarioFlag' type='checkbox' onchange='openSubDialog(event)'/></div>";
        ricDom += "<input required type='hidden' name='destinatarioFlag' class='inputHelper' value='0'><input id='destinatarioFlag' type='checkbox' onclick='this.previousSibling.value=1-this.previousSibling.value' onchange='openSubDialog(event)'></div>";

        ricDom += "<div class='col col-md-4 form-group accettazioneConsensoContainer'><label for='accettazioneConsenso'>Adeguata verifica</label>";
        ricDom += "<input required type='hidden' class='inputHelper' name='accettazioneConsenso' value='0'><input id='accettazioneConsenso' type='checkbox' onclick='this.previousSibling.value=1-this.previousSibling.value' onchange='openDialogoPostepay(event)'></div>";

        ricDom += "<div class='subDialogForm riceventeCont hidden'>";
        ricDom += "<div class='col col-md-12'>Dati Ordinante</div>";
        ricDom += "<div class='col col-md-4 form-group nome'><input name='nomeVersante' class='form-control' placeholder='Nome'/></div>";
        ricDom += "<div class='col col-md-4 form-group cognome'><input name='cognomeVersante' class='form-control' placeholder='Cognome'/></div>";
        ricDom += "<div class='col col-md-4 form-group codiceFiscale'><input name='codFiscaleVersante'  class='form-control uppercased codiceFiscale' placeholder='Codice Fiscale'/></div>";
        ricDom += "</div>";

        ricDom += "<div class='col col-md-12' id='filesUpload'>";
        ricDom += "<div class='col col-md-12' id='msgUploadPostepay'>" + MSG_UPLOAD_POSTEPAY + "</div>";

        if(!(isSmartSolution && operatingSystem == "isLinux")) {
          ricDom += "<ul id='ppMethodTab' class='nav nav-tabs nav-justified'><li class='active'><a data-toggle='tab' href='#ppUpload'>Carica file</a></li><li><a data-toggle='tab' href='#ppMobile'>Usa lo smartphone</a></li><li><a data-toggle='tab' href='#ppWebcam'>Usa la webcam</a></li></ul>";
        }

        var uploadMobile = "<div id='qrCodeRow' class='col col-md-12'><div id='ppQrCode' class='qrCode col col-md-4'></div><div class='col col-md-8 text-justify'>Scansiona il QR code: potrai  caricare i documenti direttamente dal tuo smartphone!<br />Per ragioni di sicurezza il QR code sar&agrave; rigenerato ad ogni ricarica, pertanto non potr&agrave; essere utilizzato quello di una ricarica precedente. Qualora dovessero presentarsi problemi di invio, provare a chiudere e riaprire questa sezione con il tasto X in alto a destra e ad effettuare una nuova scansione.<br /><strong>Si ricorda di inquadrare esclusivamente la documentazione richiesta.</strong></div></div>";
        var uploadForm = "<div class='form-inline'><div class='form-group'><input style='margin-bottom:10px;' type='file' accept='.png, .jpg, .jpeg, .pdf' name='files[]' id='js-upload-files' multiple></div></div><div>Oppure trascina i file nello spazio sottostante (formati supportati: .jpg, .png, .pdf)</div><div class='upload-drop-zone' id='drop-zone'>Trascina i files qui</div>";
        var uploadWebCam = "<div class='camera-app'><div id='ppNotIE' style='display:none; text-align:center'>Funzione non supportata per i browser Internet Explorer ed Edge. </div><div id='ppMissingWebcam' style='display: none; text-align:center'>Webcam non trovata. Verificare il collegamento e premere il tasto Refresh.</div><video autoplay id='camera-stream'></video><img id='snap' style='max-width: 100% !important; max-height: 100% !important' src='data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=='><canvas id='snapshot' class='hidden'></canvas><canvas id='resizer' class='hidden'></canvas></div><div class='camera-controls'><div id='take-photo' class='btn btn-primary'>Scatta</div><div id='send-photo' class='btn btn-primary'>Conferma</div><div id='delete-photo' class='btn btn-primary'>Nuovo scatto</div><div id='refresh-webcam' class='btn btn-primary'>Refresh</div></div><strong>Si ricorda di inquadrare esclusivamente la documentazione richiesta.</strong>";

        if(!(isSmartSolution && operatingSystem == "isLinux")) {
          ricDom += "<div class='tab-content'>";
          ricDom += "<div id='ppUpload' class='tab-pane active'>";
          ricDom += uploadForm;
          ricDom += "</div>";
          ricDom += "<div id='ppMobile' class='tab-pane'>";
          ricDom += uploadMobile;
          ricDom += "</div>";
          ricDom += "<div id='ppWebcam' class='tab-pane'>";
          ricDom += uploadWebCam;
          ricDom += "</div>";
          ricDom += "</div>";
        } else {
          ricDom += "<div id='ppMobile' class='tab-pane'>";
          ricDom += uploadMobile;
          ricDom += "</div>";
        }

        ricDom += "<div class='col col-md-12' id='uploadedFiles'><div class='list-group'></div></div>";
        ricDom += "<canvas id='resizer' class='hidden'></canvas>";
        ricDom += "</div>";

      } else if(idProdotto == ID_PROD_MAV_RAV) {
        // mavrav
        ricDom += buildFormGroup(12, "<div class='col-xs-12 form-radio-group'><label class='radio-inline'><input id='mav' type='radio' value='M' checked name='tipoProdotto' required name='tipoProdotto'>Mav</label><label class='radio-inline'><input id='rav' type='radio' value='R' name='tipoProdotto' required name='tipoProdotto'>Rav</label></div>");
        ricDom += inserisciToken();
        ricDom += buildFormGroup(4, "<input name='numeroMavRav' class='form-control fmField fmGenericField' required placeholder='Codice Mav/Rav'/>");
        ricDom += buildFormGroup(4, "<div class='input-group'><input name='importo' class='importo fmField' type='number' required placeholder='00' /><span class='comma'>,</span><input class='decimali fmField' maxlength=2 onkeyup='onlyNumbers(event)' onblur='formattaDecimali(event)' placeholder='00' type='tel'></div>", "importoMavRavContainer importoContainer");
        ricDom += buildFormGroup(12, "<div>Dati pagatore (chi deve pagare il Mav/Rav)</div>");
        ricDom += buildFormGroup(12, "<div class='col-xs-12 form-radio-group'><label class='radio-inline'><input type='radio' name='tipoPagatore' value='pf' checked='checked' onClick='switchTipoPagatoreMavRav()' required>Persona fisica</label><label class='radio-inline'><input type='radio' name='tipoPagatore' value='pg' onClick='switchTipoPagatoreMavRav()' required>Persona giuridica</label></div>");
        ricDom += "<div id='panelPagatoreMavRavPf'>";
        ricDom += buildFormGroup(4, "<input name='nomePagatore' class='form-control' placeholder='Nome' required />");
        ricDom += buildFormGroup(4, "<input name='cognomePagatore' class='form-control' placeholder='Cognome' required />");
        ricDom += buildFormGroup(4, "<div class='input-group'><input name='codFiscalePagatore' class='form-control uppercased codiceFiscale' placeholder='Codice Fiscale' required />" + getSpanInfoCodiceFiscale() + "</div>");
        ricDom += "</div>";
        ricDom += "<div id='panelPagatoreMavRavPg' class='hidden'>";
        ricDom += buildFormGroup(8, "<input name='denominazionePagatore' class='form-control' placeholder='Denominazione' />");
        ricDom += buildFormGroup(4, "<div class='input-group'><input name='partitaIvaPagatore' class='form-control' placeholder='Partita IVA' /><span class='input-group-addon form-info-field' onclick='showInfoBollettiniPartitaIVA()'><span class='glyphicon glyphicon-info-sign' aria-hidden='true'></span></span></div>");
        ricDom += "</div>";
        ricDom += buildFormGroup(8, "<input name='indirizzoPagatore' class='form-control' placeholder='Indirizzo' required />");
        ricDom += buildFormGroup(4, "<input name='cittaPagatore' class='form-control' placeholder='Citt&agrave;' required />");
        ricDom += buildFormGroup(8, "<input name='emailPagatore' class='form-control' placeholder='Email' />");
        ricDom += buildFormGroup(4, "<input name='telefonoPagatore' class='form-control' placeholder='telefonoPagatore' required />");

        ricDom += buildFormGroup(12, "<div class='destinatarioFlagContainer'><label for='pagatoreMavRavFlag'>La persona che hai di fronte non &egrave; il pagatore del Mav/Rav</label><input required type='hidden' name='destinatarioFlag' class='inputHelper' value='0'><input id='pagatoreMavRavFlag' type='checkbox' onclick='this.previousSibling.value=1-this.previousSibling.value' onchange='onEsecutoreMavRavClick(event)'></div>");

        ricDom += "<div class='panel-esecutore-mav-rav riceventeCont hidden'>";
        ricDom += buildFormGroup(12, "<div>Dati Esecutore (la persona che hai di fronte)</div>");
        ricDom += buildFormGroup(4, "<input name='nomeEsecutore' class='form-control' placeholder='Nome' />");
        ricDom += buildFormGroup(4, "<input name='cognomeEsecutore' class='form-control' placeholder='Cognome' />");
        ricDom += buildFormGroup(4, "<div class='input-group'><input name='codFiscaleEsecutore' class='form-control uppercased codiceFiscale' placeholder='Codice Fiscale' />" + getSpanInfoCodiceFiscale() + "</div>");
        ricDom += buildFormGroup(8, "<input name='indirizzoEsecutore' class='form-control' placeholder='Indirizzo' />");
        ricDom += buildFormGroup(4, "<input name='cittaEsecutore' class='form-control' placeholder='Citt&agrave;' />");
        ricDom += "</div>";
        ricDom += "<div class='col col-xs-12 form-group'><div class='col col-xs-12 text-center'><strong>Orari di servizio</strong></div><div class='col col-xs-12'>" + ORARI_BOLLETTINI_DESC + "</div></div>"

      } else if(idProdotto == ID_PROD_PAGOPA){
        //PAGO PA
        ricDom += "<div id='ppMobile' class='row' style='margin: 0'>";
        ricDom += "<div class='col-md-4 form-group'><input id='pagoPaQrCodeScan' type='search' class='form-control sovrascriviTema' placeholder=\"Scansiona qui il QR code dell'avviso\"></div>";
        ricDom += "<div class='col-md-8 form-group text-justify'>Se non hai un lettore di QR code e vuoi acquisire i dati con il telefono <p class='btn-link' style='display: inline-block' onclick='openModalQrCodePagoPa()'>CLICCA QUI.</p></div>";
        ricDom += "</div>";

        ricDom += inserisciToken();
        ricDom += buildFormGroup(4, "<input name='codiceAvviso' class='form-control fmField fmGenericField' placeholder='Codice avviso' required />");
        ricDom += buildFormGroup(4, "<input name='cfGnlEnte' class='form-control fmField fmGenericField' placeholder='CF/P.IVA Ente Creditore' required />");
        ricDom += buildFormGroup(12, "<div>Dati pagatore (destinatario dell'avviso di pagamento)</div>");
        ricDom += buildFormGroup(12, "<div class='col-xs-12 form-radio-group'><label class='radio-inline'><input type='radio' name='tipoPagatore' value='pf' checked='checked' onClick='switchTipoPagatorePagoPa()' required>Persona fisica</label><label class='radio-inline'><input type='radio' name='tipoPagatore' value='pg' onClick='switchTipoPagatorePagoPa()' required>Persona giuridica</label></div>");
        ricDom += "<div id='panelPagatorePagoPaPf'>";
        ricDom += buildFormGroup(4, "<input name='nomePagatore' class='form-control' placeholder='Nome' required />");
        ricDom += buildFormGroup(4, "<input name='cognomePagatore' class='form-control' placeholder='Cognome' required />");
        ricDom += buildFormGroup(4, "<div class='input-group'><input name='codFiscalePagatore' class='form-control uppercased codiceFiscale' placeholder='Codice Fiscale' required />" + getSpanInfoCodiceFiscale() + "</div>");
        ricDom += "</div>";
        ricDom += "<div id='panelPagatorePagoPaPg' class='hidden'>";
        ricDom += buildFormGroup(8, "<input name='denominazionePagatore' class='form-control' placeholder='Denominazione' />");
        ricDom += buildFormGroup(4, "<div class='input-group'><input name='partitaIvaPagatore' class='form-control' placeholder='Partita IVA' /><span class='input-group-addon form-info-field' onclick='showInfoBollettiniPartitaIVA()'><span class='glyphicon glyphicon-info-sign' aria-hidden='true'></span></span></div>");
        ricDom += "</div>";
        ricDom += buildFormGroup(8, "<input name='indirizzoPagatore' class='form-control' placeholder='Indirizzo' required />");
        ricDom += buildFormGroup(4, "<input name='cittaPagatore' class='form-control' placeholder='Citt&agrave;' required />");
        ricDom += buildFormGroup(8, "<input name='emailPagatore' class='form-control' placeholder='Email' />");
        ricDom += buildFormGroup(4, "<input name='telefonoPagatore' class='form-control' placeholder='Telefono' />");

        ricDom += buildFormGroup(12, "<div class='destinatarioFlagContainer'><label for='pagatorePagoPaFlag'>La persona che hai di fronte non &egrave; il destinatario dell'avviso</label><input required type='hidden' name='destinatarioFlag' class='inputHelper' value='0'><input id='pagatorePagoPaFlag' type='checkbox' onclick='this.previousSibling.value=1-this.previousSibling.value' onchange='onEsecutorePagoPaClick(event)'></div>");

        ricDom += "<div class='panel-esecutore-pagopa riceventeCont hidden'>";
        ricDom += buildFormGroup(12, "<div>Dati Esecutore (la persona che hai di fronte)</div>");
        ricDom += buildFormGroup(4, "<input name='nomeEsecutore' class='form-control' placeholder='Nome' />");
        ricDom += buildFormGroup(4, "<input name='cognomeEsecutore' class='form-control' placeholder='Cognome' />");
        ricDom += buildFormGroup(4, "<div class='input-group'><input name='codFiscaleEsecutore' class='form-control uppercased codiceFiscale' placeholder='Codice Fiscale' />" + getSpanInfoCodiceFiscale() + "</div>");
        ricDom += buildFormGroup(8, "<input name='indirizzoEsecutore' class='form-control' placeholder='Indirizzo' />");
        ricDom += buildFormGroup(4, "<input name='cittaEsecutore' class='form-control' placeholder='Citt&agrave;' />");
        ricDom += "</div>";
        ricDom += "<div class='col col-xs-12 form-group'><div class='col col-xs-12 text-center'><strong>Orari di servizio</strong></div><div class='col col-xs-12'>" + ORARI_BOLLETTINI_DESC + "</div></div>"

        ricDom += "<input name='operazione' type='hidden'>";
        ricDom += "<input name='reserveRequestIdPagoPa' type='hidden'>";
        if(isSmartSolution) {
          ricDom += "<input name='fmPagoPaBeneficiario' class='fmField fmGenericField' data-placeholder='Beneficiario' type='hidden'>";
          ricDom += "<input name='fmPagoPaImporto' class='fmField importoText' data-placeholder='Importo' type='hidden'>";
        }
      } else if(idProdotto == ID_PROD_BOLLOAUTO){

        // Lettura dei tipi di veicolo e predisposizione dei campi telaio/targa
        var tipi = TIPI_VEICOLO_BOLLOAUTO.split("#");
        var options = "";
        var presenteTipoConTarga = false;
        var presenteTipoConTelaio = false;
        var numElements = 2; //token e tipo veicolo
        for(var indexTipi=0; indexTipi<tipi.length; indexTipi++) {
          var tipo = tipi[indexTipi].split("_"); // 0-value, 1-label, 2-targa/telaio
          options += "<option value=\"" + tipo[0] + "\" data-tt=\"" + tipo[2] + "\">" + tipo[1] + "</option>";
          if(tipo[2] == "targa") presenteTipoConTarga = true;
          if(tipo[2] == "telaio") presenteTipoConTelaio = true;
        }
        if(presenteTipoConTarga) numElements++;
        if(presenteTipoConTelaio) numElements++;
        var colMd = 12 / numElements;

        ricDom += inserisciToken(colMd);
        ricDom += buildFormGroup(colMd, "<select class=\"form-control\" name=\"tipoVeicolo\" required onchange='onTipoVeicoloChanged()'><option value data-tt=\"select\">Tipo veicolo</option>" + options + "</select>");
        if(isSmartSolution) {
          if(presenteTipoConTarga) ricDom += buildFormGroup(colMd, "<div class='input-group'><input name='targa' class='form-control' placeholder='Targa' disabled='disabled' " + buildShowFmHiddenFieldKeyEvents("targa", idProdotto) + " />" + buildShowFmHiddenFieldButton("targa", idProdotto) + "</div>");
          if(presenteTipoConTelaio) ricDom += buildFormGroup(colMd, "<div class='input-group'><input name='telaio' class='form-control' placeholder='Telaio' disabled='disabled' " + buildShowFmHiddenFieldKeyEvents("telaio", idProdotto) + " />" + buildShowFmHiddenFieldButton("telaio", idProdotto) + "</div>");
        } else {
          if(presenteTipoConTarga) ricDom += buildFormGroup(colMd, "<input name='targa' class='form-control' placeholder='Targa' disabled='disabled' />");
          if(presenteTipoConTelaio) ricDom += buildFormGroup(colMd, "<input name='telaio' class='form-control' placeholder='Telaio' disabled='disabled' />");
        }

        ricDom += buildFormGroup(12, "<div>Dati pagatore</div>");
        ricDom += buildFormGroup(12, "<div class='col-xs-12 form-radio-group'><label class='radio-inline'><input type='radio' name='tipoPagatore' value='pf' checked='checked' onClick='switchTipoPagatoreBolloAuto()' required>Persona fisica</label><label class='radio-inline'><input type='radio' name='tipoPagatore' value='pg' onClick='switchTipoPagatoreBolloAuto()' required>Persona giuridica</label></div>");
        ricDom += "<div id='panelPagatoreBolloAutoPf'>";
        ricDom += buildFormGroup(4, "<input name='nomePagatore' class='form-control' placeholder='Nome' required />");
        ricDom += buildFormGroup(4, "<input name='cognomePagatore' class='form-control' placeholder='Cognome' required />");
        ricDom += buildFormGroup(4, "<div class='input-group'><input name='codFiscalePagatore' class='form-control uppercased codiceFiscale' placeholder='Codice Fiscale' required />" + getSpanInfoCodiceFiscale() + "</div>");
        ricDom += "</div>";
        ricDom += "<div id='panelPagatoreBolloAutoPg' class='hidden'>";
        ricDom += buildFormGroup(8, "<input name='denominazionePagatore' class='form-control' placeholder='Denominazione' />");
        ricDom += buildFormGroup(4, "<div class='input-group'><input name='partitaIvaPagatore' class='form-control' placeholder='Partita IVA' /><span class='input-group-addon form-info-field' onclick='showInfoBollettiniPartitaIVA()'><span class='glyphicon glyphicon-info-sign' aria-hidden='true'></span></span></div>");
        ricDom += "</div>";
        ricDom += buildFormGroup(8, "<input name='indirizzoPagatore' class='form-control' placeholder='Indirizzo' required />");
        ricDom += buildFormGroup(4, "<input name='cittaPagatore' class='form-control' placeholder='Citt&agrave;' required />");
        ricDom += buildFormGroup(8, "<input name='emailPagatore' class='form-control' placeholder='Email' />");
        ricDom += buildFormGroup(4, "<input name='telefonoPagatore' class='form-control' placeholder='Telefono' />");

        ricDom += buildFormGroup(12, "<div class='destinatarioFlagContainer'><label for='pagatoreBolloAutoFlag'>La persona che hai di fronte non &egrave; il intestatario del pagamento del bollo</label><input required type='hidden' name='destinatarioFlag' class='inputHelper' value='0'><input id='pagatoreBolloAutoFlag' type='checkbox' onclick='this.previousSibling.value=1-this.previousSibling.value' onchange='onEsecutoreBolloAutoClick(event)'></div>");

        ricDom += "<div class='panel-esecutore-bollo-auto riceventeCont hidden'>";
        ricDom += buildFormGroup(12, "<div>Dati Esecutore (la persona che hai di fronte)</div>");
        ricDom += buildFormGroup(4, "<input name='nomeEsecutore' class='form-control' placeholder='Nome' />");
        ricDom += buildFormGroup(4, "<input name='cognomeEsecutore' class='form-control' placeholder='Cognome' />");
        ricDom += buildFormGroup(4, "<div class='input-group'><input name='codFiscaleEsecutore' class='form-control uppercased codiceFiscale' placeholder='Codice Fiscale' />" + getSpanInfoCodiceFiscale() + "</div>");
        ricDom += buildFormGroup(8, "<input name='indirizzoEsecutore' class='form-control' placeholder='Indirizzo' />");
        ricDom += buildFormGroup(4, "<input name='cittaEsecutore' class='form-control' placeholder='Citt&agrave;' />");
        ricDom += "</div>";
        ricDom += "<div class='col col-xs-12 form-group'><div class='col col-xs-12 text-center'><strong>Orari di servizio</strong></div><div class='col col-xs-12'>" + ORARI_BOLLETTINI_DESC + "</div></div>";
        ricDom += AVVISI_BOLLOAUTO;

        ricDom += "<input name='operazione' type='hidden'>";
        if(isSmartSolution) {
          ricDom += "<input name='fmBolloAutoImporto' class='fmField importoText' data-placeholder='Importo' type='hidden'>";
        }

      }else if(idProdotto == ID_PROD_CBILL){
        // cbill
        ricDom += inserisciToken();

        ricDom += "<div class='col col-md-4 form-group nomeAzienda'><div class='input-group'><input id='autocompleteAzienda' onfocus='onFocusCampoAziendaCbill(this)' onblur='onBlurCampoAziendaCbill(this, event)' name='nomeAzienda' onkeyup='getListaAziende(event)' class='form-control parametroSia' placeholder='Nome Azienda'/><span class='input-group-addon inCorso' id='cercaAziendeWait'><span class='glyphicon glyphicon-search' aria-hidden='true'></span></span></div></div>";
        ricDom += "<div class='col col-md-4 form-group codiceFiscale'><div class='input-group'><input name='codiceFiscale' required class='form-control uppercased parametroSia codiceFiscale' placeholder='Codice Fiscale'/><span class='input-group-addon' id='cercaBollettiniBtn' onclick='cercaBollettini(event)'><span class='glyphicon glyphicon-search' aria-hidden='true'></span></span></div></div>";
        ricDom += "<div id='SIA_popUp' class='panel panel-default hidden'></div>";
        ricDom += "<div class='col col-md-4 form-group codiceSIA'><input id='codSia' name='codiceSia' class='form-control' required placeholder='Codice SIA'/></div>";
        ricDom += "<div class='col col-md-4 form-group numeroIdBollettino'><input id='idBollettino' name='numeroIdBollettino' class='form-control codiceCBill' required placeholder='Numero Bollettino'/></div>";
        ricDom += "<div class='col col-md-4 form-group importoContainer'><div id='importoVersamento' class='input-group'><input name='importo' class='importo' type='number' required placeholder='00'/><span class='comma'>,</span><input class='decimali' maxlength=2 onkeyup='onlyNumbers(event)' onblur='formattaDecimali(event)' placeholder='00' type='tel'></div></div>";

      }else if(idProdotto == ID_PROD_BOLLETTINI_OLD){
        // bollettini postali
        notifica = false;
        ricDom += "<div class='pointer' onclick='goToProduct(event, 23)' style='font-weight:bold;text-decoration:underline'>Ora puoi effettuare Bollettini Postali tramite il nuovo servizio CBILL </div>";

      } else if(idProdotto == ID_PROD_AMAZON_WALLET) {
        ricDom += inserisciToken();
        ricDom += buildFormGroup(4, "<div class='input-group'><input name='importo' class='importo fmField' type='number' required placeholder='00'  onkeypress='return killEnterEvent(window.event)' onkeyup='return killEnterEvent(window.event)' onkeydown='return killEnterEvent(window.event)' /><span class='comma'>,</span><input class='decimali fmField' maxlength=2   onkeypress='return killEnterEvent(window.event)' onkeydown='return killEnterEvent(window.event)' onkeyup='if( killEnterEvent(window.event) ) { onlyNumbers(event) } else { return false }' onblur='formattaDecimali(event)' placeholder='00' type='tel'></div>", "importoBollettinoContainer importoContainer");
        ricDom += buildFormGroup(4, "<div class='input-group'><input name='codiceConto' class='form-control' placeholder='Telefono o Codice Conto' required  onkeypress='return killEnterEvent(window.event)' onkeyup='return killEnterEvent(window.event)' onkeydown='return killEnterEvent(window.event)'>" + getSpanInfoCodiceConto() + "</div>");
        ricDom += "<div class=\"col col-xs-12 form-group\">";
        ricDom += "<div class=\"col col-xs-12\">" + MSG_EPIPOLI_WALLET_OUT_OF_RANGE_MSG + "</div>";
        ricDom += "</div>";
      } else if(idProdotto == ID_PROD_BOLLETTINO_FRECCIA) {
        ricDom += inserisciToken();
        //ricDom += "<input name=\"idProdotto\" type=\"hidden\" required=\"\" value=\"113\"><input name=\"requestId\" type=\"hidden\" required=\"\" value=\"\"><span class=\"searchTarget\">Bollettino Freccia</span>";
        //ricDom += "<div class=\"descrizioneProdotto\">Pagamenti</div>";
        //ricDom += "<div class=\"formProdotto row\">";
        //ricDom += "<input type=\"text\" style=\"display:none\"><input type=\"password\" style=\"display:none\"><input type=\"text\" style=\"display:none\"><input type=\"password\" style=\"display:none\">";
        //ricDom +=  "<div class=\"col col-md-4 form-group token\"><input name=\"token\" autocomplete=\"new-password\" type=\"password\" class=\"form-control sovrascriviTema\" required=\"\" placeholder=\"Token\"></div>";
        ricDom +=  "<div class=\"col col-md-8 form-group\"><input name=\"cinIdPagamento\" class=\"form-control cinIdPagamento\" placeholder=\"Cod.Identificativo Pagamento\" data-placeholder=\"Cod.Identificativo Pagamento\" onblur=\"this.value=this.value.toUpperCase().trim()\" maxlength=\"21\" required></div>";
        ricDom +=  "<div class=\"col col-md-8 form-group\">";
        ricDom +=  "<input name=\"iban\" class=\"form-control iban\" placeholder=\"Coordinate Bancarie Creditore\" data-placeholder=\"Coordinate Bancarie Creditore\" onblur=\"this.value=this.value.toUpperCase().trim()\" maxlength=\"27\" required>";
        ricDom += "</div>";
        ricDom += "<div class=\"col col-md-4 form-group\">";
        ricDom += "<input name=\"codEsenzione\" class=\"form-control codEsenzione\" placeholder=\"Cod.Esenzione\" data-placeholder=\"Cod.Esenzione\" onkeyup=\"onlyNumbers(event)\" onblur=\"this.value=this.value.trim();if(this.value != '1' && this.value != '0'){this.value = '1';}\" maxlength=\"1\" required>";
        ricDom += "</div>";
        ricDom += "<div id=\"bollettini_biller_container\" class=\"riceventeCont hidden\" style=\"padding-top: 14px\">";
        ricDom += "<div id=\"bollettini_biller_name\" class=\"form-label\"></div>";
        ricDom += "<div id=\"bollettini_biller_additional_fields\"></div>";
        ricDom += "</div>";
        ricDom += "<div class=\"col col-md-4 form-group\"><input name=\"cinImporto\" class=\"form-control cinImporto\" placeholder=\"Cin.Importo\" data-placeholder=\"Cin.Importo\" onblur=\"this.value=this.value.toUpperCase().trim()\" maxlength=\"1\" required></div>";
        ricDom += "<div class=\"col col-md-4 form-group\"><input name=\"cinIntermedio\" class=\"form-control cinIntermedio\" placeholder=\"Cin.Intermedio\" data-placeholder=\"Cin.Intermedio\" onblur=\"this.value=this.value.toUpperCase().trim()\" maxlength=\"1\" required></div>";
        ricDom += "<div class=\"col col-md-4 form-group\"><input name=\"cinComplessivo\" class=\"form-control cinComplessivo\" placeholder=\"Cin.Complessivo\" data-placeholder=\"Cin.Complessivo\" onblur=\"this.value=this.value.toUpperCase().trim()\" maxlength=\"1\" required></div>";
        ricDom += "<div class=\"col col-md-8 form-group\"><input name=\"causaleVersamento\" class=\"form-control causaleVersamento\" placeholder=\"Causale\"></div>";

        //ricDom += "<div class=\"col col-md-4 form-group importoBollettinoContainer importoContainer\"><div class=\"input-group\"><input name=\"importoVersamento\" class=\"form-control importoVersamento\" type=\"number\" required=\"\" placeholder=\"00\" data-placeholder=\"00\"><span class=\"comma\">,</span><input class=\"decimali fmField\" maxlength=\"2\" onkeyup=\"onlyNumbers(event)\" onblur=\"formattaDecimali(event)\" placeholder=\"00\" type=\"tel\" data-placeholder=\"00\"></div></div>";
        ricDom += buildFormGroup(4, "<div class='input-group'><input name='importo' class='importo fmField' type='number' required placeholder='00' /><span class='comma'>,</span><input class='decimali fmField' maxlength=2 onkeyup='onlyNumbers(event)' onblur='formattaDecimali(event)' placeholder='00' type='tel'></div>", "importoBollettinoContainer importoContainer");

        //ricDom += "<div class=\"col col-md-4 form-group\"><input name=\"importoVersamento\" class=\"form-control importoVersamento\" placeholder=\"00,00\"></div>";
        ricDom += "<div class=\"col col-md-12 form-group\">";
        ricDom += "<div>Dati pagatore (chi deve pagare il bollettino)</div>";
        ricDom += "</div>";
        ricDom += "<div class=\"col col-md-12 form-group\">";
        ricDom += "<div class=\"col-xs-12 form-radio-group\"><label class=\"radio-inline\"><input type=\"radio\" name=\"tipoPagatore\" value=\"pf\" checked=\"checked\" onclick=\"switchTipoPagatoreBollettinoFreccia()\" required=\"\">Persona fisica</label><label class=\"radio-inline\"><input type=\"radio\" name=\"tipoPagatore\" value=\"pg\" onclick=\"switchTipoPagatoreBollettinoFreccia()\" required=\"\">Persona giuridica</label></div>";
        ricDom += "</div>";
        ricDom += "<div id=\"panelPagatoreBollettinoFrecciaPf\">";
        ricDom += "<div class=\"col col-md-4 form-group\"><input name=\"nomePagatore\" class=\"form-control\" placeholder=\"Nome\" required=\"required\"></div>";
        ricDom += "<div class=\"col col-md-4 form-group\"><input name=\"cognomePagatore\" class=\"form-control\" placeholder=\"Cognome\" required=\"required\"></div>";
        ricDom += "<div class=\"col col-md-4 form-group\">";
        ricDom += "<div class=\"input-group\"><input name=\"codFiscalePagatore\" class=\"form-control uppercased codFiscalePagatore\" placeholder=\"Codice Fiscale\" required=\"required\" onblur=\"this.value=this.value.toUpperCase().trim()\"><span class=\"input-group-addon form-info-field\" onclick=\"showInfoCodiceFiscale()\"><span class=\"glyphicon glyphicon-info-sign\" aria-hidden=\"true\"></span></span></div>";
        ricDom += "</div>";
        ricDom += "</div>";
        ricDom += "<div id=\"panelPagatoreBollettinoFrecciaPg\" class=\"hidden\">";
        ricDom += "<div class=\"col col-md-8 form-group\"><input name=\"denominazionePagatore\" class=\"form-control\" placeholder=\"Denominazione\"></div>";
        ricDom += "<div class=\"col col-md-4 form-group\">";
        ricDom += "<div class=\"input-group\"><input name=\"partitaIvaPagatore\" class=\"form-control\" placeholder=\"Partita IVA\" onblur=\"this.value=this.value.toUpperCase().trim()\"><span class=\"input-group-addon form-info-field\" onclick=\"showInfoBollettiniPartitaIVA()\"><span class=\"glyphicon glyphicon-info-sign\" aria-hidden=\"true\"></span></span></div>";
        ricDom += "</div>";
        ricDom += "</div>";
        ricDom += "<div class=\"col col-md-8 form-group\"><input name=\"indirizzoPagatore\" class=\"form-control\" placeholder=\"Indirizzo\" required=\"\"></div>";
        ricDom += "<div class=\"col col-md-4 form-group\"><input name=\"cittaPagatore\" class=\"form-control\" placeholder=\"Città\" required=\"\"></div>";
        ricDom += "<div class=\"col col-md-8 form-group\"><input name=\"emailPagatore\" class=\"form-control\" placeholder=\"Email\"></div>";
        ricDom += "<div class=\"col col-md-4 form-group\"><input name=\"telefonoPagatore\" class=\"form-control telefonoPagatore\" placeholder=\"Telefono\"></div>";
        ricDom += "<div class=\"col col-md-12 form-group\">";
        ricDom += "<div class=\"destinatarioFlagContainer\"><label for=\"pagatoreBollettinoFrecciaFlag\">La persona che hai di fronte non è il pagatore del bollettino</label><input required=\"\" type=\"hidden\" name=\"destinatarioFlag\" class=\"inputHelper\" value=\"0\"><input id=\"pagatoreBollettinoFrecciaFlag\" type=\"checkbox\" onclick=\"this.previousSibling.value=1-this.previousSibling.value\" onchange=\"onEsecutoreBollettinoClick(event)\"></div>";
        ricDom +="</div>";
        ricDom += "<div class=\"panel-esecutore-bollettino riceventeCont hidden\">";
        ricDom += "<div class=\"col col-md-12 form-group\">";
        ricDom += "<div>Dati Esecutore (la persona che hai di fronte)</div>";
        ricDom += "</div>";
        ricDom += "<div class=\"col col-md-4 form-group\"><input name=\"nomeEsecutore\" class=\"form-control\" placeholder=\"Nome\"></div>";
        ricDom += "<div class=\"col col-md-4 form-group\"><input name=\"cognomeEsecutore\" class=\"form-control\" placeholder=\"Cognome\"></div>";
        ricDom += "<div class=\"col col-md-4 form-group\">";
        ricDom += "<div class=\"input-group\"><input name=\"codFiscaleEsecutore\" class=\"form-control uppercased codiceFiscale\" placeholder=\"Codice Fiscale\" onblur=\"this.value=this.value.toUpperCase().trim()\"><span class=\"input-group-addon form-info-field\" onclick=\"showInfoCodiceFiscale()\"><span class=\"glyphicon glyphicon-info-sign\" aria-hidden=\"true\"></span></span></div>";
        ricDom += "</div>";
        ricDom += "<div class=\"col col-md-8 form-group\"><input name=\"indirizzoEsecutore\" class=\"form-control\" placeholder=\"Indirizzo\"></div>";
        ricDom += "<div class=\"col col-md-4 form-group\"><input name=\"cittaEsecutore\" class=\"form-control\" placeholder=\"Città\"></div>";
        ricDom += "</div>";
        ricDom += "<div class=\"col col-xs-12 form-group\">";
        ricDom += "<div class=\"col col-xs-12 text-center\"><strong>Orari di servizio</strong></div>";
        ricDom += "<div class=\"col col-xs-12\">Tutti i giorni dalle ore 6:00 alle ore 00:30</div>";
        ricDom += "</div>";
        ricDom += "<input name=\"reserveRequestIdBollettinoPagato\" type=\"hidden\" value=\"\">";
        //ricDom += "</div>";
        //ricDom += "<div class=\"dettaglio row\"></div>";
        //ricDom += "<div class=\"confirm\">";
        //ricDom += "<div class=\"btn btn-primary col col-md-3\" onclick=\"confermaVendita(113)\">Conferma</div>";
        //ricDom += "</div>";
      } else if(idProdotto == ID_PROD_BOLLETTINI) {
        ricDom += inserisciToken();
        ricDom += buildFormGroup(8, "<div class='input-group'><input name='barcode' maxlength='50' class='form-control barcodeBollettino' placeholder='Scansiona qui il codice a barre (per bollettini di tipo 896)'/><span class='input-group-addon form-info-field' onclick='showInfoBollettiniBarcode()'><span class='glyphicon glyphicon-info-sign' aria-hidden='true'></span></span></div>");
        ricDom += buildFormGroup(4, "<select class=\"form-control\" name=\"tipoBollettino\" required onchange='onTipoBollettinoChanged()'><option value>Tipo bollettino</option><option value=\"123\">123 - Bianco generico</option><option value=\"451\">451 - Bianco personalizzato</option><option value=\"674\">674 - Premarcato non fatturatore</option><option value=\"896\">896 - Premarcato fatturatore</option></select>");
        ricDom += buildFormGroup(4, "<div class='input-group'><input name='ccp' class='form-control fmField fmGenericField' placeholder='Numero conto corrente' required /><span class='input-group-addon buttonWithLoading form-info-field' onclick='getBillerBollettinoFromCcp()'><span class='glyphicon glyphicon-search' aria-hidden='true'></span></span></div>");
        ricDom += buildFormGroup(4, "<div class='input-group'><input name='importo' class='importo fmField' type='number' required placeholder='00' /><span class='comma'>,</span><input class='decimali fmField' maxlength=2 onkeyup='onlyNumbers(event)' onblur='formattaDecimali(event)' placeholder='00' type='tel'></div>", "importoBollettinoContainer importoContainer");
        ricDom += "<div id='bollettini_biller_container' class='riceventeCont hidden' style='padding-top: 14px'>";
        ricDom += "<div id='bollettini_biller_name' class='form-label'></div>";
        ricDom += "<div id='bollettini_biller_additional_fields'></div>";
        ricDom += "</div>";
        ricDom += buildFormGroup(8, "<input name='causale' class='form-control causale' placeholder='Causale'/>");
        ricDom += buildFormGroup(4, "<input name='codiceBollettino' disabled='disabled' class='form-control fmField fmGenericField' placeholder='Codice bollettino (per tipi 674 e 896)' />");

        ricDom += buildFormGroup(12, "<div>Dati pagatore (chi deve pagare il bollettino)</div>");
        ricDom += buildFormGroup(12, "<div class='col-xs-12 form-radio-group'><label class='radio-inline'><input type='radio' name='tipoPagatore' value='pf' checked='checked' onClick='switchTipoPagatoreBollettino()' required>Persona fisica</label><label class='radio-inline'><input type='radio' name='tipoPagatore' value='pg' onClick='switchTipoPagatoreBollettino()' required>Persona giuridica</label></div>");
        ricDom += "<div id='panelPagatoreBollettinoPf'>";
        ricDom += buildFormGroup(4, "<input name='nomePagatore' class='form-control' placeholder='Nome' required />");
        ricDom += buildFormGroup(4, "<input name='cognomePagatore' class='form-control' placeholder='Cognome' required />");
        ricDom += buildFormGroup(4, "<div class='input-group'><input name='codFiscalePagatore' class='form-control uppercased codiceFiscale' placeholder='Codice Fiscale' required />" + getSpanInfoCodiceFiscale() + "</div>");
        ricDom += "</div>";
        ricDom += "<div id='panelPagatoreBollettinoPg' class='hidden'>";
        ricDom += buildFormGroup(8, "<input name='denominazionePagatore' class='form-control' placeholder='Denominazione' />");
        ricDom += buildFormGroup(4, "<div class='input-group'><input name='partitaIvaPagatore' class='form-control' placeholder='Partita IVA' /><span class='input-group-addon form-info-field' onclick='showInfoBollettiniPartitaIVA()'><span class='glyphicon glyphicon-info-sign' aria-hidden='true'></span></span></div>");
        ricDom += "</div>";
        ricDom += buildFormGroup(8, "<input name='indirizzoPagatore' class='form-control' placeholder='Indirizzo' required />");
        ricDom += buildFormGroup(4, "<input name='cittaPagatore' class='form-control' placeholder='Citt&agrave;' required />");
        ricDom += buildFormGroup(8, "<input name='emailPagatore' class='form-control' placeholder='Email' />");
        ricDom += buildFormGroup(4, "<input name='telefonoPagatore' class='form-control' placeholder='Telefono' />");

        ricDom += buildFormGroup(12, "<div class='destinatarioFlagContainer'><label for='pagatoreBollettinoFlag'>La persona che hai di fronte non &egrave; il pagatore del bollettino</label><input required type='hidden' name='destinatarioFlag' class='inputHelper' value='0'><input id='pagatoreBollettinoFlag' type='checkbox' onclick='this.previousSibling.value=1-this.previousSibling.value' onchange='onEsecutoreBollettinoClick(event)'></div>");

        ricDom += "<div class='panel-esecutore-bollettino riceventeCont hidden'>";
        ricDom += buildFormGroup(12, "<div>Dati Esecutore (la persona che hai di fronte)</div>");
        ricDom += buildFormGroup(4, "<input name='nomeEsecutore' class='form-control' placeholder='Nome' />");
        ricDom += buildFormGroup(4, "<input name='cognomeEsecutore' class='form-control' placeholder='Cognome' />");
        ricDom += buildFormGroup(4, "<div class='input-group'><input name='codFiscaleEsecutore' class='form-control uppercased codiceFiscale' placeholder='Codice Fiscale' />" + getSpanInfoCodiceFiscale() + "</div>");
        ricDom += buildFormGroup(8, "<input name='indirizzoEsecutore' class='form-control' placeholder='Indirizzo' />");
        ricDom += buildFormGroup(4, "<input name='cittaEsecutore' class='form-control' placeholder='Citt&agrave;' />");
        ricDom += "</div>";

        ricDom += "<div class='col col-xs-12 form-group'><div class='col col-xs-12 text-center'><strong>Orari di servizio</strong></div><div class='col col-xs-12'>" + ORARI_BOLLETTINI_DESC + "</div></div>"

        ricDom += "<input name='reserveRequestIdBollettinoPagato' type='hidden'>";

      } else if (idProdotto == ID_PROD_PAYSAFE_DIRECT) {
        ricDom += "<div style='width: 100%; display: inline-block'>";
        ricDom += inserisciToken();
        ricDom += "<div class='col col-md-4 form-group barcode'><div class='input-group'><input name='barcode' class='form-control' required placeholder='Codice a barre'/><span class='input-group-addon'><span class='glyphicon glyphicon-barcode' aria-hidden='true'></span></span></div></div>";
        ricDom += "</div>";
        ricDom += "<div class='col col-md-12'>Ordinante</div>";
        ricDom += "<div class='col col-md-4 form-group nome'><input name='nome' class='form-control' required placeholder='Nome'/></div>";
        ricDom += "<div class='col col-md-4 form-group cognome'><input name='cognome' class='form-control' required placeholder='Cognome'/></div>";
        ricDom += "<div class='col col-md-4 form-group codiceFiscale'><input name='codiceFiscale' class='form-control' required placeholder='Codice fiscale'/></div>";
        ricDom += "<div class='col col-md-4 form-group tipoDocumento'><input name='tipoDocumento' class='form-control' required placeholder='Tipo documento'/></div>";
        ricDom += "<div class='col col-md-4 form-group numeroDocumento'><input name='numeroDocumento' class='form-control' required placeholder='Numero documento'/></div>";
        ricDom += "<div class='col col-md-4 form-group dataScadenzaDocumento'><input name='dataScadenzaDocumento' required class='form-control' placeholder='Data scadenza documento'/></div>";

      } else if (idProdotto == ID_PROD_PAYSAFE_CASH) {
        ricDom += "<div style='width: 100%; display: inline-block'>";
        ricDom += inserisciToken();
        ricDom += "<div class='col col-md-4 form-group barcode'><div class='input-group'><input name='barcode' class='form-control' required placeholder='Codice a barre' onkeyup='onKeyUpBarcodePaysafeCash(this, event)'/><span class='input-group-addon'><span class='glyphicon glyphicon-barcode' aria-hidden='true'></span></span></div></div>";
        ricDom += "</div>";
        ricDom += "<div class='col col-md-4 form-group'><input readonly id='psCashTransazione' name='idTranEsterno' class='form-control' required placeholder='Transazione'/></div>";
        ricDom += "<div class='col col-md-4 form-group'><input readonly id='psCashImporto' name='importo' class='form-control' required placeholder='Importo'/></div>";
        ricDom += "<div class='col col-md-12'>Ordinante</div>";
        ricDom += "<div class='col col-md-4 form-group nome'><input name='nome' class='form-control' required placeholder='Nome'/></div>";
        ricDom += "<div class='col col-md-4 form-group cognome'><input name='cognome' class='form-control' required placeholder='Cognome'/></div>";
        ricDom += "<div class='col col-md-4 form-group dataNascita'><input name='dataNascita' class='form-control' required placeholder='Data di nascita'/></div>";
        ricDom += "<div class='col col-md-4 form-group tipoDocumento'><input name='tipoDocumento' class='form-control' required placeholder='Tipo documento'/></div>";
        ricDom += "<div class='col col-md-4 form-group numeroDocumento'><input name='numeroDocumento' class='form-control' required placeholder='Numero documento'/></div>";
        ricDom += "<div class='col col-md-4 form-group dataScadenzaDocumento'><input name='dataScadenzaDocumento' required class='form-control' placeholder='Data scadenza documento'/></div>";

      } else if (idProdotto == ID_PROD_SNAI_VOUCHER) {
        //ricDom += inserisciToken();

        ricDom +=  ('<input type="text" style="display:none" /><input type="password" style="display:none">' + "<div class='col col-md-4 form-group token'><input id='svtoken' name='token' onkeyup='onKeyUpSVToken()' autocomplete='new-password' type='password' class='form-control sovrascriviTema' required placeholder='Token'/></div>");

        ricDom += "<div class='col col-md-4 form-group codiceConto'><input id='svconto' name='codiceConto' class='form-control' required placeholder='Codice Conto'/></div>";
        ricDom += "<div class='col col-md-4 form-group codiceVoucher'><div class='input-group'><input id='svvoucher' name='codiceVoucher' class='form-control' required placeholder='Codice Voucher'/><span class='input-group-addon' id='cercaVoucherBtn' onclick='cercaVoucher(event)'><span class='glyphicon glyphicon-search' aria-hidden='true'></span></span></div></div>";
        ricDom += "<div class='pointer' onclick='goToProduct(event, ID_PROD_IPAGOO)' style='font-weight:bold;text-decoration:underline'>Vuoi provare un prelievo IPagoo? Clicca qui!</div>";
        ricDom += "<div id='snaivouchersecretdiv' class='hidden'>";
        ricDom += "<div class='col col-md-6 form-group senderId'>Id mittente: <input id='svsender' name='senderId' class='form-control' value='85' /></div>";
        ricDom += "<div class='col col-md-6 form-group receiverId'>Id destinatario: <input id='svreceiver' name='receiverId' class='form-control' value='55' /></div>";
        ricDom += "<div class='col col-md-4 form-group'><div  class='btn btn-default' onclick='hideSnaiVoucherSecretDiv()'>Hide</div></div>";
        ricDom += "<div class='col col-md-4 form-group'><div  class='btn btn-default' onclick='precompilaSnaiVoucher()'>Precompila</div></div>";
        ricDom += "<div class='col col-md-4 form-group'><div  class='btn btn-default' onclick='resetIdSnaiVoucher()'>Reset sender/receiver</div></div>";
        ricDom += "</div>";
      }
      else if (idProdotto == ID_PROD_IPAGOO) {
        ricDom +=  ('<input type="text" style="display:none" /><input type="password" style="display:none">' + "<div class='col col-md-4 form-group token'><input id='ipagootoken' name='token' onkeyup='onKeyUpIpagooToken()' autocomplete='new-password' type='password' class='form-control sovrascriviTema' required placeholder='Token'/></div>");
        ricDom += "<div class='col col-md-4 form-group email'><input id='ipagooemail'  name='email' class='form-control' required placeholder='Email'/></div>";
        ricDom += "<div class='col col-md-4 form-group importoContainer'><div id='importoVersamento' class='input-group'><input id='ipagooimportoint' name='importo' class='importo' type='number' required placeholder='00'/><span class='comma'>,</span><input id='ipagooimportodec' class='decimali' maxlength=2 onkeyup='onlyNumbers(event)' onblur='formattaDecimali(event)' placeholder='00' type='tel'></div></div>";
        ricDom += "<div class='col col-md-4 form-group hidden'><input id='ipagoooperazione'  name='operazione' class='form-control' required/></div>";
        ricDom += "<div class='col col-md-4 form-group hidden'><input id='ipagooreqid'  name='reqid' class='form-control' /></div>";
        ricDom += "<div id='btRichiediAuthIpagoo' style='width: 100%; display: inline-block; text-align: right'>";
        ricDom += "<div class='btn btn-primary' onclick='richiediAutorizzazioneIpagoo()'  style='margin-right: 15px'>Richiedi autorizzazione</div>";
        ricDom += "</div>";
        ricDom += "<div id='ipagooauthmessage' class='col col-md-12 form-group'></div>";
        ricDom += "<div id='btRefreshStatusIpagoo' style='width: 100%; display: inline-block; text-align: right'>";
        ricDom += "<div class='btn btn-primary' onclick='inviaRequestIpagoo(\"S\")' style='margin-right: 15px'>Aggiorna</div>";
        ricDom += "</div>";

      }
    }

    // opzionali
    if(notifica){

      ricDom += "<div class='col col-md-4 form-group notificaMail'><input class='form-control' name='notifica' placeholder='Notifica Mail (opzionale)'/></div>";

    }
    ricDom += "</div>";
    ricDom += "<div class='dettaglio row'>";
    ricDom += "</div>";
    if(idProdotto != ID_PROD_BOLLETTINI_OLD){
      ricDom += "<div class='confirm'><div class='btn btn-primary col col-md-3' onclick='confermaVendita("+idProdotto+")'>Conferma</div></div>";
    }
    ricDom += "</div>";// chiusura body
    ricDom+="</form>";

    ricDom += "</div>";
    ricDom += "</div>";

    html+=ricDom;
  }
  $("#vendita-container").append(html);
  var h = $("#vendita-container").outerHeight();
  $("#central-content").css("min-height", h);
  abilitaCategorieProdotto();
}

function getExec(codiceFamiglia, idProdotto) {
  var exec = "";
  if(codiceFamiglia == COD_FAM_PIN || codiceFamiglia == COD_FAM_GIFT_CARDS || codiceFamiglia == COD_FAM_RICARICHE_SOLIDALI){
    exec = "execVenditaPin";
  } else if(codiceFamiglia == COD_FAM_RICARICHE ){
    exec = "execVenditaRicarica";
  } else if(codiceFamiglia == COD_FAM_ATTIVAZIONE_SIM) {
    exec = "execAcquistoSim";
  } else if(codiceFamiglia == COD_FAM_PRODOTTI_FINANZIARI){
    exec = execProdottiFinanziari.hasOwnProperty(PREFISSO_PRODOTTO + idProdotto) ? execProdottiFinanziari[PREFISSO_PRODOTTO + idProdotto] : "funzioneNonAttiva";
  } else if(codiceFamiglia == COD_FAM_PRODOTTI_CAMERALI) {
    exec = execProdottiCamerali.hasOwnProperty(PREFISSO_PRODOTTO + idProdotto) ? execProdottiCamerali[PREFISSO_PRODOTTO + idProdotto] : "funzioneNonAttiva";
  } else if(codiceFamiglia == COD_FAM_RICARICA_WALLET) {
    exec = execProdottiRicaricaWallet.hasOwnProperty(PREFISSO_PRODOTTO + idProdotto) ? execProdottiRicaricaWallet[PREFISSO_PRODOTTO + idProdotto] : "funzioneNonAttiva";
  }

  return exec;
}

function setInputFilters() {
  $("input.codiceFiscale").on({
    keyup: function(event) {limitaLunghezzaStringa(event, 16);},
    keypress: function(event) {limitaLettereNumeri(event);}
  });

  $("input.codiceCBill").on({
    keypress: function(event) {readCodiceCBill(event, $(this));}
  });

  $("input.causale").on({
    keypress: function(event) {limitaCaratteriCausale(event, $(this));}
  });

  $("input[name=numeroMavRav]").on({
    keypress: function(event) {limitaNumeri(event, $(this));}
  });
}

function readCodiceCBill(e, input) {
  if ( e.which === 13 ) { //blocca il tasto invio
    e.preventDefault();
  }
  codiceBollettinoChars.push(String.fromCharCode(e.which));
  if (codiceBollettinoPressed == false) {
    setTimeout(function(){ //per leggere il barcode devono essere stati inseriti almeno 20 caratteri in mezzo secondo
      if (codiceBollettinoChars.length >= 20) {
        var barcode = codiceBollettinoChars.join("");
        logOnConsole("[readCodiceCBill] Barcode Scanned: " + barcode);

        // Lettura codice bollettino
        var codice = barcode.substring(2, 20); //elimina il 18 iniziale e prende esattamente 18 caratteri
        logOnConsole("[readCodiceCBill] Codice bollettino: " + codice);
        $(input).val(codice);

        //Lettura importo
        if (codiceBollettinoChars.length >= 46) {
          var importoCent = barcode.substring(36, 46);
          //rimuove gli zeri iniziali
          while(importoCent && importoCent.charAt(0) === '0')
            importoCent = importoCent.substr(1);
          logOnConsole("[readCodiceCBill] Importo in centesimi: " + importoCent);
          if(importoCent) {
            var interi;
            var decimali;
            if(importoCent.length >= 3) { //due decimali e almeno un intero
              interi = importoCent.substring(0, importoCent.length-2);
              $(input).parent().parent().find(".importoContainer").find("input.importo").val(interi);
              decimali = importoCent.substring(importoCent.length-2, importoCent.length);
              $(input).parent().parent().find(".importoContainer").find("input.decimali").val(decimali);
            } else if(importoCent.length === 2) { // due decimali
              $(input).parent().parent().find(".importoContainer").find("input.decimali").val(importoCent);
            } else if(importoCent.length === 1) { // un decimale
              $(input).parent().parent().find(".importoContainer").find("input.decimali").val("0" + importoCent);
            }
          } else {
            logOnConsole("[readCodiceCBill] Importo composto di soli zeri");
          }
        } else {
          logOnConsole("[readCodiceCBill] Non ricevuti abbastanza caratteri per leggere l'importo, numero caratteri: " + codiceBollettinoChars.length);
        }
      } else {
        limitaLunghezzaStringa(e, 18);
      }
      codiceBollettinoChars = [];
      codiceBollettinoPressed = false;
    },500);
  }
  codiceBollettinoPressed = true;

}

function readBarcodeBollettino(e, input) {
  limitaNumeri(e);
  if ( e.which === 13 ) { //blocca il tasto invio
    e.preventDefault();
  }

  var hasChanged = $(e.target).val() != $(e.target).data("previousValue");
  if(hasChanged && input.val().length >= 50) {
    logOnConsole("[readBarcodeBollettino] Barcode Scanned: " + input.val());
    cleanBillerBollettini();
    splitBarcodeBollettino(input.val());
    getBillerBollettino();
  }

//	//Versione che si basa sul tempo di scrittura
//	codiceBollettinoChars.push(String.fromCharCode(e.which));
//	if (codiceBollettinoPressed == false) {
//		setTimeout(function(){ //per leggere il barcode devono essere stati inseriti almeno 50 caratteri in mezzo secondo
//			if (codiceBollettinoChars.length >= 50) {
//				var barcode = codiceBollettinoChars.join("");
//				logOnConsole("[readBarcodeBollettino] Barcode Scanned: " + barcode);
//				splitBarcodeBollettino(barcode);
//			}
//			codiceBollettinoChars = [];
//            codiceBollettinoPressed = false;
//		},500);
//	}
//	codiceBollettinoPressed = true;
}

// Struttura del barcode: 2 lunghezza IV campo, 18 IV campo, 2 lunghezza ccp, 12 ccp, 2 lunghezza importo, 10 importo, 1 lunghezza tipo, 3 tipo
function splitBarcodeBollettino(barcode) {
  if(barcode.length >= 50 && $("div#prodotto_" + ID_PROD_BOLLETTINI).length > 0) {
    var container = $("div#prodotto_" + ID_PROD_BOLLETTINI);

    //Clean dei campi interessati
    container.find("select[name='tipoBollettino']").find("option:selected").removeAttr("selected");
    container.find("input[name='codiceBollettino']").val("");
    container.find("input[name='ccp']").val("");
    container.find(".importoBollettinoContainer").find("input.importo").val("");
    container.find(".importoBollettinoContainer").find("input.decimali").val("");

    // Lettura codice bollettino
    var codice = barcode.substring(2, 20); //elimina il 18 iniziale e prende esattamente 18 caratteri
    logOnConsole("[splitBarcodeBollettino] Codice bollettino: " + codice);
    container.find("input[name='codiceBollettino']").val(codice);

    //Lettura tipo bollettino
    var tipo = barcode.substring(47, 50);
    logOnConsole("[splitBarcodeBollettino] Tipo: " + tipo);
    container.find("select[name='tipoBollettino']").find("option[value='" + tipo + "']").attr("selected", "true");
    abilitaDisabilitaCodiceBollettino();

    // Lettura conto corrente postale
    var ccp = barcode.substring(22, 34);
    logOnConsole("[splitBarcodeBollettino] Conto corrente postale: " + ccp);
    container.find("input[name='ccp']").val(ccp);
    container.find("input[name='ccp']").data('previousValue', ccp);

    //Lettura importo
    if (barcode.length >= 46) {
      var importoCent = barcode.substring(36, 46);
      //rimuove gli zeri iniziali
      while(importoCent && importoCent.charAt(0) === '0')
        importoCent = importoCent.substr(1);
      logOnConsole("[splitBarcodeBollettino] Importo in centesimi: " + importoCent);
      if(importoCent) {
        var interi;
        var decimali;
        if(importoCent.length >= 3) { //due decimali e almeno un intero
          interi = importoCent.substring(0, importoCent.length-2);
          container.find(".importoBollettinoContainer").find("input.importo").val(interi);
          decimali = importoCent.substring(importoCent.length-2, importoCent.length);
          container.find(".importoBollettinoContainer").find("input.decimali").val(decimali);
        } else if(importoCent.length === 2) { // due decimali
          container.find(".importoBollettinoContainer").find("input.decimali").val(importoCent);
        } else if(importoCent.length === 1) { // un decimale
          container.find(".importoBollettinoContainer").find("input.decimali").val("0" + importoCent);
        }
      } else {
        logOnConsole("[splitBarcodeBollettino] Importo composto di soli zeri");
      }
    } else {
      logOnConsole("[splitBarcodeBollettino] Non ricevuti abbastanza caratteri per leggere l'importo, numero caratteri: " + codiceBollettinoChars.length);
    }

  }
}

function onTipoBollettinoChanged() {
  abilitaDisabilitaCodiceBollettino();
  getBillerBollettinoFromTipoBollettino();
}

function abilitaDisabilitaCodiceBollettino() {
  var container = $("div#prodotto_" + ID_PROD_BOLLETTINI);
  var tipoSelected = container.find("select[name='tipoBollettino']").val();
  var codiceBollettinoInput = container.find("input[name='codiceBollettino']");
  if(tipoSelected && (tipoSelected == "896" || tipoSelected == "674")) {
    codiceBollettinoInput.removeAttr("disabled");
    codiceBollettinoInput.attr("required", true);
  } else {
    codiceBollettinoInput.attr("disabled", "disabled");
    codiceBollettinoInput.removeAttr("required");
    codiceBollettinoInput.val("");
  }
}

function onTipoVeicoloChanged() {
  var container = $("div#prodotto_" + ID_PROD_BOLLOAUTO);
  var tt = container.find("select[name='tipoVeicolo'] option:selected").attr("data-tt")
  var targaInput = container.find("input[name='targa']");
  var telaioInput = container.find("input[name='telaio']");
  if(tt == "select") {
    telaioInput.attr("disabled", "disabled");
    telaioInput.removeAttr("required");
    telaioInput.val("");
    targaInput.attr("disabled", "disabled");
    targaInput.removeAttr("required");
    targaInput.val("");
  } else if(tt == "targa") {
    targaInput.removeAttr("disabled");
    targaInput.attr("required", true);
    telaioInput.attr("disabled", "disabled");
    telaioInput.removeAttr("required");
    telaioInput.val("");
  } else if(tt == "telaio"){
    telaioInput.removeAttr("disabled");
    telaioInput.attr("required", true);
    targaInput.attr("disabled", "disabled");
    targaInput.removeAttr("required");
    targaInput.val("");
  }
}

function showIpagooAuth() {
  resetIpagooForm();

  $("#btRichiediAuthIpagoo").show();
  $("#ipagoooperazione").val("A");
}

function showIpagooPending(reqId) {
  resetIpagooForm();

  $("#btRefreshStatusIpagoo").show();
  $('#ipagooauthmessage').text("La richiesta è ancora in sospeso");
  $("#ipagoooperazione").val("S");
  $('#ipagooreqid').val(reqId);
}

function showIpagooConfirmed(reqId) {
  resetIpagooForm();
  $("#ipagoooperazione").val("C");
  $('#ipagooreqid').val(reqId);

  $("#venditaPaymatInCorso").val(0);
  eseguiVendita(ID_PROD_IPAGOO, COD_FAM_PRODOTTI_FINANZIARI);
  showIpagooAuth();
}

function showIpagooDenied(reqId) {
  resetIpagooForm();
  $('#ipagooauthmessage').text("La richiesta è stata rifiutata");
}

function showIpagooError() {
  resetIpagooForm();
  $('#ipagooauthmessage').text("Errore durante l'elaborazione");
}

function resetIpagooForm() {
  $("#prodotto_" + ID_PROD_IPAGOO + " .confirm").hide();
  $("#btRichiediAuthIpagoo").hide();
  $("#btRefreshStatusIpagoo").hide();

  $("#ipagoooperazione").val("");
  $('#ipagooreqid').val("");
  $('#ipagooauthmessage').text("");

//	$("#ipagootoken").show();
//	$("#ipagooemail").show();
//	$("#ipagooimportoint").show();
//	$("#ipagooimportodec").show();

//	$("#ipagootoken").hide();
//	$("#ipagooemail").hide();
//	$("#ipagooimportoint").hide();
//	$("#ipagooimportodec").hide();
}

function onKeyUpIpagooToken() {
  if($("#ipagootoken").val().toLowerCase() == "prec") {
    precompilaIpagoo();
  }
}

function precompilaIpagoo() {
  $("#ipagooemail").val("fvv75338@loapq.com");
  $("#ipagooimportoint").val("10");
  $("#ipagooimportodec").val("00");
}

function onKeyUpSVToken() {
  if($("#svtoken").val().toLowerCase() == "show") {
    showSnaiVoucherSecretDiv();
  }

  if($("#svtoken").val().toLowerCase() == "prec") {
    precompilaSnaiVoucher();
  }
}

function precompilaSnaiVoucher() {
  $("#svconto").val("6031980011112525");
  $("#svvoucher").val("41571");
}

function resetIdSnaiVoucher() {
  $("#svsender").val("85");
  $("#svreceiver").val("55");
}

function showSnaiVoucherSecretDiv() {
  $("#snaivouchersecretdiv").removeClass("hidden");
}

function hideSnaiVoucherSecretDiv() {
  $("#snaivouchersecretdiv").addClass("hidden");
}

function onBlurCampoAziendaCbill(input, event){
  if(browserAgent != "isIE"){
    if(event.relatedTarget && event.relatedTarget.tagName == "LI"){
      //nonchiudere
    }else{
      if($(input).attr("data-codsia") == "" || $(input).attr("data-codsia") == undefined){

        input.value = input.value ? input.value+ " [Azienda sconosciuta]" : "";
        $("#listaAziende").remove();
      }
    }
  }

}
function onFocusCampoAziendaCbill(input){
  input.value = "";
  $(input).attr("data-codsia", "");
}
function checkIfEnterKey(event){
  if(event.which == 13){
    fillAzienda(event.target);
  }
}
function inserisciToken(mdWidth){
  if(!mdWidth) mdWidth = "4";

  var str = "";
  //WORKAROUND BUG CHROME AUTOCOMPLETE OFF
  str += '<input type="text" style="display:none" />';
  str += '<input type="password" style="display:none">';
  //
  str += "<div class='col col-md-" + mdWidth + " form-group token'><input name='token' autocomplete='new-password' type='password' class='form-control sovrascriviTema' required placeholder='Token'/></div>";
  return str;
}

function buildFormGroup(mdWidth, content, customContainerClass) {
  return "<div class='col col-md-" + mdWidth + " form-group" + (customContainerClass ? " " + customContainerClass : "") + "'>" + content + "</div>";
}

function sortProdotti(){
  var domaArray = [];
  $(".categoria-prodotto-item").sort(function(a,b){
    if(parseInt($(a).attr("data-index")) < parseInt($(b).attr("data-index"))) {
      return -1;
    } else {
      return 1;
    }
  }).each(function() {
    domaArray.push(this);
  });
  $(".categoria-prodotto-item").remove();
  for(var i =0; i < domaArray.length; i++){
    $("#vendita-container").append(domaArray[i]);
  }
  abilitaCategorieProdotto();
}

function limitaPostePay(event){
  if(parseInt($(event.target).val()) > 997){
    $(event.target).val(997);
  }
}
function limitaAnno(event){
  if($(event.target).val().length >= 4){
    var d = new Date();
    d.setFullYear(d.getFullYear() - 5);
    var annoMinimo = d.getFullYear();
    if(parseInt($(event.target).val()) < annoMinimo){
      $(event.target).val(annoMinimo);
    }
  }

}
function onlyNumbers(event){
  if(isNaN(event.target.value)){
    event.target.value = event.target.value.slice(0,event.target.value.length-1);
  }
}
function formattaDecimali(event){
  var v = $(event.target).val().toString();
  if(v.length == 0){
    $(event.target).val("00");
  }else if(v.length == 1){
    $(event.target).val(v+"0");
  }
}
function checkIfNumber(event){
  var val = $(event.target).val();
  if(isNaN(val)){
    $(event.target).parents(".form-group").addClass("has-error");
  }else{
    $(event.target).parents(".form-group").removeClass("has-error");
  }
}
function limitaLunghezzaStringa(event, nCaratteri){
  if(event.target.value.length >= nCaratteri){
    event.target.value = event.target.value.slice(0, nCaratteri);
  }
}
function goToProduct(event, idProdotto){
  var el = $(".categoria-prodotto-item[id='prodotto_"+idProdotto+"']");
  $(el).trigger("click");
  event.stopPropagation();
  fireSearchFromVisuraOrCompanyCardCamerali(true, idProdotto);
}

function confermaGoToRicerca(idProdottoCorrente) {
  goToProduct(event, ID_PROD_RICERCA_AZIENDA);
  fireSearchFromVisuraOrCompanyCardCamerali(true, idProdottoCorrente);
  removeCustomDialog();
}

function goToRicercaFromVisuraOrCompanyCard(idProdottoCorrente) {
  var msgDialog = "Per procedere con l'acquisto del prodotto &egrave; necessario effettuare la ricerca dell'impresa";
  var dialogParams = {
    idContent : "confermaApriRicercaFromVisura",
    title : "Ricerca Impresa per procedere con l'acquisto del prodotto",
    msg : msgDialog,
    action : "confermaGoToRicerca(" + idProdottoCorrente + ")",
    confirmText : 'RICERCA IMPRESA',
    annullaText : 'ANNULLA'
  }
  openGenericDialog(dialogParams);
}

function retrieveSelectedIdProdotto() {
  try {
    var tempIdProdotto = $("div[id^='" + PREFISSO_PRODOTTO + "'].active").attr("id").replace(PREFISSO_PRODOTTO,'');
    if(!isNaN(tempIdProdotto)) {
      return parseInt(tempIdProdotto);
    }
    return null;
  } catch(error) {
    logOnConsole("error retrieveSelectedIdProdotto msg:" + error.message);
    return null;
  }
}

function fireSearchFromVisuraOrCompanyCardCamerali(fireSearch, idProdotto) {
  if(fireSearch) {
    var validateIdProdotto = true;
    if(!idProdotto) {
      idProdotto = retrieveSelectedIdProdotto();
    }
    if(idProdotto) {
      if(idProdotto == ID_PROD_VISURA_ORDINARIA) {
        isSearchFromVisuraCameraliOrd = fireSearch;
        isSearchFromVisuraCameraliSto = !fireSearch;
        isSearchFromCompanyCardCameraliOrd = !fireSearch;
        isSearchFromCompanyCardCameraliSto = !fireSearch;
      } else if(idProdotto == ID_PROD_VISURA_STORICA) {
        isSearchFromVisuraCameraliSto = fireSearch;
        isSearchFromVisuraCameraliOrd = !fireSearch;
        isSearchFromCompanyCardCameraliOrd = !fireSearch;
        isSearchFromCompanyCardCameraliSto = !fireSearch;
      } else if(idProdotto == ID_PROD_COMPANY_CARD_ORDINARIA) {
        isSearchFromCompanyCardCameraliOrd = fireSearch;
        isSearchFromCompanyCardCameraliSto = !fireSearch;
        isSearchFromVisuraCameraliOrd = !fireSearch;
        isSearchFromVisuraCameraliSto = !fireSearch;
      } else if(idProdotto == ID_PROD_COMPANY_CARD_STORICA) {
        isSearchFromCompanyCardCameraliSto = fireSearch;
        isSearchFromCompanyCardCameraliOrd = !fireSearch;
        isSearchFromVisuraCameraliOrd = !fireSearch;
        isSearchFromVisuraCameraliSto = !fireSearch;
      } else {
        isSearchFromVisuraCameraliOrd = !fireSearch;
        isSearchFromVisuraCameraliSto = !fireSearch;
        isSearchFromCompanyCardCameraliOrd = !fireSearch;
        isSearchFromCompanyCardCameraliSto = !fireSearch;
      }
    } else {
      isSearchFromVisuraCameraliOrd = !fireSearch;
      isSearchFromVisuraCameraliSto = !fireSearch;
      isSearchFromCompanyCardCameraliOrd = !fireSearch;
      isSearchFromCompanyCardCameraliSto = !fireSearch;
    }
  } else {
    isSearchFromVisuraCameraliOrd = fireSearch;
    isSearchFromVisuraCameraliSto = fireSearch;
    isSearchFromCompanyCardCameraliOrd = fireSearch;
    isSearchFromCompanyCardCameraliSto = fireSearch;
  }
}

function abilitaCategorieProdotto(){
  $(".categoria-prodotto-item").click(function(event){

    var id_prod = $(this).attr("id").replace("prodotto_","");

    if($(this).hasClass("active")){

      if(id_prod == ID_PROD_CBILL){
        if(browserAgent == "isIE" && event.target != document.getElementById("listaAziende")){
          var input = $("#autocompleteAzienda");
          if($(input).attr("data-codsia") == "" || $(input).attr("data-codsia") == undefined){
            input.value = input.value ? input.value+ " [Azienda sconosciuta]" : "";
            $("#listaAziende").remove();
          }
        }
      }
    }else{

      closeAndClearAll();
      $(this).addClass("active");
      $(this).find(".panel-heading").append("<span class='annullo btn btn-danger pointer' onclick='annullaVendita(event)'>X</span>");
      gestisciSpaziGriglia.call(this);
      if($.inArray(parseInt(id_prod), PRODOTTI_NO_TAGLI) < 0){
        getTagli(id_prod, this);
      }
      var offTop = $(this).offset().top;
      var h = offTop - 250;
      $('body').stop().animate({
        scrollTop:h
      }, 500, function(){

      });
      $(this).find("input").each(function(){
        $(this).on('focus', function(){
          $(this).attr("data-placeholder", $(this).attr("placeholder"));
          $(this).attr("placeholder", "");
        })
        $(this).on('blur', function(){
          $(this).attr("placeholder", $(this).attr("data-placeholder"));
        })
      })

      $('[data-toggle="tooltip"]').tooltip();
//		    validationObserver(this);
      if(id_prod == ID_PROD_POSTEPAY){
        abilitaPPMobileCamera();
        if(!(isSmartSolution && operatingSystem == "isLinux")) {
          abilitaUploadFiles();
          abilitaWebcam();
        }
      }

      if(id_prod == ID_PROD_ATTIVAZIONE_SIM_ILIAD) {
        abilitaPPMobileCamera();
        if(!(isSmartSolution && operatingSystem == "isLinux")) {
          abilitaUploadFiles();
//					abilitaWebcam();
        }
      }

      if(id_prod == ID_PROD_SNAI_VOUCHER) {
        resetIdSnaiVoucher();
        hideSnaiVoucherSecretDiv();
      }

      if(id_prod == ID_PROD_IPAGOO) {
        showIpagooAuth();
      }

      if(id_prod == ID_PROD_BOLLETTINI) {
        $("input.barcodeBollettino").on({
          keyup: function(event) {readBarcodeBollettino(event, $(this));},
          keydown: function(event) {
            $(event.target).data('previousValue', $(event.target).val());
          },
        });

        $("input[name='ccp'").on({
          blur: function(event) {
            var previousValue = $(event.target).data("previousValue") !== null && $(event.target).data("previousValue") !== undefined ? $(event.target).data("previousValue") : "";
            if($(event.target).val() != previousValue) {
              getBillerBollettinoFromCcp();
            }
            $(event.target).data('previousValue', $(event.target).val());
          }
        });
      }

      if(id_prod == ID_PROD_PAGOPA) {

        $("#pagoPaQrCodeScan").on({
          keyup: function(event) {readQrCodePagoPaFromInput(event, $(this));},
          keydown: function(event) {
            $(event.target).data('previousValue', $(event.target).val());
          },
        });
      }
      if(id_prod == ID_PROD_VISURA_ORDINARIA || id_prod == ID_PROD_VISURA_STORICA || id_prod == ID_PROD_COMPANY_CARD_ORDINARIA || id_prod == ID_PROD_COMPANY_CARD_STORICA) {
        fireSearchFromVisuraOrCompanyCardCamerali(true, id_prod);
      }

      var formProdotto = $(this);
      aggiornaFrontMonitor(formProdotto);
      formProdotto.find(".fmField").on("change", function() {
        aggiornaFrontMonitor(formProdotto);
      })

      setInputFilters();
      event.stopPropagation();
    }
  })
}

function gestisciSpaziGriglia(){

  $(".categoria-prodotto-item").removeClass("animated");

  var precedenti = $(this).prevAll(".categoria-prodotto-item");
  var successivi = $(this).nextAll();
  var fattore = 0;
  if(window.innerWidth<=993 && !isMultiple(precedenti.length, 2)){
    fattore = 2

  }else if( !isMultiple(precedenti.length, 4)){
    fattore = 4
  }
  if(fattore > 0){
    var eccedenti = fattore - (nextMultiple(precedenti.length, fattore) - precedenti.length);
    for(var i= 0; i < eccedenti; i++){
      var el = precedenti[i];
      $(this).after(el);
    }
  }
}

function openSubDialog(event){

  var subDialog = $(event.target).parents(".formProdotto").find(".subDialogForm");
  if(event.target.checked == true){
    $(subDialog).removeClass("hidden");
    $(event.target).attr("required", true);
    $(subDialog).find("input").attr("required", true);
  }else{
    $(subDialog).addClass("hidden");
    $(event.target).attr("required", false);
    $(subDialog).find("input").removeAttr("required", false);
  }

  event.stopPropagation();
}

function showInfoSIMBarcode() {
  var text = "<div>Puoi scansionare il codice della SIM per compilare automaticamente il campo.<br /><br /><img style=\"max-width: 100%; max-height: 100%\" src=\"/sites/default/files/scansiliad.png\"></div>";
  openGenericInfoDialog(text, "Scansione codice a barre");
}

function showInfoBollettiniBarcode() {
  var text = "<div>Se stai pagando un Bollettino Premarcato fatturatore (896), puoi scansionare il codice a barre presente sul bollettino per compilare automaticamente tutti i campi richiesti.<br /><br /><img style=\"max-width: 100%; max-height: 100%\" src=\"/sites/default/files/barcodescan.png\"></div>";
  openGenericInfoDialog(text, "Scansione codice a barre");
}
function showInfoVoucherBarcode() {
  var text = "<div>Puoi scansionare il codice a barre presente sul voucher per effettuare la ricerca.<br /><br /><img style=\"max-width: 100%; max-height: 100%\" src=\"/sites/default/files/barcodescan2.png\"></div>";
  openGenericInfoDialog(text, "Scansione codice a barre");
}

function showInfoBollettiniPartitaIVA() {
  var text = "<div>Inserire la partita IVA seguendo il formato dell'Unione Europea:<br />per una partita IVA italiana, inserire il codice IT seguito da 11 cifre (es. <strong>IT01729640464</strong>)</div>";
  openGenericInfoDialog(text, "Compilazione partita IVA");
}

function showInfoCodiceFiscale() {
  var text = "Puoi scansionare il codice a barre della tessera sanitaria per acquisire il codice fiscale<br /><br /><img style=\"max-width: 75%;\" src=\"/sites/default/files/tessera-sanitaria.jpg\">";
  openGenericInfoDialog(text, "Scansione codice fiscale");
}

function showInfoEmailDestinatario() {
  var text = "Inserendo l'indirizzo email del destinatario, arriverà al cliente una mail con il link dove poter scaricare il documento richiesto. Se non compili questo campo il cliente potrà scaricare il documento scansionando il QR code presente nello scontrino";
  openGenericInfoDialog(text, "Email Destinatario");
}

function showInfoCodiceConto() {
  openGenericInfoDialog(MSG_EPIPOLI_WALLET_INFO_TELEFONO_CONTO, "Valorizzare il numero di telefono o il codice conto");
}

function getSpanInfoCodiceConto() {
  return "<span class='input-group-addon form-info-field' onclick='showInfoCodiceConto()'><span class='glyphicon glyphicon-info-sign' aria-hidden='true'></span></span>";
}

function getSpanInfoCodiceFiscale() {
  return "<span class='input-group-addon form-info-field' onclick='showInfoCodiceFiscale()'><span class='glyphicon glyphicon-info-sign' aria-hidden='true'></span></span>";
}

function getSpanEmailDestinatario() {
  return "<span class='input-group-addon form-info-field' onclick='showInfoEmailDestinatario()'><span class='glyphicon glyphicon-info-sign' aria-hidden='true'></span></span>";
}

function getBillerBollettinoFromTipoBollettino() {
  // E' cambiato il tipo bollettino, occorre azzerare il biller
  chiudiTooltip();
  $("#prodotto_" + ID_PROD_BOLLETTINI).find("select[name='tipoBollettino']").parents(".form-group").removeClass("has-warning");
  $("#prodotto_" + ID_PROD_BOLLETTINI).find("select[name='tipoBollettino']").parents(".form-group").removeClass("has-error");
  cleanBillerBollettini();

  // Se il conto corrente e' stato valorizzato parte la ricerca, altrimenti si sposta il focus per far compilare il conto corrente
  var ccp = $("#prodotto_" + ID_PROD_BOLLETTINI).find("input[name='ccp']");
  if(ccp.val()) {
    getBillerBollettino();
  } else {
    ccp.parents(".form-group").addClass("has-warning");
    apriWarningTooltip($("#prodotto_" + ID_PROD_BOLLETTINI), "Compila questo campo");
    ccp.focus();
  }
}

function getBillerBollettinoFromCcp() {
  // E' cambiato il conto corrente o e' stata forzata la ricerca, occorre azzerare il biller
  chiudiTooltip();
  $("#prodotto_" + ID_PROD_BOLLETTINI).find("input[name='ccp']").parents(".form-group").removeClass("has-warning");
  $("#prodotto_" + ID_PROD_BOLLETTINI).find("input[name='ccp']").parents(".form-group").removeClass("has-error");
  cleanBillerBollettini();

  // Se il tipo bollettino e' stato selezionato parte la ricerca, altrimenti si sposta il focus per far selezionare il tipo bollettino
  var tipoBollettino = $("#prodotto_" + ID_PROD_BOLLETTINI).find("select[name='tipoBollettino']");
  if(tipoBollettino.children("option:selected").val()) {
    getBillerBollettino();
  } else {
    tipoBollettino.parents(".form-group").addClass("has-warning");
    apriWarningTooltip($("#prodotto_" + ID_PROD_BOLLETTINI), "Compila questo campo");
    tipoBollettino.focus();
  }
}

var recuperoBillerInCorso = false;
function getBillerBollettino() {
  if(!recuperoBillerInCorso) {
    var ccp = $("#prodotto_" + ID_PROD_BOLLETTINI).find("input[name='ccp']").val();
    var tipoBollettino = $("#prodotto_" + ID_PROD_BOLLETTINI).find("select[name='tipoBollettino']").children("option:selected").val();
    if (ccp && tipoBollettino) {
      recuperoBillerInCorso = true;
      $("#prodotto_" + ID_PROD_BOLLETTINI + " .buttonWithLoading").addClass("inCorso");
      var reqUrl = "/src/web/action/vendita/getInfoBollettino.php?action=getBiller&ccp=" + ccp + "&tipoBollettino=" + tipoBollettino;
      customAjaxCall(reqUrl, ID_PROD_BOLLETTINI, fillBillerBollettino, fillBillerBollettinoError, {"ccp": ccp, "tipoBollettino": tipoBollettino} );
    } else {
      logOnConsole("[getBillerBollettino] Parametri mancanti per il recupero - ccp=[" + ccp + "] tipoBollettino=[" + tipoBollettino + "]");
    }
  } else {
    logOnConsole("[getBillerBollettino] Recupero biller in corso - Richiesta corrente bloccata");
  }
}

function cleanBillerBollettini() {
  $("#bollettini_biller_container").addClass("hidden");
  $("#bollettini_biller_name").html("");
  $("#bollettini_biller_additional_fields").html("");
  $("#prodotto_" + ID_PROD_BOLLETTINI).find("input[name='causale']").removeAttr("disabled");
}

function fillBillerBollettinoError() {
  recuperoBillerInCorso = false;
  $(".buttonWithLoading.inCorso").removeClass("inCorso");
}

function fillBillerBollettino(idProdotto, parsedData, params) {
  recuperoBillerInCorso = false;
  $(".buttonWithLoading.inCorso").removeClass("inCorso");
  logOnConsole("Biller:");
  logOnConsole(parsedData);
  if(parsedData && parsedData.dati) {
    $("#bollettini_biller_name").html(buildFormGroup(12, "<div class='fmField fmGenericField' data-placeholder='Beneficiario'>" + parsedData.dati.nome + "</div>"));
    $("#bollettini_biller_container").removeClass("hidden");
    var dati = parsedData.dati;

    //quando l'elemento è unico non viene restituito un array ma un oggetto singolo
    //con questa pezza ripristino l'array che si aspetta il codice più in basso
    if(dati.campiVariabiliBollettinoCliente && !dati.campiVariabiliBollettinoCliente.length){
      var arr = new Array();
      arr[0] = dati.campiVariabiliBollettinoCliente;
      dati.campiVariabiliBollettinoCliente = arr;
    }

    if(dati.campiVariabiliBollettinoEnte && !dati.campiVariabiliBollettinoEnte.length){
      var arr = new Array();
      arr[0] = dati.campiVariabiliBollettinoEnte;
      dati.campiVariabiliBollettinoEnte = arr;
    }

    if(dati.campiVariabiliBollettinoCliente && dati.campiVariabiliBollettinoCliente.length > 0 || dati.campiVariabiliBollettinoEnte && dati.campiVariabiliBollettinoEnte.length > 0) {
      var content = "";
      if(dati.memoCausale) content += "<div class='col-xs-12 memoCausale'>" + dati.memoCausale + "</div>";
      var additionalFieldsCliente = [];
      var additionalFieldsEnte = [];
      // campiVariabiliBollettinoCliente = CUSTOM TAG
      if(dati.campiVariabiliBollettinoCliente && dati.campiVariabiliBollettinoCliente.length > 0) {
        for(var i=0; i<dati.campiVariabiliBollettinoCliente.length; i++) {
          content += buildBillerBollettinoAdditionalField(dati.campiVariabiliBollettinoCliente[i]);
          additionalFieldsCliente.push(JSON.stringify(dati.campiVariabiliBollettinoCliente[i]))
        }
      }
      // campiVariabiliBollettinoEnte = META TAG
      if(dati.campiVariabiliBollettinoEnte && dati.campiVariabiliBollettinoEnte.length > 0) {
        for(var i=0; i<dati.campiVariabiliBollettinoEnte.length; i++) {
          content += buildBillerBollettinoAdditionalField(dati.campiVariabiliBollettinoEnte[i]);
          additionalFieldsEnte.push(JSON.stringify(dati.campiVariabiliBollettinoEnte[i]))
        }
      }
      content += "<input name='additionalFieldsCliente' type='hidden' value=\"" + htmlEscape(JSON.stringify(additionalFieldsCliente)) + "\">";
      content += "<input name='additionalFieldsEnte' type='hidden' value=\"" + htmlEscape(JSON.stringify(additionalFieldsEnte)) + "\">";
      $("#bollettini_biller_additional_fields").html(content);

      // solo in presenza di custom tag viene disabilitata la causale libera
      if(dati.campiVariabiliBollettinoCliente && dati.campiVariabiliBollettinoCliente.length > 0) {
        $("div#prodotto_" + ID_PROD_BOLLETTINI).find("input[name='causale']").attr("disabled", "disabled");
        $("div#prodotto_" + ID_PROD_BOLLETTINI).find("input[name='causale']").val("");
      }

      $(".bollettini-biller-additional-field input[data-type='1']").on({
        keypress: function(event) {limitaNumeri(event);}
      });

      $(".bollettini-biller-additional-field input[data-type='2']").on({
        keypress: function(event) {limitaCaratteriData(event);}
      });
    }

    aggiornaFrontMonitor($("div#prodotto_" + ID_PROD_BOLLETTINI));
  } else {
    openRichiestatInserimentoBillerDialog(params);
  }

}

function buildBillerBollettinoAdditionalField(field) {
  // 0 stringa
  // 1 intero
  // 2 data
  var lunghezza = field.lunghezza != null ? "maxlength='" + field.lunghezza + "' " : "";
  var tipo = field.tipo != null ? field.tipo : -1;
  var placeholder = "";
  if(tipo == 2) { //date
    placeholder = "placeholder='" + (field.lunghezza && field.lunghezza >= 10 ? 'GG/MM/AAAA' : 'GG/MM/AA') + "'";
  }
  var content = "<div class='col col-md-6 form-group bollettini-biller-additional-field'><div class='col col-md-6'><label>" + field.descrizione + "</label></div><div class='col col-md-6'><input name='cvb_" + encodeURIComponent(field.nome) + "' " + lunghezza + "class='form-control' data-type='" + tipo + "' " + placeholder + " ></div></div>";
  return content;
}

function openRichiestatInserimentoBillerDialog(params) {
  var dialogParams = {
    idContent : "inserimentoBillerDialog",
    title : "Richiesta inserimento nuovo beneficiario (biller)",
    msg : buildInserimentoBillerDialog(params),
    confirmText : "Invia richiesta",
    annullaText : "Annulla",
    action : "showInserimentoBillerConfirmDialog()",
  }
  $(".venditaDialog").remove();
  $(".custom-modal").remove();
  openGenericDialog(dialogParams);
}

function buildInserimentoBillerDialog(params) {
  var content = "";
  content += "<div class=\"col-xs-12 input-group\">";
  content += "<p class='text-center' style=\"margin-bottom: 20px\">Il numero di conto corrente \"" + params.ccp + "\" non risulta censito tra i possibili beneficiari.<br />Compilare i campi seguenti per avanzare una richiesta di inserimento di un nuovo beneficiario.<br />La richiesta dovr&agrave; essere approvata prima di poter effettuare il pagamento.</p>";
  content += "<form id=\"insertBillerForm\">";
  content += "<div class=\"col-xs-12\">";
  content += "<div class=\"col-xs-6 text-right\"><span class=\"asterisk\"><label for=\"newBillerDenominazione\">Denominazione o ragione sociale</label></span></div>";
  content += "<div class=\"col-xs-6 form-group\"><input name=\'newBillerDenominazione\' class='form-control' required /></div>";
  content += "</div>";
  content += "<div class=\"col-xs-12\">";
  content += "<div class=\"col-xs-6 text-right\"><span class=\"asterisk\"><label for=\"newBillerCcp\">Numero conto corrente postale</label></span></div>";
  content += "<div class=\"col-xs-6 form-group\"><input name=\'newBillerCcp\' class='form-control' required value=\"" + params.ccp + "\" /></div>";
  content += "</div>";
  content += "<div class=\"col-xs-12\">";
  content += "<div class=\"col-xs-6 text-right\"><span class=\"asterisk\"><label for=\"newBillerNote\">Informazioni utili</label></span><div>(es. recapiti telefonici, persone da contattare)</div></div>";
  content += "<div class=\"col-xs-6 form-group\"><textarea name=\'newBillerNote\' class='form-control' rows='5' required /></div>";
  content += "</div>";
  content += "</form>";
  content += "</div>";
  return content;
}

function showInserimentoBillerConfirmDialog() {

  if(validateFormInserimentoBiller()) {
    var denominazione = $("#insertBillerForm input[name='newBillerDenominazione']").val();
    var ccp = $("#insertBillerForm input[name='newBillerCcp']").val();
    var note = $("#insertBillerForm textarea[name='newBillerNote']").val();
    var mex = "<div id=\"controllaDati\" class=\"modal-body\">" + getRowConferma("Denominazione", denominazione) + getRowConferma("Numero conto corrente", ccp) + getRowConferma("Informazioni utili", note) + "</div>";
    denominazione = encodeURIComponent(htmlEscape(denominazione));
    note = encodeURIComponent(htmlEscape(note));
    var action = "salvaInserimentoBiller('" + denominazione + "', '" + ccp + "', '" + note + "')";
    //logOnConsole(action);
    var dialogParams = {
      idContent : "inserimentoBillerDialog",
      title : "Controlla i dati e conferma",
      msg : mex,
      confirmText : "Conferma",
      annullaText : "Annulla",
      action : action,
    }
    $(".venditaDialog").remove();
    $(".custom-modal").remove();
    openGenericDialog(dialogParams);
  } else {
    apriTooltipCampiObbligatori($("#insertBillerForm"));
  }
}

function validateFormInserimentoBiller() {
  var validated = true;
  var formName = "#insertBillerForm";
  var formData = $(formName).formToArray();
  var invalidFields = [];
  if(formData) {
    for (var i = 0; i < formData.length; i++) {
      var field = formData[i];
      $(formName).find("input[name='" + field.name + "']").parents(".form-group").removeClass("has-error");
      if (field.required && field.value == "") {
        invalidFields.push(field.name);
      }
    }
    if(invalidFields.length > 0) {
      validated = false;
      for (var i=0; i<invalidFields.length; i++)
        $(formName).find("input[name='" + invalidFields[i] + "']").parents(".form-group").addClass("has-error");
    }
  }
  return validated;
}
function salvaInserimentoBiller(denominazione, ccp, note) {
  denominazione = encodeURIComponent(htmlUnescape(decodeURIComponent(denominazione)));
  note = encodeURIComponent(htmlUnescape(decodeURIComponent(note)));
  addBgFrame();
  var reqUrl = "/src/web/action/vendita/execInsertBillerBollettino.php?denominazione=" + denominazione + "&ccp=" + ccp + "&note=" + note;
  customAjaxCall(reqUrl, ID_PROD_BOLLETTINI, onBillerBollettinoRequestInserted, genericErrorCallback, null);
}

function onBillerBollettinoRequestInserted() {
  openGenericInfoDialog("La richiesta di inserimento &egrave; stata inoltrata. Sar&agrave; sottoposta a valutazione.");
}

function switchTipoPagatoreBollettino() {
  $el = $("#venditaForm_" + ID_PROD_BOLLETTINI);
  switchTipoPagatore($el, "Bollettino");
}

function switchTipoPagatoreBollettinoFreccia() {
  $el = $("#venditaForm_" + ID_PROD_BOLLETTINO_FRECCIA);
  switchTipoPagatore($el, "BollettinoFreccia");
}

function switchTipoPagatoreMavRav() {
  $el = $("#venditaForm_" + ID_PROD_MAV_RAV);
  switchTipoPagatore($el, "MavRav");
}

function switchTipoPagatorePagoPa() {
  $el = $("#venditaForm_" + ID_PROD_PAGOPA);
  switchTipoPagatore($el, "PagoPa");
}

function switchTipoPagatoreBolloAuto() {
  $el = $("#venditaForm_" + ID_PROD_BOLLOAUTO);
  switchTipoPagatore($el, "BolloAuto");
}

function switchTipoPagatore($el, tipoProdotto) {
  if($el.find("input[name='tipoPagatore']:checked").val() == "pf") {
    $el.find("#panelPagatore" + tipoProdotto + "Pg").addClass("hidden");
    $el.find("#panelPagatore" + tipoProdotto + "Pf").removeClass("hidden");
    $el.find("input[name='nomePagatore']").attr("required", true);
    $el.find("input[name='cognomePagatore']").attr("required", true);
    $el.find("input[name='codFiscalePagatore']").attr("required", true);
    $el.find("input[name='denominazionePagatore']").removeAttr("required").val("");
    $el.find("input[name='partitaIvaPagatore']").removeAttr("required").val("");
  } else {
    $el.find("#panelPagatore" + tipoProdotto + "Pf").addClass("hidden");
    $el.find("#panelPagatore" + tipoProdotto + "Pg").removeClass("hidden");
    $el.find("input[name='nomePagatore']").removeAttr("required").val("");
    $el.find("input[name='cognomePagatore']").removeAttr("required").val("");
    $el.find("input[name='codFiscalePagatore']").removeAttr("required").val("");
    $el.find("input[name='denominazionePagatore']").attr("required", true);
    $el.find("input[name='partitaIvaPagatore']").attr("required", true);
  }
}

function onEsecutoreBollettinoClick(event) {
  var subDialog = $(event.target).parents(".formProdotto").find(".panel-esecutore-bollettino");
  var requiredFields = getEsecutoreBollettinoRequiredFields();
  onEsecutoreClick(event, subDialog, requiredFields);
}

function getEsecutoreBollettinoRequiredFields() {
  return ["nomeEsecutore", "cognomeEsecutore", "codFiscaleEsecutore", "indirizzoEsecutore", "capEsecutore", "cittaEsecutore", "provinciaEsecutore", "nazioneEsecutore", "telefonoEsecutore", "emailEsecutore", "tipoDocumentoEsecutore", "numeroDocumentoEsecutore", "enteDocumentoEsecutore", "dataRilascioDocumentoEsecutore", "dataScadenzaDocumentoEsecutore"];
}

function onEsecutoreMavRavClick(event) {
  var subDialog = $(event.target).parents(".formProdotto").find(".panel-esecutore-mav-rav");
  var requiredFields = getEsecutoreMavRavRequiredFields();
  onEsecutoreClick(event, subDialog, requiredFields);
}

function getEsecutoreMavRavRequiredFields() {
  return ["nomeEsecutore", "cognomeEsecutore", "codFiscaleEsecutore", "indirizzoEsecutore", "capEsecutore", "cittaEsecutore", "provinciaEsecutore", "nazioneEsecutore", "telefonoEsecutore", "emailEsecutore", "tipoDocumentoEsecutore", "numeroDocumentoEsecutore", "enteDocumentoEsecutore", "dataRilascioDocumentoEsecutore", "dataScadenzaDocumentoEsecutore"];
}

function onEsecutorePagoPaClick(event) {
  var subDialog = $(event.target).parents(".formProdotto").find(".panel-esecutore-pagopa");
  var requiredFields = getEsecutorePagoPaRequiredFields();
  onEsecutoreClick(event, subDialog, requiredFields);
}

function getEsecutorePagoPaRequiredFields() {
  return ["nomeEsecutore", "cognomeEsecutore", "codFiscaleEsecutore", "indirizzoEsecutore", "cittaEsecutore"];
}

function onEsecutoreBolloAutoClick(event) {
  var subDialog = $(event.target).parents(".formProdotto").find(".panel-esecutore-bollo-auto");
  var requiredFields = getEsecutoreBolloAutoRequiredFields();
  onEsecutoreClick(event, subDialog, requiredFields);
}

function getEsecutoreBolloAutoRequiredFields() {
  return ["nomeEsecutore", "cognomeEsecutore", "codFiscaleEsecutore", "indirizzoEsecutore", "cittaEsecutore"];
}

function onEsecutoreClick(event, subDialog, requiredFields) {
  if(event.target.checked == true){
    $(subDialog).removeClass("hidden");
    $(event.target).attr("required", true);
    requiredFields.forEach(function (item) {
      $(event.target).parents(".formProdotto").find("[name=" + item + "]").attr("required", true);
    });
  }else{
    $(subDialog).addClass("hidden");
    $(event.target).attr("required", false);
    clearForm(subDialog);
    requiredFields.forEach(function (item) {
      $(event.target).parents(".formProdotto").find("[name=" + item + "]").removeAttr("required");
    });
  }

  event.stopPropagation();
}


function getListaAziende(event){

  var str = $(event.target).val();
  if(str.length > 3 && event.which != 8){
    var reqUrl = "/src/web/action/vendita/getInfoCBILL.php?action=findAzienda&nome="+str;
    $("#cercaAziendeWait").css("display", "table-cell");
    customAjaxCall(reqUrl, ID_PROD_CBILL, autocompleteAziende, autocompleteAziendeError, null);
  }else{
    $("#cercaAziendeWait").css("display", "none");
    $("#listaAziende").remove();
    $("#codSia").val("");
    $("#prodotto_"+ID_PROD_CBILL).find(".codiceFiscale").removeClass("ricercaAbilitata");
  }
}
function autocompleteAziende(idProdotto, parsedData){
  $("#cercaAziendeWait").css("display", "none");
  var aziende = parsedData.dati;
  $("#listaAziende").remove();
  var list = "<ul id='listaAziende' class='list-group'>";
  for(var i=0;i<aziende.length;i++){
    var el = aziende[i];
    if(el != null){
      var flag = el.abilitazioneRicercaBollettini == 1 ? 'glyphicon glyphicon-list-alt' : '';
      list+= "<li class='list-group-item pointer' tabindex='0' data-codSia='"+el.codiceSia+"' onkeyup='checkIfEnterKey(event)' onclick='fillAzienda(this)'><span class='descAz'>"+el.nome+"</span><span class='flagBoll "+ flag+"'></span></li>";
    }
  }
  list+= "</ul>";
  if($(list).find("li").length > 0){
    $("#prodotto_23 #autocompleteAzienda").parent().append(list);
  }

  if($("#listaAziende").find("li").length == 1){
    fillAzienda($("#listaAziende").find("li"));
  }
}
function autocompleteAziendeError() {
  $("#cercaAziendeWait").css("display", "none");
  logOnConsole("[autocompleteAziendeError] Errore nel recupero delle aziende");
}
function fillAzienda(el){
  var prodottoEl = $(el).parents(".categoria-prodotto-item");
  $(prodottoEl).find("#autocompleteAzienda").val($(el).find(".descAz").text());
  $(prodottoEl).find("#autocompleteAzienda").attr("data-codSia", $(el).attr("data-codSia"));
  $(prodottoEl).find("#codSia").val($(el).attr("data-codSia"));
  $("#listaAziende").remove();
  if($(el).find(".flagBoll.glyphicon").length > 0){
    $(prodottoEl).find(".codiceFiscale").addClass("ricercaAbilitata");
    apriToolTipCodFiscale();
  }else{
    $(prodottoEl).find(".codiceFiscale").removeClass("ricercaAbilitata");
  }
}
function cercaBollettini(event){
  var parent = $(event.target).parents(".codiceFiscale");
  if($(parent).hasClass("ricercaAbilitata")){
    $("#cercaBollettiniBtn").addClass("inCorso");
    $("#cercaBollettiniBtn").attr("onclick", "");
    var nomeAz = $(parent).siblings(".nomeAzienda").find("#autocompleteAzienda").val();
    var codiceSia = $(parent).siblings(".nomeAzienda").find("#autocompleteAzienda").attr("data-codSia");
    var codFisc = $(parent).find("input").val();
    var reqUrl = "/src/web/action/vendita/getInfoCBILL.php?action=bollettini&codiceSia="+codiceSia+"&codFisc="+codFisc;
    customAjaxCall(reqUrl, ID_PROD_CBILL, fillBollettino, cercaBollettiniErroCallback, null);
  }
}
function cercaBollettiniErroCallback(){
  $("#cercaBollettiniBtn").removeClass("inCorso");
  $("#cercaBollettiniBtn").attr("onclick", "cercaBollettini(event)");
}
function fillBollettino(idProdotto, data){
  $("#bollettiniCont").remove();
  $("#cercaBollettiniBtn").removeClass("inCorso");
  $("#cercaBollettiniBtn").attr("onclick", "cercaBollettini(event)");
  var dati = data.dati;
  if(dati.resultCode == 0){
    var bollettini = dati.listaBollettini;
    var list = "<div id='bollettiniCont' class='col-md-12'>";
    list+="<div class='col-md-12'><span class='bold' style='color:#000'>E' possibile scegliere tra i seguenti bollettini</span></div>";
    list+="<ul id='listaBollettini' class='col-md-12 list-group'>";
    for(var i=0;i<bollettini.length;i++){
      var bol = bollettini[i];
      if(bol != null){
        list+= "<li class='list-group-item pointer' tabindex='"+i+"' data-id='"+bol.idBollettino+"' data-importo='"+bol.importo+"' onclick='fillImportoBollettino(this)'><span class='descrizione '>ID : "+bol.idBollettino+"</span> - <span class='impBoll'>&euro; : "+ (bol.importo/100).toFixed(2)+"</span></li>";
      }
    }
    list+= "</ul>";
    list+= "<div class='col-md-12'><span id='chiudiBollettini' class='btn pull-right' onclick='chiudiListaBollettini()'>No</span></div>";
    list+= "</div>";
    if($(list).find("li").length > 0){
      $("#prodotto_23 .formProdotto .codiceFiscale").after(list);
    }
  }else{
    var dialogParams = {
      idContent : "erroreVendita",
      title : "Operazione non riuscita",
      msg : "resultCode != 0 - Dati non disponibili",
      type : "error"			}
    openGenericDialog(dialogParams);
  }

}
function fillImportoBollettino(el){
  $("#listaBollettini li").removeClass("active");
  $(el).addClass("active");
  var prodottoEl = $(el).parents(".categoria-prodotto-item");
  $(prodottoEl).find("#idBollettino").val($(el).attr("data-id"));
  var importo = importo = parseInt($(el).attr("data-importo")/100).toFixed(2).split(".");
  $(prodottoEl).find("#importoVersamento .importo").val(importo[0]);
  $(prodottoEl).find("#importoVersamento .decimali").val(importo[1]);
}
function chiudiListaBollettini(){
  $("#bollettiniCont").remove();
}


function cercaVoucher(event) {
  var conto = $("#svconto").val();
  var voucher = $("#svvoucher").val();
  if(conto && voucher) {
    $("#cercaVoucherBtn").addClass("inCorso");
    $("#cercaVoucherBtn").attr("onclick", "");
    var reqUrl = "/src/web/action/vendita/getInfoVoucher.php?codiceConto=" + conto + "&codiceVoucher=" + voucher;
    customAjaxCall(reqUrl, ID_PROD_SNAI_VOUCHER, cercaVoucherSuccess, cercaVoucherError, null);
  }
}

function cercaVoucherSuccess(idProdotto, data) {
  $("#cercaVoucherBtn").removeClass("inCorso");
  $("#cercaVoucherBtn").attr("onclick", "cercaVoucher(event)");
  if(data && data.dati) {
    importo = data.dati;
    event.target.checked = false; //resetto il check
    $("input[name='accettazioneConsenso']").val(0);
    var msg = MSG_ADEGUATA_VERIFICA_POSTEPAY;
    var dialogParams = {
      idContent : "Voucher",
      title :  "Ricerca voucher",
      msg : "L'importo legato al voucher &egrave; di " + importo + " euro.",
      type : "info",
    }
    openGenericDialog(dialogParams);
  }
}

function cercaVoucherError() {
  $("#cercaVoucherBtn").removeClass("inCorso");
  $("#cercaVoucherBtn").attr("onclick", "cercaVoucher(event)");
  logOnConsole("Errore nel recupero dell'importo voucher");
}

function annullaVendita(event){
  fireSearchFromVisuraOrCompanyCardCamerali(false);
  event.stopPropagation();
  var prodotto = $(event.target).parents(".categoria-prodotto-item");
  $(prodotto).removeClass("active");
  var idProdotto = prodotto.attr("id");
  if(annullaVenditaCustomActions.hasOwnProperty(idProdotto)) {
    annullaVenditaCustomActions[idProdotto](prodotto);
  }

  $(event.target).remove();
  clearForm(prodotto);
  sortProdotti();
  svuotaFrontMonitor();
}

function annullaVenditaBollettini(prodotto) {
  //nasconde il nome del biller e i campi aggiuntivi
  cleanBillerBollettini();

  //nasconde il pannello per l'esecutore, rimuovendo l'obbligatorieta' ai campi
  var subDialog = prodotto.find(".panel-esecutore-bollettino");
  var requiredFields = getEsecutoreBollettinoRequiredFields();
  $(subDialog).addClass("hidden");
  prodotto.find("#pagatoreBollettinoFlag").attr("required", false);
  clearForm(subDialog);
  requiredFields.forEach(function (item) {
    prodotto.find(".formProdotto").find("[name=" + item + "]").removeAttr("required");
  });

  //resetta il tipo pagatore
  $("input[name='tipoPagatore']").removeProp('checked');
  $("input[name='tipoPagatore'][value='pf']").prop('checked', true);
  switchTipoPagatoreBollettino();

  //resetta i previousValue
  $("input[name='ccp']").data('previousValue', "");
  $("input.barcodeBollettino").data('previousValue', "");
}

function annullaVenditaBollettinoFreccia(prodotto) {
  //nasconde il nome del biller e i campi aggiuntivi
  cleanBillerBollettini();

  //nasconde il pannello per l'esecutore, rimuovendo l'obbligatorieta' ai campi
  var subDialog = prodotto.find(".panel-esecutore-bollettino");
  var requiredFields = getEsecutoreBollettinoRequiredFields();
  $(subDialog).addClass("hidden");
  prodotto.find("#pagatoreBollettinoFlag").attr("required", false);
  clearForm(subDialog);
  requiredFields.forEach(function (item) {
    prodotto.find(".formProdotto").find("[name=" + item + "]").removeAttr("required");
  });

  //resetta il tipo pagatore
  $("input[name='tipoPagatore']").removeProp('checked');
  $("input[name='tipoPagatore'][value='pf']").prop('checked', true);
  switchTipoPagatoreBollettinoFreccia();

  //resetta i previousValue
  $("input[name='ccp']").data('previousValue', "");
  $("input.barcodeBollettino").data('previousValue', "");
}

function annullaVenditaMavRav(prodotto) {
  //riporta la selezione di mav/rav su mav
  $("input[name='tipoProdotto']").removeProp('checked');
  $("input[name='tipoProdotto'][value='M']").prop('checked', true);

  //nasconde il pannello per l'esecutore, rimuovendo l'obbligatorieta' ai campi
  var subDialog = prodotto.find(".panel-esecutore-mav-rav");
  var requiredFields = getEsecutoreMavRavRequiredFields();
  $(subDialog).addClass("hidden");
  prodotto.find("#pagatoreMavRavFlag").attr("required", false);
  clearForm(subDialog);
  requiredFields.forEach(function (item) {
    prodotto.find(".formProdotto").find("[name=" + item + "]").removeAttr("required");
  });

  //resetta il tipo pagatore
  $("input[name='tipoPagatore']").removeProp('checked');
  $("input[name='tipoPagatore'][value='pf']").prop('checked', true);
  switchTipoPagatoreMavRav();

}

// chiamata da onVenditaCancelExecuted
function annullaVenditaPagoPa(prodotto) {
  //nasconde il pannello per l'esecutore, rimuovendo l'obbligatorieta' ai campi
  var subDialog = prodotto.find(".panel-esecutore-pagopa");
  var requiredFields = getEsecutorePagoPaRequiredFields();
  $(subDialog).addClass("hidden");
  prodotto.find("#pagatorePagoPaFlag").attr("required", false);
  clearForm(subDialog);
  requiredFields.forEach(function (item) {
    prodotto.find(".formProdotto").find("[name=" + item + "]").removeAttr("required");
  });

  //resetta il tipo pagatore
  prodotto.find("input[name='tipoPagatore']").removeProp('checked');
  prodotto.find("input[name='tipoPagatore'][value='pf']").prop('checked', true);
  switchTipoPagatorePagoPa();

}

//chiamata da onVenditaCancelExecuted
function annullaVenditaBolloAuto(prodotto) {

  //resetta e disabilita targa e telaio
  var targaInput = prodotto.find("input[name='targa']");
  targaInput.attr("disabled", "disabled");
  targaInput.removeAttr("required");
  targaInput.val("");
  var telaioInput = prodotto.find("input[name='telaio']");
  telaioInput.attr("disabled", "disabled");
  telaioInput.removeAttr("required");
  telaioInput.val("");

  //nasconde il pannello per l'esecutore, rimuovendo l'obbligatorieta' ai campi
  var subDialog = prodotto.find(".panel-esecutore-bollo-auto");
  var requiredFields = getEsecutoreBolloAutoRequiredFields();
  $(subDialog).addClass("hidden");
  prodotto.find("#pagatoreBolloAutoFlag").attr("required", false);
  clearForm(subDialog);
  requiredFields.forEach(function (item) {
    prodotto.find(".formProdotto").find("[name=" + item + "]").removeAttr("required");
  });

  //resetta il tipo pagatore
  prodotto.find("input[name='tipoPagatore']").removeProp('checked');
  prodotto.find("input[name='tipoPagatore'][value='pf']").prop('checked', true);
  switchTipoPagatoreBolloAuto();
}

//chiamata da onVenditaCancelExecuted
function annullaVenditaSimIliad(prodotto) {

//	prodotto.find("input[name='sesso']").prop('checked', false);
//	prodotto.find("input[name='sesso'][value='1']").prop('checked', true);

  // il panel per l'acquisizione dei file si resetta in automatico

  // il taglio si autoseleziona per tutti i prodotti
}

function annullaVenditaVisuraCamerali(prodotto) {
  fireSearchFromVisuraOrCompanyCardCamerali(false);
}

function closeAndClearAll(){
  $(".categoria-prodotto-item.active").each(function(){
    var prodotto = this;
    $(prodotto).removeClass("active");
    $(prodotto).find(".panel-heading .annullo").remove();
    clearForm(prodotto);
  });
  sortProdotti();
  svuotaFrontMonitor();
}
function clearForm(prodotto){
  $(prodotto).find("input").each(function(){
    if($(this).attr("type") == "radio"){
      //
    }else if($(this).attr("type") == "checkbox"){
      $(this).prop('checked', false);
    }else{
      if($(this).attr("name") != "idProdotto" &&
        !($(this).attr("name") == "tipoReport" && $(this).attr("type") == "hidden") &&
        !($(this).attr("name") == "tipoVisura" && $(this).attr("type") == "hidden")
      ){
        $(this).val("");
      }
      if($(this).hasClass("inputHelper")){
        $(this).val(0);
      }
    }
  });

  $(prodotto).find("select").each(function() {
    $(this).val("");
  });

  if($(prodotto).attr("id") == "prodotto_" + ID_PROD_TRANSFERTO) {
    $(prodotto).find(".dettaglio").empty(); //rimuove i tagli (che vanno riletti a ogni ricarica)
  }

  if($(prodotto).attr("id") == "prodotto_" + ID_PROD_BOLLETTINI) {
    annullaVenditaBollettini($("#venditaForm_" + ID_PROD_BOLLETTINI));
  }

  if($(prodotto).attr("id") == "prodotto_" + ID_PROD_MAV_RAV) {
    annullaVenditaMavRav($("#venditaForm_" + ID_PROD_MAV_RAV));
  }

  if($(prodotto).attr("id") == "prodotto_" + ID_PROD_BOLLOAUTO) {
    annullaVenditaBolloAuto($("#venditaForm_" + ID_PROD_BOLLOAUTO));
  }

  if($(prodotto).attr("id") == "prodotto_" + ID_PROD_ATTIVAZIONE_SIM_ILIAD) {
    annullaVenditaSimIliad($("#venditaForm_" + ID_PROD_ATTIVAZIONE_SIM_ILIAD));
  }

  $(prodotto).find(".dettaglio-item .btn").removeClass("active");
  $(prodotto).find(".has-error").removeClass("has-error");
  $(prodotto).find(".dettaglio").removeClass("error");
  $(prodotto).find("#listaAziende").remove();
  $(prodotto).find(".codiceFiscale").removeClass("ricercaAbilitata");
  $(prodotto).find("#autocompleteAzienda").attr("data-codsia","");
  $(prodotto).find("#filesUpload").find("#js-upload-files").val("");
  $(prodotto).find("#uploadedFiles").find(".list-group").empty();
  filesArray = [];
  chiudiTooltip();
  scriviFilesMancanti();
  chiudiWebSocket();
  stopWebcam();
  showIpagooAuth();

  // se il box e' ancora attivo e c'è un solo taglio presente, lo riseleziona
  if (($(prodotto).hasClass("venditaForm") && $(prodotto).parents(".categoria-prodotto-item").hasClass("active") || $(prodotto).hasClass("active")) && $(prodotto).find(".dettaglio-item .btn").length == 1) {
    onTaglioClick($(prodotto).find(".dettaglio-item .btn").eq(0));
  }
}
function isMultiple(check, against) {
  return check % against === 0;
}

function nextMultiple(x, multiplo){
  return Math.ceil(x/multiplo)*multiplo;
}

function validationObserver(el){
  var requiredFields = $(el).find(".formProdotto input[required]");
  logOnConsole(requiredFields);
}

function openDialogoPostepay(event){
  if(event.target.checked){
    event.target.checked = false; //resetto il check
    $("input[name='accettazioneConsenso']").val(0);
    var msg = MSG_ADEGUATA_VERIFICA_POSTEPAY;
    var dialogParams = {
      idContent : "postePayDialog",
      title : "Verifica",
      msg : msg,
      type : "Warning",
      action : "callBackPostepay",
      yesText : 'Ho eseguito',
      noText : 'Non ho eseguito'
    }
    openGenericDialog(dialogParams);
  }
}


function callBackPostepay(esito){
  if(esito == 0){
    $("#accettazioneConsenso")[0].checked = true;
    $("input[name='accettazioneConsenso']").val(1);
    chiudiTooltip();
  }else{
    $("#accettazioneConsenso")[0].checked =  false;
    $("input[name='accettazioneConsenso']").val(0);
  }
  removeCustomDialog();
}

function openDialogAdeguataVerificaAttivazioneSimIliad(event) {
  if(event.target.checked){
    event.target.checked = false; // resetto il check
    $("input[name='adeguataVerifica']").val(0);
    openDialogAdeguataVerificaAttivazioneSim();
  }
}

function openDialogAdeguataVerificaAttivazioneSim() {
  var dialogParams = {
    idContent : "adeguataVerificaDialog",
    title : "Verifica",
    msg : MSG_ADEGUATA_VERIFICA_ATTIVAZIONE_SIM,
    type : "wideModal",
    action : "callBackAdeguataVerifica",
    yesText : 'Ho eseguito',
    noText : 'Non ho eseguito'
  }
  openGenericDialog(dialogParams);
}

function callBackAdeguataVerifica(esito) {
  if(esito == 0){
    $("#adeguataVerifica")[0].checked = true;
    $("input[name='adeguataVerifica']").val(1);
    chiudiTooltip();
  }else{
    $("#adeguataVerifica")[0].checked =  false;
    $("input[name='adeguataVerifica']").val(0);
  }
  removeCustomDialog();
}

function apriTooltipCampiObbligatori(el){
  chiudiTooltip();
  var errorInput = $(el).find(".has-error").first();
  if($(errorInput).attr("id") == "drop-zone"){
    scriviFilesMancanti();
    $("#drop-zone").addClass("missing-files");
    $("#msgUploadPostepay").addClass("error");
  }else{
    var tooltip = "<div class='formTooltip'>";
    tooltip+="Compila i campi obbligatori";
    tooltip+="<div class='triangle-down'></div>";
    tooltip+="</div>";
    $(errorInput).prepend(tooltip);
  }
}

function apriErrorTooltip(form, messaggio) {
  chiudiTooltip();
  var errorInput = $(form).find(".has-error").first();
  var tooltip =  "<div class='formTooltip'>" + messaggio + "<div class='triangle-down'></div></div>";
  $(errorInput).prepend(tooltip);
}

function apriWarningTooltip(form, messaggio) {
  chiudiTooltip();
  var input = $(form).find(".has-warning").first();
  var tooltip =  "<div class='formWarningTooltip'>" + messaggio + "<div class='warning-triangle-down'></div></div>";
  $(input).prepend(tooltip);
}

function apriToolTipCodFiscale(){
  var tooltip = "<div class='formTooltip codFiscale'>";
  tooltip+="Effettua la ricerca di bollettini non pagati tramite codice fiscale";
  tooltip+="<div class='triangle-down'></div>";
  tooltip+="</div>";
  $("#prodotto_"+ID_PROD_CBILL).find(".codiceFiscale").prepend(tooltip);
  setTimeout (function(){
    chiudiTooltip();
  }, 3000)
}
function chiudiTooltip(){
  $(".formTooltip").remove();
  $(".formWarningTooltip").remove();
  $("#drop-zone").removeClass("missing-files");
  $("#msgUploadPostepay").removeClass("error");
}
function confermaVendita(idProdotto){
  var form = $("#venditaForm_"+idProdotto);
  $(form).find("input[name='requestId']").val(Date.now());
  var formData = $(form).formToArray();

  validateForm(idProdotto, formData);
}

function buildQueryStringFromFieldId(fields) {
  var params = "";
  for (var i = 0; i < fields.length; i++) {
    params += "&" + fields[i] + "=" + $("#" + fields[i]).val();
  }
  return params.replace("&", "?");
}

function buildQueryStringFromFieldIdEscaped(fields) {
  var params = "";
  var val = "";
  for (var i = 0; i < fields.length; i++) {
    val = $("#" + fields[i]).val() ? $("#" + fields[i]).val() : "";
    params += "&" + fields[i] + "=" + encodeURIComponent(val);
  }
  return params.replace("&", "?");
}

function validateFormProdottoCamerale(idProdotto, formData) {

  var esito  = true;

  var msgDialog = "";

  //gestisco la validazione per il prodotto
  if(idProdotto == ID_PROD_RICERCA_PERSONA) {
    var nome;
    var cognome;
    var cf;
    var dataDiNascita;

    //itero tutti i campi necessari per la form
    for(var i=0; i < formData.length; i++){
      var field = formData[i];

      if(field.name == "nome") {
        nome = field.value;
      } else if(field.name == "cognome") {
        cognome = field.value;
      } else if(field.name == "cf") {
        cf = field.value;
      } else if(field.name == "dataNascita") {
        dataDiNascita = field.value;
      }
    }

    if(nome.trim() == "" && cognome.trim() == "" && cf.trim() == "") {
      msgDialog += "Campi obbligatori mancanti, occorre inserire nome e cognome oppure il codice fiscale della persona fisica";
      esito = false;
    } else if(nome.trim() != "" && cognome.trim() ==  "") {
      msgDialog += "Se uno tra nome e cognome e' valorizzato, occorre inseririrli entrambi";
      esito = false;
    } else if(cognome.trim() != "" && nome.trim() ==  "") {
      msgDialog += "Se uno tra nome e cognome e' valorizzato, occorre inseririli entrambi";
      esito = false;
    }

  } else if(idProdotto == ID_PROD_RICERCA_AZIENDA) {

    var denominazione;
    var codiceFiscale;
    var partitaIva;
    var codRea;
    var provCciaa;

    //itero tutti i campi necessari per la form
    for(var i=0; i < formData.length; i++){
      var field = formData[i];

      if(field.name == "denominazione") {
        denominazione = field.value;
      } else if(field.name == "cf") {
        codiceFiscale = field.value;
      } else if(field.name == "partitaIva") {
        partitaIva = field.value;
      } else if(field.name == "codiceRea") {
        codRea = field.value;
      } else if(field.name == "provinciaCciaa") {
        provCciaa = field.value;
      }
    }

    if(denominazione.trim() == "" && partitaIva.trim() == "" && codiceFiscale.trim() == "" && codRea.trim() == "" && provCciaa.trim() == "") {
      msgDialog += "Campi obbligatori mancanti, occorre inserirli seguendo almeno una delle combinazioni elencate a pie' di pagina";
      esito = false;
    } else if(codRea.trim() != "" && provCciaa.trim() == "") {
      msgDialog += "Necessario specificare la provincia di riferimento";
      esito = false;
    } else if(codRea.trim() == "" && provCciaa.trim() != "") {
      msgDialog += "Numero Rea non specificato";
      esito = false;
    }

  } else if(idProdotto == ID_PROD_VISURA_ORDINARIA || idProdotto == ID_PROD_VISURA_STORICA) {
    var codiceFiscale;
    var codRea;
    var provCciaa;

    //itero tutti i campi necessari per la form
    for(var i=0; i < formData.length; i++){
      var field = formData[i];

      if(field.name == "codiceFiscale") {
        codiceFiscale = field.value;
      } else if(field.name == "nrea") {
        codRea = field.value;
      } else if(field.name == "cciaa") {
        provCciaa = field.value;
      }
    }

    if((codiceFiscale.trim() == "" && codRea.trim() == "" && provCciaa.trim() == "") ||
      (codRea.trim() != "" && provCciaa.trim() == "") ||
      (codRea.trim() == "" && provCciaa.trim() != "")
    ) {
      msgDialog += "Campi obbligatori mancanti, cliccare su link Ricerca Impresa o su uno dei campi Provincia Cciaa, Numero Rea, Codice fiscale";
      esito = false;
    }

  } else if(idProdotto == ID_PROD_REPORT_AZIENDA || idProdotto == ID_PROD_COMPANY_CARD_ORDINARIA || idProdotto == ID_PROD_COMPANY_CARD_STORICA || idProdotto == ID_PROD_REPORT_AZIENDA_CATASTO) {
    var codiceFiscale;
    var codRea;
    var provCciaa;

    //itero tutti i campi necessari per la form
    for(var i=0; i < formData.length; i++){
      var field = formData[i];

      if(field.name == "cf") {
        codiceFiscale = field.value;
      } else if(field.name == "nrea") {
        codRea = field.value;
      } else if(field.name == "cciaa") {
        provCciaa = field.value;
      }
    }

    if(idProdotto == ID_PROD_COMPANY_CARD_ORDINARIA || idProdotto == ID_PROD_COMPANY_CARD_STORICA) {
      if((codiceFiscale.trim() == "" && codRea.trim() == "" && provCciaa.trim() == "") ||
        (codRea.trim() != "" && provCciaa.trim() == "") ||
        (codRea.trim() == "" && provCciaa.trim() != "")
      ) {
        msgDialog += "Campi obbligatori mancanti, cliccare su link Ricerca Impresa o su uno dei campi Provincia Cciaa, Numero Rea, Codice fiscale";
        esito = false;
      }
    } else {
      if(codiceFiscale.trim() == "" && codRea.trim() == "" && provCciaa.trim() == "") {
        msgDialog += "Campi obbligatori mancanti, occorre inserirli seguendo almeno una delle combinazioni elencate a pie' di pagina";
        esito = false;
      } else if(codRea.trim() != "" && provCciaa.trim() == "") {
        msgDialog += "Necessario specificare la sigla provincia della camera di commercio di riferimento (Cciaa)";
        esito = false;
      } else if(codRea.trim() == "" && provCciaa.trim() != "") {
        msgDialog += "Numero Rea non specificato";
        esito = false;
      }
    }
  } else if(idProdotto == ID_PROD_REPORT_PERSONA) {
    var codiceFiscale ;
    var nome;
    var cognome;
    var dataNascita;
    var luogoNascita;
    var provinciaNascita;
    var sesso;

    //itero tutti i campi necessari per la form
    for(var i=0; i < formData.length; i++){
      var field = formData[i];

      if(field.name == "cf") {
        codiceFiscale = field.value;
      } else if(field.name == "nome") {
        nome = field.value;
      } else if(field.name == "cognome") {
        cognome = field.value;
      } else if(field.name == "dataNascita") {
        dataNascita = field.value;
      } else if(field.name == "luogoNascita") {
        luogoNascita = field.value;
      } else if(field.name == "provinciaNascita") {
        provinciaNascita = field.value;
      } else if(field.name == "sesso") {
        sesso = field.value;
      }
    }

    if(codiceFiscale.trim() == "" && (nome.trim() == "" || cognome.trim() == "" || dataNascita.trim() == "" || luogoNascita.trim() == "" || provinciaNascita.trim() == "" || (sesso.trim() != "M" && sesso.trim() != "F"))) {
      msgDialog += "Valorizzare Codice Fiscale della persona fisica altrimenti Nome, Cognome, Data di Nascita, Luogo di nascita, Provincia di nascita e Sesso";
      esito = false;
    }
  }
  if(!esito) {
    var dialogParams = {
      idContent : "vendita-container",
      title : "Campi da valorizzare",
      msg : msgDialog,
      type : "error"
    };
    openGenericDialog(dialogParams);
  }

  return esito;
}


function validateForm(idProdotto, formData){

  var form = $("#venditaForm_"+idProdotto);
  var el = $(form).parents(".categoria-prodotto-item");
  $(el).find(".form-group").removeClass("has-error");
  var validated = true;

  for(var i=0; i < formData.length; i++){
    var field = formData[i];
    if(field.required){
      if(idProdotto == ID_PROD_POSTEPAY && field.name == "accettazioneConsenso" && field.value == "0"){
        $(el).find("input[name='"+ field.name +"']").parents(".form-group").addClass("has-error");
        var msg = MSG_ADEGUATA_VERIFICA_POSTEPAY;
        var dialogParams = {
          idContent : "postePayDialog",
          title : "Verifica",
          msg : msg,
          type : "Warning",
          action : "callBackPostepay",
          yesText : "Ho eseguito",
          noText : "Non ho eseguito"
        }
        openGenericDialog(dialogParams);
        validated = false;
        break;
      } else if (idProdotto == ID_PROD_ATTIVAZIONE_SIM_ILIAD && field.name == "adeguataVerifica" && field.value == "0") {
        $(el).find("input[name='"+ field.name +"']").parents(".form-group").addClass("has-error");
        openDialogAdeguataVerificaAttivazioneSim();
        validated = false;
        break;
      }else if(field.name == "taglio"){
        if($(form).find("input[name='taglio']").val() == "" || $(form).find("input[name='taglio']").val() == undefined){
          $(el).find(".dettaglio.row").addClass("error");
          validated = false;
          break;
        }
      } else if(field.type == "select-one" && !field.value) {
        $(el).find("select[name='"+ field.name +"']").parents(".form-group").addClass("has-error");
        validated = false;
        break;
      }else if(field.type != "radio" && field.type != "checkbox" && field.value == ""){
        $(el).find("input[name='"+ field.name +"']").parents(".form-group").addClass("has-error");
        validated = false;
        break;
      }else{
        //$(el).find("[name='"+ field.name +"']").parents(".form-group").removeClass("has-error");
      }
    }
  }


  //se è un prodotto che contiene tagli

//	if(validated == true && $(form).find("input[name='taglio']").length > 0){
//		if($(form).find("input[name='taglio']").val() == "" || $(form).find("input[name='taglio']").val() == undefined){
//			$(el).find(".dettaglio.row").addClass("error");
//			validated = false;
//		}
//	}

  //aggiungo i decimali se necessario
  if(validated == true &&
    ($(form).parents(".categoria-prodotto-item").attr("data-codicefamiglia") == COD_FAM_PRODOTTI_FINANZIARI ||
      $(form).parents(".categoria-prodotto-item").attr("data-codicefamiglia") == COD_FAM_RICARICA_WALLET)){
    var decimali = $(form).find("input.decimali").val();
    if(decimali == ""){
      decimali = "00";
    }
    for(var i=0; i < formData.length; i++){
      var obj = formData[i];
      if(obj.name == "importo"){
        obj.value = obj.value + "," + decimali;
      }
    }
  }


  //validazione upload immagini
  if(validated == true && (idProdotto == ID_PROD_POSTEPAY || idProdotto == ID_PROD_ATTIVAZIONE_SIM_ILIAD)){
    if(filesArray.length == NUM_FILES){
      $("#drop-zone").removeClass("has-error");

      if(browserAgent == "isIE"){
        startUpload_IE();
      }else{
        startUpload();
      }
    }else{
      $("#drop-zone").addClass("has-error");
      validated = false;
    }
  }

  // bollettini: Validazione della ricerca biller sulla base del cc e delle date sui custom tags
  if(validated && idProdotto == ID_PROD_BOLLETTINI) {
    if($("#bollettini_biller_container").hasClass("hidden")) {
      $("input[name='ccp']").parents(".form-group").addClass("has-error");
      apriErrorTooltip(el, "Avvia la ricerca con il tasto lente");
      return false;
    }

    var dateValid = true;
    $("#bollettini_biller_additional_fields input[data-type='2']").each(function() {
      if($(this).val() && !regexDDMMAA.test($(this).val()) && !regexDDMMAAAA.test($(this).val())) {
        $(this).parents(".form-group").addClass("has-error");
        apriErrorTooltip(el, "Formato data non valido");
        dateValid = false;
      }
    });
    if(!dateValid) return false;

    var causaleValid = true;
    var causale = $("#venditaForm_" + ID_PROD_BOLLETTINI + " input[name='causale']").val();
    if(causale && !regexCausaleBollettino.test(causale)) {
      $("#venditaForm_" + ID_PROD_BOLLETTINI + " input[name='causale']").parents(".form-group").addClass("has-error");
      apriErrorTooltip(el, "Sono stati inseriti caratteri non ammessi");
      causaleValid = false;
    }
    if(!causaleValid) return false;

  }

  // Amazon wallet, verifica del range di importi
  if(validated && idProdotto == ID_PROD_AMAZON_WALLET) {
    var importo = $("#venditaForm_" + ID_PROD_AMAZON_WALLET + " input[name='importo']").val();
    var decimali = $("#venditaForm_" + ID_PROD_AMAZON_WALLET + " input.decimali").val();
    var importodec = parseInt((importo ? String(importo) : "0").concat(decimali ? String(decimali) : "00"));
    if(!importodec) importodec = 0;
    var rangeMin = parseInt(MSG_EPIPOLI_WALLET_OUT_OF_RANGE_MIN) * 100;
    var rangeMax = parseInt(MSG_EPIPOLI_WALLET_OUT_OF_RANGE_MAX) * 100;

    if(importodec < rangeMin || importodec > rangeMax) {
      $(el).find("input[name='importo']").parents(".form-group").addClass("has-error");
      apriErrorTooltip(el, "Inserire un importo tra " + MSG_EPIPOLI_WALLET_OUT_OF_RANGE_MIN + "&euro; e " + MSG_EPIPOLI_WALLET_OUT_OF_RANGE_MAX + "&euro;");
      return false;
    }
  }

  //if(validated) {
  //	validated = validateMandatoryGroupFields(idProdotto);
  //}

  //se il prodotto è uno di quelli cribis vai nella apposita funzione di validazione form
  if(idProdotto == ID_PROD_RICERCA_PERSONA ||
    idProdotto == ID_PROD_RICERCA_AZIENDA ||
    idProdotto == ID_PROD_REPORT_PERSONA ||
    idProdotto == ID_PROD_REPORT_AZIENDA ||
    idProdotto == ID_PROD_VISURA_ORDINARIA ||
    idProdotto == ID_PROD_VISURA_STORICA ||
    idProdotto == ID_PROD_COMPANY_CARD_ORDINARIA ||
    idProdotto == ID_PROD_COMPANY_CARD_STORICA ||
    idProdotto == ID_PROD_REPORT_AZIENDA_CATASTO) {
    if(!validateFormProdottoCamerale(idProdotto, formData)) {
      validated = false;
    }
  }

  var checkData = false;
  if(validated) {
    validated = validateDate(idProdotto);
    if(!validated) checkData = true;
  }

  if(validated){
    chiudiTooltip();
    openVenditaDialog(idProdotto, formData);
    return true;
  }else{
    if(!checkData) apriTooltipCampiObbligatori(el);
    return false;
  }

}


function richiediAutorizzazioneIpagoo() {
  var form = $("#venditaForm_" + ID_PROD_IPAGOO);
  $(form).find("input[name='requestId']").val(Date.now());
  var formData = $(form).formToArray();

  var el = $(form).parents(".categoria-prodotto-item");

  var validated = true;

  for(var i=0; i < formData.length; i++){
    var field = formData[i];
    if(field.required){
      if(field.type != "radio" && field.type != "checkbox" && field.value == ""){
        $(el).find("input[name='"+ field.name +"']").parents(".form-group").addClass("has-error");
        validated = false;
        break;
      }else{
        $(el).find("input[name='"+ field.name +"']").parents(".form-group").removeClass("has-error");
      }
    }
  }
  //aggiungo i decimali se necessario
  if(validated == true){
    var decimali = $(form).find("input.decimali").val();
    if(decimali == ""){
      decimali = "00";
    }
    for(var i=0; i < formData.length; i++){
      var obj = formData[i];
      if(obj.name == "importo"){
        obj.value = obj.value + "," + decimali;
      }
    }
  }

  if(validated){
    chiudiTooltip();
    openRichiestaAutorizzazioneIpagooDialog(ID_PROD_IPAGOO, formData);
    return true;
  }else{
    apriTooltipCampiObbligatori(el);
    return false;
  }
}

function openRichiestaAutorizzazioneIpagooDialog(idProdotto, formData) {
//	if ($(".venditaDialog:visible").length > 0){
//		return false;
//	}
//
//	$(".venditaDialog").remove();
//	$(".custom-modal").remove();
//	$(".modal-backdrop").remove();
//
//
//	var el = $(".categoria-prodotto-item[id='prodotto_"+idProdotto+"']");
//	var codFamiglia = $(el).attr("data-codicefamiglia");
//
//	var msgObj = {};
//	for(var i=0; i < formData.length; i++){
//		var p = formData[i];
//		msgObj[p.name] = p.value;
//	}
//
//	var msgConferma = "";
//	msgConferma+="<div><span>Brand : </span> <span><strong>"+$(el).find(".searchTarget").text()+"</strong></span></div>";
//	msgConferma+=getRowConferma("Email", msgObj.email);
//	msgConferma+=getRowConferma("Importo", " &euro; " +msgObj.importo);
//
//	var modal = "";
//	// aptri modal
//	modal += '<div class="modal fade venditaDialog" id="simple-modal" tabindex="-1" role="dialog" aria-labelledby="simple-modal" aria-hidden="true">';
//	modal += '<div class="modal-dialog" role="document">';
//	modal += '<div class="modal-content" id="controllaDati">';
//	modal += '<div class="modal-header">';
//	modal += '<h5 class="modal-title" id="simple-modal-label">Controlla i dati e conferma</h5>';
//	modal += '<button type="button" class="close chiudi close-modal" aria-label="Close">';
//	modal += '<span aria-hidden="true">&times;</span>';
//	modal += '</button>';
//	modal += '</div>';
//	modal += '<div class="modal-body">';
//	modal += msgConferma;
//	modal += '</div>';
//	modal += '<div class="modal-footer">';
//
//	modal += '<button type="button" id="annullaAzione" class="btn btn-secondary pointer chiudi">Chiudi</button>';
//
//	modal += '<button type="button" id="conferma" class="btn btn-primary sovrascriviTema pointer" onclick="inviaRequestIpagoo(\'A\')">Ok</button>';
//
//	modal += '</div>';
//	// chiudi Modal
//	modal += '</div></div></div>';
//
//	$("body").append(modal);
//
//	$(".venditaDialog").modal("show");
//
//	$(".venditaDialog").find(".chiudi").click(function() {
//		removeCustomDialog();
//	});
}

function inviaRequestIpagoo(operazione) {
//	$("#venditaPaymatInCorso").val(0);
//	$("#venditaForm_"+ID_PROD_IPAGOO).ajaxSubmit(
//			{
//				type : "post",
//				beforeSubmit:  beforefSub,
//				success : function(data){
//					if(IsJsonString(data)) {
//						var parsedData = JSON.parse(data);
//						if (parsedData.codice == 0 || parsedData.esito == 0 || parsedData.resultCode == 0){
//							var params = {};
//							params.codFamiglia = COD_FAM_PRODOTTI_FINANZIARI;
//							params.mediaCaller = 0;
//
//							$("#bgFrame").removeClass("fadeIn");
//							$("#bgFrame").addClass("fadeOut");
//							$("#bgFrame").one('animationend',function(event) {
//								$(this).remove();
//							})
//
//							logOnConsole(parsedData);
//
//							if(operazione == 'A') {
//								var dialogParams = {
//										idContent : "richiestaAutorizzazioneInviata",
//										title : "Richiesta autorizzazione",
//										msg : "La richiesta &egrave; stata inviata",
//										type : "info"
//									}
//								openGenericDialog(dialogParams);
//								if(parsedData.dati && parsedData.dati.NotificationId)
//									showIpagooPending(parsedData.dati.NotificationId);
//								else {
//									showIpagooError();
//								}
//							}
//
//							if(operazione == 'S') {
//
//								if(parsedData.dati && parsedData.dati.Status && parsedData.dati.NotificationId) {
//									var status = parsedData.dati.Status.toLowerCase();
//									var reqid = parsedData.dati.NotificationId;
//									if(status == "pending")
//										showIpagooPending(reqid);
//									else if(status == "accepted")
//										showIpagooConfirmed(reqid);
//									else if(status == "rejected")
//										showIpagooDenied(reqid);
//								}
//
//							}
//
//							lastVenditaObj = null;
//						}else if(isErrorCode(parsedData.codice)){
//							if ($("#bgFrame").length > 0) {
//								$("#bgFrame").remove();
//							}
//							var dialogParams = {
//									idContent : "erroreVendita",
//									title : "Operazione non riuscita",
//									msg : parsedData.resultDesc || parsedData.descrizione,
//									type : "error"
//								}
//							openGenericDialog(dialogParams);
//						}else{
//
//							if(callbackError){
//								callbackError();
//							}else{
//								if ($("#bgFrame").length > 0) {
//									$("#bgFrame").remove();
//								}
//								var dialogParams = {
//										idContent : "erroreVendita",
//										title : "Operazione non riuscita",
//										msg : parsedData.resultDesc || parsedData.descrizione,
//										type : "error"
//									}
//								openGenericDialog(dialogParams);
//							}
//						}
//					}else {
//
//						if(callbackError){
//							callbackError();
//						}else{
//							var dialogParams = {
//									idContent : "erroreVendita",
//									title : "Operazione non riuscita",
//									msg : "Dati non disponibili",
//									type : "error"
//								}
//							openGenericDialog(dialogParams);
//						}
//					}
//
//					//resetto input vendita
//					$("#venditaPaymatInCorso").val(0);
//
//				},
//				error : function(data, errorMsg, errorDesc) {
//
//					if(callbackError){
//						callbackError();
//					}else{
//						if ($("#bgFrame").length > 0) {
//							$("#bgFrame").remove();
//						}
//						logOnConsole("ERRORE VENDITA : " + errorDesc);
//						var dialogParams = {
//								idContent : "erroreVendita",
//								title : errorMsg,
//								msg : errorDesc,
//								type : "error"
//							}
//						openGenericDialog(dialogParams);
//					}
//
//					//resetto input vendita
//					$("#venditaPaymatInCorso").val(0);
//
//				}
//			});
}





function getRowConferma(key, value) {
  return "<div><span>" + key + " : </span><span><strong>" + (value !== null && value !== undefined ? value : "") + "</strong></span></div>";
}

function formatCentesimiInEuro(value) {
  var euro = value / 100;
  euro = euro.toLocaleString("it-IT", {style:"currency", currency:"EUR"});
  return euro;
}


function isErrorCode(code){
  var arrayCodiciErrore = [666666, 101101, 701];
  var is = false;
  for(var i = 0; i < arrayCodiciErrore.length; i++){
    if(arrayCodiciErrore[i] == code){
      is = true;
    }
  }
  return is;
}

// Inizio Gestione Retry
var isRetryShop = false;
var retryShopCount = 0;
var titleFinalRetry = "";
var msgFinalRetry = "";
var defauIfShowErrorTicketPreview = true;

function isDefaultRetryShop() {
  return window.DEFAULT_FLAG_RETRY_SHOP ? window.DEFAULT_FLAG_RETRY_SHOP : false;
}

function maxRetryShop() {
  return window.MAX_RETRY_SHOP ? window.MAX_RETRY_SHOP : 5;
}

function ifRetry(callbackError) {
  if(isDefaultRetryShop() && isRetryShop && ++retryShopCount <= maxRetryShop()) {
    removeCustomDialog();
    if(retryShopCount == maxRetryShop()) {
      addBgFrame();
      if(callbackError){
        callbackError();
      }
      else {
        var dialogParams = {
          idContent : "erroreVendita",
          title : titleFinalRetry,
          msg : msgFinalRetry,
          type : "error"
        }
        openGenericDialog(dialogParams);
      }
      removeBgFrame();
      isRetryShop = false;
      retryShopCount = 0;
      titleFinalRetry = "";
      msgFinalRetry = "";
      return false;
    } else {
      addBgFrame();
      return true;
    }
  } else {
    removeBgFrame();
    isRetryShop = false;
    retryShopCount = 0;
    titleFinalRetry = "";
    msgFinalRetry = "";
    return false;
  }
}
// Fine Gestione Retry
function eseguiVendita(idProdotto, codFamiglia){
  var callbackError = null;
  if(isDefaultRetryShop()) defauIfShowErrorTicketPreview = false;
  if(idProdotto == ID_PROD_CBILL) {
    callbackError = function (idProdotto, parsedData, params) {
      if(defauIfShowErrorTicketPreview) errorTicketPreview(ID_PROD_CBILL, getCbillErrorParams());
      lastVenditaObj = null;
    }
  }
  defauIfShowErrorTicketPreview = true;

  $("#venditaForm_"+idProdotto).ajaxSubmit(
    {
      type : "post",
      beforeSubmit:  beforefSub,
      success : function(data){
        //alert("data:" + data);
        if(IsJsonString(data)) {
          var parsedData = JSON.parse(data);
          if (parsedData.codice == 0 || parsedData.esito == 0 || parsedData.resultCode == 0){
            isRetryShop = false;
            var params = {};
            params.codFamiglia = codFamiglia;
            params.mediaCaller = 0;

            if(idProdotto == ID_PROD_TRANSFERTO){
              var el = $(".categoria-prodotto-item[id='prodotto_"+idProdotto+"']");
              $(el).find(".dettaglio").empty();
            }

            if(idProdotto == ID_PROD_BOLLETTINI && parsedData.dati && parsedData.dati.resultDetail && parsedData.dati.resultDetail == "111") {

              // Richiesta di conferma per eseguire comunque il pagamento anche se il bollettino e' gia' pagato
              var dialogParams = {
                idContent : "confermaBollettinoGiaPagatoDialog",
                title : "Conferma pagamento bollettino",
                msg : "Il bollettino inserito risulta essere gi&agrave; pagato. Si vuole eseguire il pagamento?<br />Premere 'Conferma' per eseguire il pagamento, 'Annulla' per annullare l'operazione",
                confirmText : "Conferma",
                annullaText : "Annulla",
                action : "confirmVenditaBollettinoGiaPagato()",
                actionAnnulla: "cancelVenditaBollettinoGiaPagato"
              }
              $(".venditaDialog").remove();
              $(".custom-modal").remove();
              removeBgFrame();
              openGenericDialog(dialogParams);


              /*} else if(idProdotto == ID_PROD_BOLLETTINO_FRECCIA && parsedData.dati) { //&& parsedData.dati.resultDetail && parsedData.dati.resultDetail == "111"

								// Richiesta di conferma per eseguire comunque il pagamento Freccia anche se il bollettino Freccia e' gia' pagato
								var dialogParams = {
									idContent : "confermaBollettinoFrecciaGiaPagatoDialog",
									title : "Conferma pagamento bollettino Freccia",
									msg : "Il bollettino Freccia inserito risulta essere gi&agrave; pagato. Si vuole eseguire il pagamento?<br />Premere 'Conferma' per eseguire il pagamento, 'Annulla' per annullare l'operazione",
									confirmText : "Conferma",
									annullaText : "Annulla",
									action : "confirmVenditaBollettinoFrecciaGiaPagato()",
									actionAnnulla: "cancelVenditaBollettinoFrecciaGiaPagato"
								}
								$(".venditaDialog").remove();
								$(".custom-modal").remove();
								removeBgFrame();
								openGenericDialog(dialogParams);


							*/}  else if(idProdotto == ID_PROD_PAGOPA) { //siamo gia' nel caso di resultcode=0
              logOnConsole(parsedData);

              var msg = "<div id=\"controllaDati\"><strong>Riepilogo pagamento:</strong><br /><br />";
              msg += getRowConferma("Beneficiario", parsedData.dati["beneficiario"]);
              msg += getRowConferma("Importo", formatCentesimiInEuro(parsedData.dati["importo"]));
              msg += getRowConferma("Causale", parsedData.dati["causale"]);
              msg += "<br />Premere 'Conferma' per eseguire il pagamento, 'Annulla' per annullare l'operazione</div>";

              // Aggiorna campi sul front monitor
              if(isSmartSolution) {
                $("#prodotto_" + ID_PROD_PAGOPA).find("input[name=fmPagoPaBeneficiario]").val(parsedData.dati["beneficiario"]);
                $("#prodotto_" + ID_PROD_PAGOPA).find("input[name=fmPagoPaImporto]").val(formatCentesimiInEuro(parsedData.dati["importo"]));
                aggiornaFrontMonitor($("#prodotto_" + ID_PROD_PAGOPA));
              }

              // La vendita esegue solo la reserve che recupera dei dati aggiuntivi da confermare. Se la reserve e' ok viene mostrata una modale per effettuare la confirm o la cancel
              var dialogParams = {
                idContent : "confermaPagoPaDialog",
                title : "Conferma pagamento",
                msg : msg,
                confirmText : "Conferma",
                annullaText : "Annulla",
                action : "confirmVenditaPagoPa()",
                actionAnnulla: "cancelVenditaPagoPa"
              }
              $(".venditaDialog").remove();
              $(".custom-modal").remove();
              removeBgFrame();
              openGenericDialog(dialogParams);

            } else if(idProdotto == ID_PROD_BOLLOAUTO) { //siamo gia' nel caso di resultcode=0
              logOnConsole(parsedData);

              var msg = "<div id=\"controllaDati\"><strong>Riepilogo pagamento:</strong><br /><br />";
              msg += getRowConferma("Importo", formatCentesimiInEuro(parsedData.dati["importo"]));
              msg += getRowConferma("Causale", parsedData.dati["causale"]);
              msg += "<br />Premere 'Conferma' per eseguire il pagamento, 'Annulla' per annullare l'operazione</div>";

              // Aggiorna campi sul front monitor
              if(isSmartSolution) {
                $("#prodotto_" + ID_PROD_BOLLOAUTO).find("input[name=fmBolloAutoImporto]").val(formatCentesimiInEuro(parsedData.dati["importo"]));
                aggiornaFrontMonitor($("#prodotto_" + ID_PROD_BOLLOAUTO));
              }

              // La vendita esegue solo la reserve che recupera dei dati aggiuntivi da confermare. Se la reserve e' ok viene mostrata una modale per effettuare la confirm o la cancel
              var dialogParams = {
                idContent : "confermaBolloAutoDialog",
                title : "Conferma pagamento",
                msg : msg,
                confirmText : "Conferma",
                annullaText : "Annulla",
                action : "confirmVenditaBolloAuto()",
                actionAnnulla: "cancelVenditaBolloAuto"
              }
              $(".venditaDialog").remove();
              $(".custom-modal").remove();
              removeBgFrame();
              openGenericDialog(dialogParams);

            } else if(idProdotto == ID_PROD_ATTIVAZIONE_SIM_ILIAD) { //siamo gia' nel caso di resultcode=0

              logOnConsole(parsedData);

              var telefono = $("#prodotto_" + ID_PROD_ATTIVAZIONE_SIM_ILIAD).find("input[name=telefono]").val();
              var importo = $("#prodotto_" + ID_PROD_ATTIVAZIONE_SIM_ILIAD).find("span.taglio.active").find("span.valoreTaglio").html();

              // Aggiorna campi sul front monitor
              if(isSmartSolution) {
                $("#prodotto_" + ID_PROD_ATTIVAZIONE_SIM_ILIAD).find("input[name=fmAttivazioneSimIliadImporto]").val(importo);
                aggiornaFrontMonitor($("#prodotto_" + ID_PROD_ATTIVAZIONE_SIM_ILIAD));
              }


              var msg = "<div class=\"ticket-container\">" +
                "<div id=\"ticketPreview\" style='position:relative; top:0px;'>" +
                "<div id=\"contentPreview\" moznomarginboxes mozdisallowselectionprint>" +
                "<div id=\"btnStampaTop\" class=\"col-md-12 buttonCont\"><div class=\"btn btn-primary no-print\" onclick=\"window.print()\">Stampa</div></div>" +
                "<div id=\"header\" class=\"col-md-12\"><div id=\"logoPaymat\" class=\"immagine\"><img src=\"/sites/all/themes/snaitech/loghi/paymat_snaitech_blu.png\"></div></div>" +
                //										"<div class=\"nomeEsercente bold\">ESERCENTE DANIELE 03</div>" +
                "<div class=\"default scontrino\">Pre-Scontrino</div>" +
                "<div class=\"default descrizioneProdotto\">Ricarica Attivazione Sim</div>" +
                "<div class=\"default dataTransazione\">" + getDataEOra() + "</div>" +
                "<div class=\"logoCompany\"><img src=\"/sites/default/files/adm_pr_images/iliad.png\"></div>" +
                "<div class=\"corpo\">" +
                "<div class=\"riga\"><span class=\"chiave\">Numero di contatto : </span><span class=\"valore bold\">" + telefono + "</span></div>" +
                "<div class=\"riga\"><span class=\"chiave\">Importo : </span><span class=\"valore bold\">" + importo + "</span></div>" +
                "</div>" +
                "<div class=\"noteTransazione\">Attenzione: l'operazione non si è ancora conclusa. Controlli attentamente i dati qui riportati prima di procedere e si ricordi di ritirare e conservare la ricevuta finale che costituisce la prova dell'avvenuto pagamento. Per l'attivazione, segua le istruzioni sull'Iliad SIM Pack.</div>" +
                "<div class=\"linkSito bold\">www.snaipay.it</div>" +
                "<div class=\"helpDesk bold\">HelpDesk 800.550.550 - Snaitech S.p.A</div>" +
                "</div></div></div>";


              // Prescontrino prima di effettuare la confirm
              var dialogParams = {
                idContent : "confermaAttivazioneSimIliadDialog",
                title : "Conferma attivazione",
                msg : msg,
                confirmText : "Conferma",
                annullaText : "Annulla",
                action : "confirmAcquistoSimIliad()",
                actionAnnulla: "cancelAcquistoSimIliad"
              }
              $(".venditaDialog").remove();
              $(".custom-modal").remove();
              removeBgFrame();
              openGenericDialog(dialogParams);

            } else {
              ticketPreview(idProdotto, parsedData, params);
              aggiornaSaldoAzienda();
              aggiornaDareAvereFrontMonitor(idProdotto, parsedData);
              lastVenditaObj = null;
            }

          }else if(isErrorCode(parsedData.codice)){
            isRetryShop = true;
            if ($("#bgFrame").length > 0) {
              $("#bgFrame").remove();
            }
            titleFinalRetry = "Operazione non riuscita";
            msgFinalRetry = parsedData.resultDesc || parsedData.descrizione;
            var dialogParams = {
              idContent : "erroreVendita",
              title : titleFinalRetry,
              msg : msgFinalRetry,
              type : "error"
            }
            if(!isDefaultRetryShop()) openGenericDialog(dialogParams);
          }else{
            isRetryShop = true;
            if(callbackError){
              if(!isDefaultRetryShop()) callbackError();
            }else{
              if ($("#bgFrame").length > 0) {
                $("#bgFrame").remove();
              }
              titleFinalRetry = "Operazione non riuscita";
              msgFinalRetry = parsedData.resultDesc || parsedData.descrizione;
              var dialogParams = {
                idContent : "erroreVendita",
                title : titleFinalRetry,
                msg : msgFinalRetry,
                type : "error"
              }
              if(!isDefaultRetryShop()) openGenericDialog(dialogParams);
            }
          }
        }else {
          isRetryShop = true;
          if(callbackError){
            if(!isDefaultRetryShop()) callbackError();
          }else{
            titleFinalRetry = "Operazione non riuscita";
            msgFinalRetry = "Dati non disponibili";
            var dialogParams = {
              idContent : "erroreVendita",
              title : titleFinalRetry,
              msg : msgFinalRetry,
              type : "error"
            }

            removeBgFrame();
            if(!isDefaultRetryShop()) openGenericDialog(dialogParams);
          }
        }

        //resetto input vendita
        $("#venditaPaymatInCorso").val(0);
        if(ifRetry(callbackError)) {
          $.ajax(this);
          return;
        }
      },
      error : function(data, errorMsg, errorDesc) {
        isRetryShop = true;
        if(callbackError){
          if(!isDefaultRetryShop()) callbackError();
        }else{
          if ($("#bgFrame").length > 0) {
            $("#bgFrame").remove();
          }
          logOnConsole("ERRORE VENDITA : " + errorDesc);
          titleFinalRetry = errorMsg;
          msgFinalRetry = errorDesc;
          var dialogParams = {
            idContent : "erroreVendita",
            title : titleFinalRetry,
            msg : msgFinalRetry,
            type : "error"
          }

          if(!isDefaultRetryShop()) openGenericDialog(dialogParams);
        }

        //resetto input vendita
        $("#venditaPaymatInCorso").val(0);
        if(ifRetry(callbackError)) {
          $.ajax(this);
          return;
        }
      }
    });

}

function beforefSub(formData, jqForm, options){
  var idProd = parseInt($(jqForm).attr("id").replace("venditaForm_", ""));

  if($("#venditaPaymatInCorso").val() == 1){
    var dialogParams = {
      idContent : "erroreVendita",
      title : "Operazione non riuscita",
      msg : "Impossibile effettuare l'operazione, è presente una vendita in corso",
      type : "error"
    }
    openGenericDialog(dialogParams);
    return false;
  }

  if($(jqForm).parents(".categoria-prodotto-item").attr("data-codicefamiglia") == COD_FAM_PRODOTTI_FINANZIARI ||
    $(jqForm).parents(".categoria-prodotto-item").attr("data-codicefamiglia") == COD_FAM_RICARICA_WALLET){
    var decimali = $(jqForm).find("input.decimali").val();
    if(decimali == ""){
      decimali = "00";
    }
    for(var i=0; i < formData.length; i++){
      var obj = formData[i];
      if(obj.name == "importo"){
        var amount = obj.value + "," + decimali;
        obj.value = fromEuroToCents(amount);
      }
    }
  }

  if(idProd == ID_PROD_POSTEPAY || idProd == ID_PROD_ATTIVAZIONE_SIM_ILIAD){
    uploadImmaginiPostePay(formData);
    preparePostePayData(formData);
  }

  aggiungitime(formData);
  aggiungiFlagSmartSolution(formData);
//	aggiungiIdDispositivo(formData);
  aggiungiSmint(formData);

  if(PRODOTTI_LAST_VENDITA_OBJ.indexOf(idProd) > -1){
    lastVenditaObj = {};
    for(var i=0; i < formData.length; i++){
      var p = formData[i];
      lastVenditaObj[p.name] = p.value;
    }
  }

  $("#venditaPaymatInCorso").val(1);

  if($("#bgFrame").length > 0){
    $("#bgFrame").remove();
  }
  $("body").append("<div id='bgFrame' class='animated fadeIn'><img src='/sites/all/themes/snaitech/images/load.gif'/></div>");
}


function preparePostePayData(formData){
  for(var i=0; i < formData.length; i++){
    var obj = formData[i];
    if(obj.name == "files[]"){
      formData.splice(i, 1);
    }
  }
}
function aggiungitime(formData){
  formData.push({name : "time", value : Date.now()});
}
function aggiungiFlagSmartSolution(formData) {
  formData.push({name : "isSmartDesk", value : (isSmartSolution ? "true" : "false")});
}
//function aggiungiIdDispositivo(formData) {
//	if(idDispositivo)
//		formData.push({name : "idDispositivo", value : idDispositivo });
//}
function aggiungiSmint(formData) {
  if(smint)
    formData.push({name : "smint", value : smint });
}

function aggiornaDareAvereFrontMonitor(idProdotto, parsedData) {
  if(isSmartSolution) {
    if (parsedData && parsedData.ticket) {

      var importo = parseFloat(parsedData.ticket.importoFacciale.replace(",", "."));
      var tasse = 0;
      if(parsedData.ticket.tasse && parsedData.ticket.tasse.importoTassa)
        tasse = parseFloat(parsedData.ticket.tasse.importoTassa.replace(",", "."));
      var totale = (importo + tasse).toFixed(2);

      var messaggio = {};
      messaggio.action = "movTot";
      messaggio.importo = totale;
      messaggio.prodotto = -10;
      messaggio.dareAvere = 1;  //0 - dare ; 1 - avere
      messaggio.idTran = parsedData.ticket.idTranPaymat;
      window.parent.postMessage(messaggio, "*");

    } else {
      logOnConsole("[aggiornaDareAvere] Scontrino non presente, aggiornamento non effettuato");
    }
  }
}

function aggiornaFrontMonitor(formProdotto) {

  // Style
  var style = "<style>";
  style += "#anteprimaCifre { display: none; }";
  style += "</style>";

  // header con logo e descrizione brand
  var header = "";
  header += "<div class='col-xs-12 text-center'>";
  header += "<div class='col-xs-12' style='height: 100px; display: flex; align-items: center; justify-content: center'>";
  header += "<img src='" + window.location.href.split('/').slice(0, 3).join('/') + formProdotto.find(".panel-heading").eq(0).find("img").eq(0).attr("src") + "' style='max-height: 100%; max-width: 100%'>";
  header += "</div>";
  header += "<div class='col-xs-12' style='margin: 10px 0; font-weight: bold; text-transform: uppercase;'>";
  header += formProdotto.find(".searchTarget").html();
  header += "</div>";
  header += "</div>";

  // body con tutti i campi variabili
  var body = "";
  body += "<div class='col-xs-12' style='font-size: 18px'>";

  // campi 'hidden', ossia che vengono visualizzati solo con una richiesta esplicita dell'operatore
  $(formProdotto).find(".fmHiddenField").each(function() {
    if($(this).val() || $(this).html()) {
      body += buildFmFieldRow($(this).attr("data-placeholder"), $(this).val() + $(this).html(), null, true);
    }
  });

  // Tagli (pin e ricariche)
  var taglio = formProdotto.find("span.taglio.active");
  if (taglio.length > 0) {
    body += buildFmFieldRow(taglio.find("span.descrizioneTaglio").html() + "*",  taglio.find("span.valoreTaglio").html() + " &euro;", "(*) Potrebbero essere inclusi dei costi di commissione");
  }

  // Campi generici, si usa il placeholder come etichetta e il value come valore
  $(formProdotto).find(".fmGenericField").each(function() {
    if($(this).val() || $(this).html()) {
      body += buildFmFieldRow($(this).attr("data-placeholder"), $(this).val() + $(this).html());
    }
  });

  // importo da testo, occorre aggiungere la nota
  var importoText = $(formProdotto).find(".fmField.importoText");
  if(importoText.length > 0 && importoText.val()) {
    body += buildFmFieldRow("Importo*", importoText.val(), "(*) Potrebbero essere inclusi dei costi di commissione");
  }

  // importo da form, occorre mettere insieme interi e decimali e aggiungere la nota
  var importoInt = $(formProdotto).find(".fmField.importo");
  var importoDec = $(formProdotto).find(".fmField.decimali");
  if(importoInt.length > 0 && importoDec.length > 0 && (importoInt.val() || importoDec.val())) {
    var importoIntVal = importoInt.val() ? "" + importoInt.val() : "0";
    var importoDecVal = importoDec.val() ? "" + importoDec.val() : "0";
    if(importoDecVal.length == 1) importoDecVal += "0";
    body += buildFmFieldRow("Importo*", importoIntVal + "," + importoDecVal + " &euro;", "(*) Potrebbero essere inclusi dei costi di commissione");
  }

  body += "</div>";

  // contenitore di header e body
  var html = "";
  html += style;
  html += "<div class='col-xs-12' style=\"margin-top: 20px; font-family: 'Lato, Helvetica Neue, Arial, sans-serif'; font-size: 20px;\">";
  html += header;
  html += body;
  html += "</div>";

  toFrontMonitor(html);
}

function getGenericFmFieldRow(formProdotto, field) {
  var html = "";
  if(field.length > 0 && field.val()) {
    html = buildFmFieldRow(label, field.val());
  }
  return html;
}

function buildFmFieldRow(label, value, note, isHidden) {
  var html = "";
  html += "<div class='col-xs-12' style='background-color: #DEDEDE; border: 1px solid " + (isHidden ? "#dd0000" : "#000") + "; word-wrap: break-word; margin: 5px 0; text-align: center;" + (isHidden ? " color: #dd0000;" : "") + "'>";
  if(label) html += "<span style='font-weight: normal; display: block;'>" + label + "</span>";
  html += "<span style='font-weight: bold; display: block'>" + value + "</span>";
  if(note) html += "<span style='font-size: 11px; display: block'>" + note + "</div>";
  html += "</div>";
  return html;
}

function buildShowFmHiddenFieldKeyEvents(fieldName, idProdotto) {
  return "onkeydown=\"keyShowFmHiddenField('" + fieldName + "', " + idProdotto + ", event)\" onkeyup=\"keyHideFmHiddenField('" + fieldName + "', " + idProdotto + ", event)\"";
}

function buildShowFmHiddenFieldButton(fieldName, idProdotto) {
  return "<span class='input-group-addon' onmousedown=\"showFmHiddenField('" + fieldName + "', " + idProdotto + ")\" onmouseup=\"hideFmHiddenField('" + fieldName + "', " + idProdotto + ")\" title='Tieni premuto per mostrare su front monitor'><span class='glyphicon glyphicon-share-alt'></span></span>"
}

function keyShowFmHiddenField(fieldName, idProdotto, event) {
  //35 = tasto fine, 36 = tasto home
  if(event.which + "" === FRONTMONITOR_SHAREKEY) {
    event.preventDefault();
    showFmHiddenField(fieldName, idProdotto);
  }
}

function showFmHiddenField(fieldName, idProdotto) {
  $("#prodotto_" + idProdotto).find("input[name=" + fieldName + "]").addClass("fmHiddenField");
  aggiornaFrontMonitor($("#prodotto_" + idProdotto));
}

function keyHideFmHiddenField(fieldName, idProdotto, event) {
  //35 = tasto fine, 36 = tasto home
  if(event.which + "" === FRONTMONITOR_SHAREKEY) {
    event.preventDefault();
    hideFmHiddenField(fieldName, idProdotto);
  }
  checkIfNumber(event);
}

function hideFmHiddenField(fieldName, idProdotto) {
  $("#prodotto_" + idProdotto).find("input[name=" + fieldName + "]").removeClass("fmHiddenField");
  aggiornaFrontMonitor($("#prodotto_" + idProdotto));
}

function svuotaFrontMonitor() {
  toFrontMonitor("<style>#anteprimaCifre { display: none; }</style>");
}

function confirmVenditaBollettinoGiaPagato() {
  $("#venditaForm_" + ID_PROD_BOLLETTINI).find("input[name='reserveRequestIdBollettinoPagato']").val(lastVenditaObj["requestId"]);
  $("#venditaForm_" + ID_PROD_BOLLETTINI).find("input[name='requestId']").val(Date.now());
  eseguiVendita(ID_PROD_BOLLETTINI, COD_FAM_PRODOTTI_FINANZIARI);
}

function confirmVenditaBollettinoFrecciaGiaPagato() {
  $("#venditaForm_" + ID_PROD_BOLLETTINO_FRECCIA).find("input[name='reserveRequestIdBollettinoPagato']").val(lastVenditaObj["requestId"]);
  $("#venditaForm_" + ID_PROD_BOLLETTINO_FRECCIA).find("input[name='requestId']").val(Date.now());
  eseguiVendita(ID_PROD_BOLLETTINO_FRECCIA, COD_FAM_PRODOTTI_FINANZIARI);
}

function cancelVenditaBollettinoGiaPagato() {
  var reserveRequestId = lastVenditaObj["requestId"];
  var requestId = Date.now();
  var reqUrl = "/src/web/action/vendita/execCancelBollettino.php?reserveRequestId=" + reserveRequestId + "&requestId=" + requestId;
  customAjaxCall(reqUrl, ID_PROD_BOLLETTINI, onVenditaBollettinoGiaPagatoCanceled, onVenditaBollettinoGiaPagatoCanceled, {"reserveRequestId": reserveRequestId, "requestId": requestId} );
}

function cancelVenditaBollettinoFrecciaGiaPagato() {
  var reserveRequestId = lastVenditaObj["requestId"];
  var requestId = Date.now();
  var reqUrl = "/src/web/action/vendita/execCancelBollettinoFreccia.php?reserveRequestId=" + reserveRequestId + "&requestId=" + requestId;
  customAjaxCall(reqUrl, ID_PROD_BOLLETTINO_FRECCIA, onVenditaBollettinoFrecciaGiaPagatoCanceled, onVenditaBollettinoFrecciaGiaPagatoCanceled, {"reserveRequestId": reserveRequestId, "requestId": requestId} );
}


function onVenditaBollettinoGiaPagatoCanceled() {
  lastVenditaObj = null;
  annullaVenditaBollettini($("#venditaForm_" + ID_PROD_BOLLETTINI));
  clearForm($("#venditaForm_" + ID_PROD_BOLLETTINI));
  removeBgFrame();
}

function onVenditaBollettinoFrecciaGiaPagatoCanceled() {
  lastVenditaObj = null;
  annullaVenditaBollettini($("#venditaForm_" + ID_PROD_BOLLETTINO_FRECCIA));
  clearForm($("#venditaForm_" + ID_PROD_BOLLETTINO_FRECCIA));
  removeBgFrame();
}


function confirmVenditaPagoPa() {
  var requestId = Date.now();
  var reqUrl = "/src/web/action/vendita/" + execProdottiFinanziari[PREFISSO_PRODOTTO + ID_PROD_PAGOPA] + ".php?operazione=confirm&reserveRequestId=" + lastVenditaObj["requestId"] + "&requestId=" + requestId;
  reqUrl += getSmartSolutionParametersAsString();
  addBgFrame();
  customAjaxCall(reqUrl, ID_PROD_PAGOPA, onVenditaConfirmExecuted, onVenditaConfirmError, {"reserveRequestId": lastVenditaObj["requestId"], "requestId": requestId} );
}

function cancelVenditaPagoPa() {
  var requestId = Date.now();
  var reqUrl = "/src/web/action/vendita/" + execProdottiFinanziari[PREFISSO_PRODOTTO + ID_PROD_PAGOPA] + ".php?operazione=cancel&reserveRequestId=" + lastVenditaObj["requestId"] + "&requestId=" + requestId;
  reqUrl += getSmartSolutionParametersAsString();
  customAjaxCall(reqUrl, ID_PROD_PAGOPA, onVenditaCancelExecuted, onVenditaCancelExecuted, {"reserveRequestId": lastVenditaObj["requestId"], "requestId": requestId} );
}

function confirmVenditaBolloAuto() {
  var requestId = Date.now();
  var reqUrl = "/src/web/action/vendita/" + execProdottiFinanziari[PREFISSO_PRODOTTO + ID_PROD_BOLLOAUTO] + ".php?operazione=confirm&reserveRequestId=" + lastVenditaObj["requestId"] + "&requestId=" + requestId;
  reqUrl += getSmartSolutionParametersAsString();
  addBgFrame();
  customAjaxCall(reqUrl, ID_PROD_BOLLOAUTO, onVenditaConfirmExecuted, onVenditaConfirmError, {"reserveRequestId": lastVenditaObj["requestId"], "requestId": requestId} );
}

function cancelVenditaBolloAuto() {
  var requestId = Date.now();
  var reqUrl = "/src/web/action/vendita/" + execProdottiFinanziari[PREFISSO_PRODOTTO + ID_PROD_BOLLOAUTO] + ".php?operazione=cancel&reserveRequestId=" + lastVenditaObj["requestId"] + "&requestId=" + requestId;
  reqUrl += getSmartSolutionParametersAsString();
  customAjaxCall(reqUrl, ID_PROD_BOLLOAUTO, onVenditaCancelExecuted, onVenditaCancelExecuted, {"reserveRequestId": lastVenditaObj["requestId"], "requestId": requestId} );
}

function confirmAcquistoSimIliad() {
  var requestId = Date.now();
  var reqUrl = "/src/web/action/vendita/" + execAttivazioniSim[PREFISSO_PRODOTTO + ID_PROD_ATTIVAZIONE_SIM_ILIAD] + ".php?operazione=confirm&reserveRequestId=" + lastVenditaObj["requestId"] + "&requestId=" + requestId;
  reqUrl += getSmartSolutionParametersAsString();
  addBgFrame();
  customAjaxCall(reqUrl, ID_PROD_ATTIVAZIONE_SIM_ILIAD, onVenditaConfirmExecuted, onVenditaConfirmError, {"reserveRequestId": lastVenditaObj["requestId"], "requestId": requestId} );
}

function cancelAcquistoSimIliad() {
  var requestId = Date.now();
  var reqUrl = "/src/web/action/vendita/" + execAttivazioniSim[PREFISSO_PRODOTTO + ID_PROD_ATTIVAZIONE_SIM_ILIAD] + ".php?operazione=cancel&reserveRequestId=" + lastVenditaObj["requestId"] + "&requestId=" + requestId;
  reqUrl += getSmartSolutionParametersAsString();
  customAjaxCall(reqUrl, ID_PROD_ATTIVAZIONE_SIM_ILIAD, onVenditaCancelExecuted, onVenditaCancelExecuted, {"reserveRequestId": lastVenditaObj["requestId"], "requestId": requestId} );
}

function getSmartSolutionParametersAsString() {
  var value = "";
  value += "&time=" + Date.now();
  value += "&isSmartDesk=" + (isSmartSolution ? "true" : "false");
  value += smint ? ("&smint=" + smint) : "";
  return value;
}

function onVenditaConfirmExecuted(idProdotto, parsedData, params) {
  logOnConsole(parsedData);
  params.mediaCaller = 0;
  ticketPreview(idProdotto, parsedData, params);
  aggiornaSaldoAzienda();
  aggiornaDareAvereFrontMonitor(idProdotto, parsedData);
  lastVenditaObj = null;
  $("#venditaPaymatInCorso").val(0);
}

function onVenditaConfirmError() {
  $(".custom-modal").find(".chiudi").off("click"); //rimuove la cancel agganciata al tasto
  $(".custom-modal").find(".chiudi").click(function() { //ripristina la chiusura standard
    removeCustomDialog();
  });
  genericErrorCallback();
}

function onVenditaCancelExecuted(idProdotto, parsedData, params) {
  logOnConsole(parsedData);
  lastVenditaObj = null;
  annullaVenditaCustomActions[PREFISSO_PRODOTTO + idProdotto]($("#venditaForm_" + idProdotto));
  clearForm($("#venditaForm_" + idProdotto));
  aggiornaFrontMonitor($("#prodotto_" + idProdotto));
  removeBgFrame();
  $("#venditaPaymatInCorso").val(0);
}

var MAX_VENDITA_RETRY = 4;
var nRetry = 0;
function uploadImmaginiPostePay(formData){

  if(READY_TO_UPLOAD){
    var params = (JSON.stringify(arrayToUpload));
    var metadata = (JSON.stringify(arrayMetadataToUpload));
    formData.push({name:"immagini", value:params, type: "text", required: true});
    formData.push({name:"metadata", value:metadata, type: "text", required: true});
  }else{
    if(nRetry < MAX_VENDITA_RETRY){
      setTimeout(function(){
        nRetry++;
        eseguiVendita(idProdotto, codFamiglia);
      }, 500);
      return false;
    }else{
      var dialogParams = {
        idContent : "erroreVendita",
        title : "Operazione non riuscita",
        msg : "Raggiunto limite massimo tentativi (immagini non caricate)",
        type : "error"
      }
      openGenericDialog(dialogParams);
      return false;
    }

  }

}

function getCbillErrorParams() {
  var params = {};
  params.descrizioneProdotto = "Pagamento Cbill";
  params.esitoTransazione = "Transazione non<br />andata a buon fine";
  params.footerScontrino = "SNAI S.P.A.";
  params.linkSitoWeb = "www.snaipay.it";
  params.logoAziendale = "paymat_snaitech_blu.png";
  params.logoBrand = "cbill.png";
  params.numeroAssistenza = "800.550.550";
  params.importo = lastVenditaObj ? lastVenditaObj.importo : "";
  params.numeroIdBollettino = lastVenditaObj ? lastVenditaObj.numeroIdBollettino : "";
  params.codFiscale = lastVenditaObj ? lastVenditaObj.codFiscale : "";
  params.variabile = '{"codiceSIAA":"' + (lastVenditaObj ? lastVenditaObj.codiceSia : "") + '","descEnte":"'+ $("#autocompleteAzienda").val() + '"}';
  return params;
}

function closeIFrame(){
  $("#printerNew").removeClass("slideInUp");
  $("#printerNew").addClass("slideOutDown");
  $("#bgFrame").removeClass("fadeIn");
  $("#bgFrame").addClass("fadeOut");
  $("#bgFrame").one('animationend',function(event) {
    $("#bgFrame").remove();
  })
}


function flashMessage(el, idContent, title, msg, classes){
  var top = $(el).offset().top + ($(el).outerHeight()/2);
  var left = window.innerWidth/2;

  var dialog = "";
  dialog+="<div class='flashMsg animated hidden "+classes+"' id='"+idContent+"' style='position:fixed;top:"+top+"px;left:"+left+"px;width:300px'>";
  dialog+="<div>"+msg+"</div>";
  dialog+="</div>";

  $("body").append(dialog);
  $(".flashMsg").addClass("slideInUp");
  $(".flashMsg").removeClass("hidden");
  setTimeout(function(){
    $(".flashMsg").removeClass("slideInUp");
    $(".flashMsg").addClass("slideOutUp");
    $(".flashMsg").one('animationend',function(event) {
      $(this).remove();
    })
  }, 1000);
}


function catchWindowPrint(){
  var beforePrint = function() {
    alert('Functionality to run before printing.');
  };
  var afterPrint = function() {
    alert('Functionality to run after printing');
  };

  if (window.matchMedia) {
    var mediaQueryList = window.matchMedia('print');
    mediaQueryList.addListener(function(mql) {
      if (mql.matches) {
        beforePrint();
      } else {
        afterPrint();
      }
    });
  }

  window.onbeforeprint = beforePrint;
  window.onafterprint = afterPrint;
}

function controllaCF(input){
  var pattern = /^[a-zA-Z]{6}[0-9]{2}[a-zA-Z][0-9]{2}[a-zA-Z][0-9]{3}[a-zA-Z]$/;
  var val = $(input).val();
  if (val.search(pattern) == -1){
    $(input).parents(".form-group").addClass("has-error");
    return false;
  }else{
    $(input).parents(".form-group").removeClass("has-error");
    return true;
  }
}

function limitaCaratteriCausale(event) {
  return limitaOnRegex(event, regexCausaleBollettino);
}

function limitaCaratteriData(event) {
  return limitaOnRegex(event, regexData);
}

function limitaLettereNumeri(event) {
  var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);
  if (regexLettereNumeri.test(str)) {
    return true;
  }
  event.preventDefault();
  return false;
}

function limitaNumeri(event) {
  var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);
  if (regexNumeri.test(str)) {
    return true;
  }
  event.preventDefault();
  return false;
}

function limitaOnRegex(event, regex) {
  var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);
  if (regex.test(str)) {
    return true;
  }
  event.preventDefault();
  return false;
}

function openVenditaDialog(idProdotto, formData) {


  if ($(".venditaDialog:visible").length > 0){
    return false;
  }

  $(".venditaDialog").remove();
  $(".custom-modal").remove();
  $(".modal-backdrop").remove();


  var el = $(".categoria-prodotto-item[id='prodotto_"+idProdotto+"']");
  var codFamiglia = $(el).attr("data-codicefamiglia");

  var msgObj = {};
  for(var i=0; i < formData.length; i++){
    var p = formData[i];
    msgObj[p.name] = p.value;
  }

  var msgConferma = "";
  msgConferma+="<div><span>Brand : </span> <span><strong>"+$(el).find(".searchTarget").text()+"</strong></span></div>";

  if(codFamiglia == COD_FAM_PIN || codFamiglia == COD_FAM_GIFT_CARDS || codFamiglia == COD_FAM_RICARICHE_SOLIDALI){
    msgConferma+="<div><span>Importo </span> <span><strong> &euro; "+fromCentsToEuro(msgObj.importo)+"</strong></span></div>";
  }else if(codFamiglia == COD_FAM_RICARICHE ){

    msgConferma+=getRowConferma("Numero Cel.", msgObj.cellulare);

    if(idProdotto == ID_PROD_TRANSFERTO) {
      if(msgObj.operator){
        msgConferma+=getRowConferma("Operatore", decodeURI(msgObj.operator));
        msgConferma+=getRowConferma("Importo", formatCentesimiInEuro(msgObj.importo));
        msgConferma+=getRowConferma("Valore spedito", msgObj.destCurrency + " " + msgObj.destPriceSent);
        msgConferma+=getRowConferma("Valore ricevuto", msgObj.destCurrency + " " + msgObj.destPriceRec);
      }
    }else{
      msgConferma+="<div><span>Importo </span> <span><strong> &euro; "+fromCentsToEuro(msgObj.importo)+"</strong></span></div>";
    }
  }else if(codFamiglia == COD_FAM_PRODOTTI_FINANZIARI){

    if(idProdotto == ID_PROD_CBILL){
      msgConferma+=getRowConferma("Nome Azienda", msgObj.nomeAzienda);
      msgConferma+=getRowConferma("Codice Sia", msgObj.codiceSia);
      msgConferma+=getRowConferma("Codice fiscale", msgObj.codiceFiscale);
      msgConferma+=getRowConferma("Numero bollettino", msgObj.numeroIdBollettino);
      msgConferma+=getRowConferma("Importo", " &euro; " +msgObj.importo); //
    } else if(idProdotto == ID_PROD_MAV_RAV) {
      var tipoProdotto = msgObj.tipoProdotto == "M" ? "Mav" : "Rav";
      msgConferma+=getRowConferma("Tipo prodotto", tipoProdotto);
      msgConferma+=getRowConferma("Codice", msgObj.numeroMavRav);
      msgConferma+=getRowConferma("Importo", " &euro; " +msgObj.importo);
      msgConferma+="<div><span><strong>- Dati pagatore</strong></span></div>";
      msgConferma+=getRowConferma("Tipo pagatore", msgObj.tipoPagatore == "pf" ? "Persona fisica" : "Persona giuridica");
      if(msgObj.tipoPagatore == "pf") {
        msgConferma+=getRowConferma("Nome e Cognome", msgObj.nomePagatore + " " + msgObj.cognomePagatore);
        msgConferma+=getRowConferma("Codice fiscale", msgObj.codFiscalePagatore ? msgObj.codFiscalePagatore.toUpperCase() : msgObj.codFiscalePagatore);
      } else {
        msgConferma+=getRowConferma("Denominazione", msgObj.denominazionePagatore);
        msgConferma+=getRowConferma("Partita IVA", msgObj.partitaIvaPagatore);
      }
      msgConferma+=getRowConferma("Indirizzo", msgObj.indirizzoPagatore + ", " + msgObj.cittaPagatore);
      msgConferma+=getRowConferma("Contatti", msgObj.telefonoPagatore + " " + msgObj.emailPagatore);
      if(msgObj.codFiscaleEsecutore) {
        var tipoDocumentoEsecutore = $(el).find("select[name='tipoDocumentoEsecutore'] option:selected").text();
        msgConferma+="<div><span><strong>- Dati Esecutore</strong></span></div>";
        msgConferma+=getRowConferma("Nome e Cognome", msgObj.nomeEsecutore + " " + msgObj.cognomeEsecutore);
        msgConferma+=getRowConferma("Codice fiscale", msgObj.codFiscaleEsecutore ? msgObj.codFiscaleEsecutore.toUpperCase() : msgObj.codFiscaleEsecutore);
        msgConferma+=getRowConferma("Indirizzo", msgObj.indirizzoEsecutore + ", " + msgObj.cittaEsecutore);
      }
    } else if(idProdotto == ID_PROD_PAGOPA) {
      msgConferma+=getRowConferma("Codice avviso", msgObj.codiceAvviso);
      msgConferma+=getRowConferma("CF/P.IVA Ente Creditore", msgObj.cfGnlEnte);

      msgConferma+="<div><span><strong>- Dati pagatore</strong></span></div>";
      msgConferma+=getRowConferma("Tipo pagatore", msgObj.tipoPagatore == "pf" ? "Persona fisica" : "Persona giuridica");
      if(msgObj.tipoPagatore == "pf") {
        msgConferma+=getRowConferma("Nome e Cognome", msgObj.nomePagatore + " " + msgObj.cognomePagatore);
        msgConferma+=getRowConferma("Codice fiscale", msgObj.codFiscalePagatore ? msgObj.codFiscalePagatore.toUpperCase() : msgObj.codFiscalePagatore);
      } else {
        msgConferma+=getRowConferma("Denominazione", msgObj.denominazionePagatore);
        msgConferma+=getRowConferma("Partita IVA", msgObj.partitaIvaPagatore);
      }
      msgConferma+=getRowConferma("Indirizzo", msgObj.indirizzoPagatore + ", " + msgObj.cittaPagatore);
      msgConferma+=getRowConferma("Contatti", msgObj.telefonoPagatore + " " + msgObj.emailPagatore);
      if(msgObj.codFiscaleEsecutore) {
        var tipoDocumentoEsecutore = $(el).find("select[name='tipoDocumentoEsecutore'] option:selected").text();
        msgConferma+="<div><span><strong>- Dati Esecutore</strong></span></div>";
        msgConferma+=getRowConferma("Nome e Cognome", msgObj.nomeEsecutore + " " + msgObj.cognomeEsecutore);
        msgConferma+=getRowConferma("Codice fiscale", msgObj.codFiscaleEsecutore ? msgObj.codFiscaleEsecutore.toUpperCase() : msgObj.codFiscaleEsecutore);
        msgConferma+=getRowConferma("Indirizzo", msgObj.indirizzoEsecutore + ", " + msgObj.cittaEsecutore);
      }

    } else if(idProdotto == ID_PROD_BOLLOAUTO) {

      msgConferma+="<div><span><strong>- Dati pagatore</strong></span></div>";
      msgConferma+=getRowConferma("Tipo pagatore", msgObj.tipoPagatore == "pf" ? "Persona fisica" : "Persona giuridica");
      if(msgObj.tipoPagatore == "pf") {
        msgConferma+=getRowConferma("Nome e Cognome", msgObj.nomePagatore + " " + msgObj.cognomePagatore);
        msgConferma+=getRowConferma("Codice fiscale", msgObj.codFiscalePagatore ? msgObj.codFiscalePagatore.toUpperCase() : msgObj.codFiscalePagatore);
      } else {
        msgConferma+=getRowConferma("Denominazione", msgObj.denominazionePagatore);
        msgConferma+=getRowConferma("Partita IVA", msgObj.partitaIvaPagatore);
      }
      msgConferma+=getRowConferma("Indirizzo", msgObj.indirizzoPagatore + ", " + msgObj.cittaPagatore);
      msgConferma+=getRowConferma("Contatti", msgObj.telefonoPagatore + " " + msgObj.emailPagatore);
      if(msgObj.codFiscaleEsecutore) {
        var tipoDocumentoEsecutore = $(el).find("select[name='tipoDocumentoEsecutore'] option:selected").text();
        msgConferma+="<div><span><strong>- Dati Esecutore</strong></span></div>";
        msgConferma+=getRowConferma("Nome e Cognome", msgObj.nomeEsecutore + " " + msgObj.cognomeEsecutore);
        msgConferma+=getRowConferma("Codice fiscale", msgObj.codFiscaleEsecutore ? msgObj.codFiscaleEsecutore.toUpperCase() : msgObj.codFiscaleEsecutore);
        msgConferma+=getRowConferma("Indirizzo", msgObj.indirizzoEsecutore + ", " + msgObj.cittaEsecutore);
      }

    } else if(idProdotto == ID_PROD_POSTEPAY){
      msgConferma+=getRowConferma("Numero carta", msgObj.numeroPostePay);
      msgConferma+=getRowConferma("Titolare", msgObj.nomeTitolare + " " + msgObj.cognomeTitolare);
      msgConferma+=getRowConferma("CF Titolare", msgObj.codFiscaleTitolare);
      if(msgObj.nomeVersante != "" && msgObj.cognomeVersante != ""){
        msgConferma+=getRowConferma("Ordinante", msgObj.nomeVersante + " " + msgObj.cognomeVersante);
        msgConferma+=getRowConferma("CF Ordinante", msgObj.codFiscaleVersante);
      }
      msgConferma+=getRowConferma("Importo", " &euro; " +msgObj.importo);
    }else if(idProdotto == ID_PROD_SNAI_VOUCHER){
      msgConferma+=getRowConferma("Codice conto", msgObj.codiceConto);
      msgConferma+=getRowConferma("Codice voucher", msgObj.codiceVoucher);
    } else if(idProdotto == ID_PROD_IPAGOO) {
      msgConferma+=getRowConferma("Email", msgObj.email);
      msgConferma+=getRowConferma("Importo", " &euro; " +msgObj.importo);
    } else if(idProdotto == ID_PROD_BOLLETTINI) {
      var tipoBollettino = $(el).find("select[name='tipoBollettino'] option:selected").text();
      var tipoDocumentoPagatore = $(el).find("select[name='tipoDocumentoPagatore'] option:selected").text();
      msgConferma+=getRowConferma("Tipo bollettino", tipoBollettino ? tipoBollettino : msgObj.tipoBollettino);
      msgConferma+=getRowConferma("Numero conto corrente", msgObj.ccp);
      msgConferma+=getRowConferma("Importo", " &euro; " +msgObj.importo);
      var customTags = "";
      $(".bollettini-biller-additional-field").each(function() {
        customTags += $(this).find("label").eq(0).text() + "&nbsp;<i>" + $(this).find("input").eq(0).val() + "</i>&nbsp;-&nbsp;";
      });
      if(customTags) {
        customTags = customTags.slice(0, "&nbsp;-&nbsp;".length * -1);
        if(msgObj.causale) customTags += "<br />" + msgObj.causale;
      }
      msgConferma+=getRowConferma("Causale", customTags ? customTags : msgObj.causale);
      if(msgObj.codiceBollettino) msgConferma+=getRowConferma("Codice bollettino", msgObj.codiceBollettino);
      msgConferma+="<div><span><strong>- Dati pagatore</strong></span></div>";
      msgConferma+=getRowConferma("Tipo pagatore", msgObj.tipoPagatore == "pf" ? "Persona fisica" : "Persona giuridica");
      if(msgObj.tipoPagatore == "pf") {
        msgConferma+=getRowConferma("Nome e Cognome", msgObj.nomePagatore + " " + msgObj.cognomePagatore);
        msgConferma+=getRowConferma("Codice fiscale", msgObj.codFiscalePagatore);
      } else {
        msgConferma+=getRowConferma("Denominazione", msgObj.denominazionePagatore);
        msgConferma+=getRowConferma("Partita IVA", msgObj.partitaIvaPagatore);
      }
      msgConferma+=getRowConferma("Indirizzo", msgObj.indirizzoPagatore + ", " + msgObj.cittaPagatore);
      msgConferma+=getRowConferma("Contatti", msgObj.telefonoPagatore + " " + msgObj.emailPagatore);
      //msgConferma+=getRowConferma("Email", msgObj.emailPagatore);
      //msgConferma+=getRowConferma("Telefono", msgObj.telefonoPagatore);
      //msgConferma+=getRowConferma("Indirizzo", msgObj.indirizzoPagatore + ", " + msgObj.capPagatore + " " + msgObj.cittaPagatore + " (" + msgObj.provinciaPagatore + ") - " + msgObj.nazionePagatore);
      //msgConferma+=getRowConferma("Contatti", msgObj.telefonoPagatore + " " + msgObj.emailPagatore);
      //msgConferma+=getRowConferma("Documento", (tipoDocumentoPagatore ? tipoDocumentoPagatore : msgObj.tipoDocumentoPagatore) + " n. " + msgObj.numeroDocumentoPagatore + " rilasciato da " + msgObj.enteDocumentoPagatore + " - Data rilascio " + msgObj.dataRilascioDocumentoPagatore + " Data scadenza " + msgObj.dataScadenzaDocumentoPagatore);
      if(msgObj.codFiscaleEsecutore) {
        var tipoDocumentoEsecutore = $(el).find("select[name='tipoDocumentoEsecutore'] option:selected").text();
        msgConferma+="<div><span><strong>- Dati Esecutore</strong></span></div>";
        msgConferma+=getRowConferma("Nome e Cognome", msgObj.nomeEsecutore + " " + msgObj.cognomeEsecutore);
        msgConferma+=getRowConferma("Codice fiscale", msgObj.codFiscaleEsecutore);
        msgConferma+=getRowConferma("Indirizzo", msgObj.indirizzoEsecutore + ", " + msgObj.cittaEsecutore);
        //msgConferma+=getRowConferma("Indirizzo", msgObj.indirizzoEsecutore + ", " + msgObj.capEsecutore + " " + msgObj.cittaEsecutore + " (" + msgObj.provinciaEsecutore + ") - " + msgObj.nazioneEsecutore);
        //msgConferma+=getRowConferma("Contatti", msgObj.telefonoEsecutore + " " + msgObj.emailEsecutore);
        //msgConferma+=getRowConferma("Documento", (tipoDocumentoEsecutore ? tipoDocumentoEsecutore : msgObj.tipoDocumentoEsecutore) + " n. " + msgObj.numeroDocumentoEsecutore + " rilasciato da " + msgObj.enteDocumentoEsecutore + " - Data rilascio " + msgObj.dataRilascioDocumentoEsecutore + " Data scadenza " + msgObj.dataScadenzaDocumentoEsecutore);
      }
    } else if(idProdotto == ID_PROD_BOLLETTINO_FRECCIA) {
      msgConferma+=getRowConferma("Iban", msgObj.iban);
      msgConferma+=getRowConferma("Cin Importo", msgObj.cinImporto);
      msgConferma+=getRowConferma("Cin Intermedio", msgObj.cinIntermedio);
      msgConferma+=getRowConferma("Cin Complessivo", msgObj.cinComplessivo);
      msgConferma+=getRowConferma("Importo", " &euro; " +msgObj.importo);
      var customTags = "";
      $(".bollettini-biller-additional-field").each(function() {
        customTags += $(this).find("label").eq(0).text() + "&nbsp;<i>" + $(this).find("input").eq(0).val() + "</i>&nbsp;-&nbsp;";
      });
      if(customTags) {
        customTags = customTags.slice(0, "&nbsp;-&nbsp;".length * -1);
        if(msgObj.causale) customTags += "<br />" + msgObj.causale;
      }
      msgConferma+=getRowConferma("Causale", customTags ? customTags : msgObj.causaleVersamento);
      if(msgObj.codiceBollettino) msgConferma+=getRowConferma("Codice bollettino", msgObj.codiceBollettino);
      msgConferma+="<div><span><strong>- Dati pagatore</strong></span></div>";
      msgConferma+=getRowConferma("Tipo pagatore", msgObj.tipoPagatore == "pf" ? "Persona fisica" : "Persona giuridica");
      if(msgObj.tipoPagatore == "pf") {
        msgConferma+=getRowConferma("Nome e Cognome", msgObj.nomePagatore + " " + msgObj.cognomePagatore);
        msgConferma+=getRowConferma("Codice fiscale", msgObj.codFiscalePagatore);
      } else {
        msgConferma+=getRowConferma("Denominazione", msgObj.denominazionePagatore);
        msgConferma+=getRowConferma("Partita IVA", msgObj.partitaIvaPagatore);
      }
      msgConferma+=getRowConferma("Indirizzo", msgObj.indirizzoPagatore + ", " + msgObj.cittaPagatore);
      msgConferma+=getRowConferma("Contatti", msgObj.telefonoPagatore + " " + msgObj.emailPagatore);
      //msgConferma+=getRowConferma("Email", msgObj.emailPagatore);
      //msgConferma+=getRowConferma("Telefono", msgObj.telefonoPagatore);
      //msgConferma+=getRowConferma("Indirizzo", msgObj.indirizzoPagatore + ", " + msgObj.capPagatore + " " + msgObj.cittaPagatore + " (" + msgObj.provinciaPagatore + ") - " + msgObj.nazionePagatore);
      //msgConferma+=getRowConferma("Contatti", msgObj.telefonoPagatore + " " + msgObj.emailPagatore);
      //msgConferma+=getRowConferma("Documento", (tipoDocumentoPagatore ? tipoDocumentoPagatore : msgObj.tipoDocumentoPagatore) + " n. " + msgObj.numeroDocumentoPagatore + " rilasciato da " + msgObj.enteDocumentoPagatore + " - Data rilascio " + msgObj.dataRilascioDocumentoPagatore + " Data scadenza " + msgObj.dataScadenzaDocumentoPagatore);
      if(msgObj.codFiscaleEsecutore) {
        var tipoDocumentoEsecutore = $(el).find("select[name='tipoDocumentoEsecutore'] option:selected").text();
        msgConferma+="<div><span><strong>- Dati Esecutore</strong></span></div>";
        msgConferma+=getRowConferma("Nome e Cognome", msgObj.nomeEsecutore + " " + msgObj.cognomeEsecutore);
        msgConferma+=getRowConferma("Codice fiscale", msgObj.codFiscaleEsecutore);
        msgConferma+=getRowConferma("Indirizzo", msgObj.indirizzoEsecutore + ", " + msgObj.cittaEsecutore);
        //msgConferma+=getRowConferma("Indirizzo", msgObj.indirizzoEsecutore + ", " + msgObj.capEsecutore + " " + msgObj.cittaEsecutore + " (" + msgObj.provinciaEsecutore + ") - " + msgObj.nazioneEsecutore);
        //msgConferma+=getRowConferma("Contatti", msgObj.telefonoEsecutore + " " + msgObj.emailEsecutore);
        //msgConferma+=getRowConferma("Documento", (tipoDocumentoEsecutore ? tipoDocumentoEsecutore : msgObj.tipoDocumentoEsecutore) + " n. " + msgObj.numeroDocumentoEsecutore + " rilasciato da " + msgObj.enteDocumentoEsecutore + " - Data rilascio " + msgObj.dataRilascioDocumentoEsecutore + " Data scadenza " + msgObj.dataScadenzaDocumentoEsecutore);
      }
    }
  } else if(codFamiglia == COD_FAM_ATTIVAZIONE_SIM){
    if(idProdotto == ID_PROD_ATTIVAZIONE_SIM_ILIAD) {
      msgConferma+=getRowConferma("Importo", fromCentsToEuro(msgObj.importo));
      msgConferma+=getRowConferma("Codice SIM", msgObj.codiceSim);
      msgConferma+=getRowConferma("Cliente", msgObj.nome + " " + msgObj.cognome + " - " + msgObj.cf);
      msgConferma+=getRowConferma("Telefono", msgObj.telefono);
      msgConferma+=getRowConferma("Indirizzo", msgObj.toponimo + " " + msgObj.nomestrada + " " + msgObj.civico + ", " + msgObj.cap + " " + (msgObj.frazione ? msgObj.frazione + " " : "") + msgObj.comune + " (" + msgObj.provincia + ")");
    }
  } else if(codFamiglia == COD_FAM_PRODOTTI_CAMERALI) {

    if(idProdotto == ID_PROD_RICERCA_PERSONA) {
      msgConferma+="<div><span><strong>- Dati di Ricerca</strong></span></div>";
      msgConferma+=getRowConferma("Nome e Cognome", msgObj.nome + " " + msgObj.cognome);
      msgConferma+=getRowConferma("Codice fiscale", msgObj.cf);
      msgConferma+=getRowConferma("Data di nascita", msgObj.dataNascita);
      msgConferma+=getRowConferma("Citt&agrave; di nascita", msgObj.cittaNascita);
      msgConferma+=getRowConferma("Provincia di nascita", msgObj.provinciaNascita);
      msgConferma+=getRowConferma("Sesso", msgObj.sesso);
      msgConferma+=getRowConferma("Email destinatario", msgObj.email);
    } else if(idProdotto == ID_PROD_RICERCA_AZIENDA) {
      msgConferma+="<div><span><strong>- Dati di Ricerca</strong></span></div>";
      msgConferma+=getRowConferma("Denominazione", msgObj.denominazione);
      msgConferma+=getRowConferma("Codice fiscale", msgObj.cf);
      msgConferma+=getRowConferma("Partita IVA", msgObj.partitaIva);
      msgConferma+=getRowConferma("Numero Rea", msgObj.codiceRea);
      msgConferma+=getRowConferma("Provincia Cciaa", msgObj.provinciaCciaa);
      //msgConferma+=getRowConferma("Numero Crif", msgObj.numeroCrif);
      msgConferma+=getRowConferma("Tipo forma giuridica", msgObj.formaGiuridica);
      msgConferma+=getRowConferma("Indirizzo", msgObj.indirizzo);
      msgConferma+=getRowConferma("Nazione", msgObj.nazione);
      msgConferma+=getRowConferma("Provincia", msgObj.provincia);
      msgConferma+=getRowConferma("Citt&agrave;", msgObj.citta);
      msgConferma+=getRowConferma("CAP", msgObj.cap);
      msgConferma+=getRowConferma("Email destinatario", msgObj.email);
    } else if(idProdotto == ID_PROD_REPORT_PERSONA) {
      msgConferma+="<div><span><strong>- Dati Report persona</strong></span></div>";
      //msgConferma+=getRowConferma("Tipo Report", msgObj.tipoReport);
      msgConferma+=getRowConferma("Codice fiscale", msgObj.cf);
      msgConferma+=getRowConferma("Nome", msgObj.nome);
      msgConferma+=getRowConferma("Cognome", msgObj.cognome);
      msgConferma+=getRowConferma("Sesso", msgObj.sesso);
      msgConferma+=getRowConferma("Data di nascita", msgObj.dataNascita);
      msgConferma+=getRowConferma("Luogo di nascita", msgObj.luogoNascita);
      msgConferma+=getRowConferma("Provincia di nascita", msgObj.provinciaNascita);
      msgConferma+=getRowConferma("Email destinatario", msgObj.email);
    } else if(idProdotto == ID_PROD_REPORT_AZIENDA || idProdotto == ID_PROD_COMPANY_CARD_ORDINARIA || idProdotto == ID_PROD_COMPANY_CARD_STORICA || idProdotto == ID_PROD_REPORT_AZIENDA_CATASTO) {
      msgConferma+="<div><span><strong>- Dati Report Azienda</strong></span></div>";
      //msgConferma+=getRowConferma("Tipo Report", msgObj.tipoReport);
      msgConferma+=getRowConferma("Provincia Cciaa", msgObj.cciaa);
      msgConferma+=getRowConferma("Numero Rea", msgObj.nrea);
      msgConferma+=getRowConferma("Codice Fiscale o Partita Iva", msgObj.cf);
      //msgConferma+=getRowConferma("Numero Crif", msgObj.numeroCrif);
      msgConferma+=getRowConferma("Email destinatario", msgObj.email);
    } else if(idProdotto == ID_PROD_VISURA_ORDINARIA) {
      msgConferma+="<div><span><strong>- Dati Visura Camerale Ordinaria</strong></span></div>";
      msgConferma+=getRowConferma("Provincia Cciaa", msgObj.cciaa);
      msgConferma+=getRowConferma("Numero Rea", msgObj.nrea);
      msgConferma+=getRowConferma("Codice Fiscale o Partita Iva", msgObj.codiceFiscale);
      msgConferma+=getRowConferma("Email destinatario", msgObj.email);
    } else if(idProdotto == ID_PROD_VISURA_STORICA) {
      msgConferma+="<div><span><strong>- Dati Visura Camerale Storica</strong></span></div>";
      msgConferma+=getRowConferma("Provincia Cciaa", msgObj.cciaa);
      msgConferma+=getRowConferma("Numero Rea", msgObj.nrea);
      msgConferma+=getRowConferma("Codice Fiscale o Partita Iva", msgObj.codiceFiscale);
      msgConferma+=getRowConferma("Email destinatario", msgObj.email);
    }

  } else if(codFamiglia == COD_FAM_RICARICA_WALLET) {
    if(idProdotto == ID_PROD_AMAZON_WALLET) {
      msgConferma+=getRowConferma("Importo", msgObj.importo);
      msgConferma+=getRowConferma("Telefono / Codice Conto", msgObj.codiceConto);
    }
  }

  var modal = "";
  // apri modal
  modal += '<div class="modal fade venditaDialog" id="simple-modal" tabindex="-1" role="dialog" aria-labelledby="simple-modal" aria-hidden="true">';
  modal += '<div class="modal-dialog" role="document">';
  modal += '<div class="modal-content" id="controllaDati">';
  modal += '<div class="modal-header">';
  modal += '<h5 class="modal-title" id="simple-modal-label">Controlla i dati e conferma</h5>';
  modal += '<button type="button" class="close chiudi close-modal" aria-label="Close">';
  modal += '<span aria-hidden="true">&times;</span>';
  modal += '</button>';
  modal += '</div>';
  modal += '<div class="modal-body">';
  modal += msgConferma;
  modal += '</div>';
  modal += '<div class="modal-footer">';

  modal += '<button type="button" id="annullaAzione" class="btn btn-secondary pointer chiudi">Chiudi</button>';

  modal += '<button type="button" id="conferma" class="btn btn-primary sovrascriviTema pointer" onclick="eseguiVendita('+idProdotto+', '+codFamiglia+')">Ok</button>';

  modal += '</div>';
  // chiudi Modal
  modal += '</div></div></div>';

  $("body").append(modal);

  $(".venditaDialog").modal("show");

  $(".venditaDialog").find(".chiudi").click(function() {
    removeCustomDialog();
  });
}

// LETTURA BARCODE PAYSAFE

var psCashBarcodeChars = 19;

function onKeyUpBarcodePaysafeCash(input, event) {
  if(input.value.length >= psCashBarcodeChars) {

    var ean = input.value.substring(0, 13);
    var idTransazione = input.value.substring(13, input.value.length - 1);
    var checkDigit = input.value.substring(input.value.length - 1);
    var validation = validatePaysafeCheckDigit(ean + "" + idTransazione, checkDigit);
    $('#psCashTransazione').val(idTransazione);
    $('#psCashImporto').val(ean);
    //alert("ean = " + ean + " - idtransazione = " + idTransazione + " - checkDigit = " + checkDigit + " validation = " + (validation ? "true" : "false"));
  }
}

function validatePaysafeCheckDigit(str, checkDigit) {
  var somma = 0;
  for (var i = 0; i < str.length; i++) {
    somma += (str.charAt(i) * (i%2==0 ? 1 : 3));
  }
  return checkDigit == 10 - (somma%10);
}


// WEBCAM

var video, image;
var currentTrack;
var take_photo_btn, delete_photo_btn, send_photo_btn, refresh_webcam_btn;

function abilitaWebcam() {

  if(browserAgent == "isIE" || browserAgent == "isEdge") {
    $("#ppNotIE").show();
  }
  else {
    $("#ppNotIE").hide();
    video = document.querySelector('#camera-stream');
    image = document.querySelector('#snap');
    take_photo_btn = document.querySelector('#take-photo');
    delete_photo_btn = document.querySelector('#delete-photo');
    send_photo_btn = document.querySelector('#send-photo');
    refresh_webcam_btn = document.querySelector('#refresh-webcam');

    setWebcamButtonListeners();
    hideWebcamUI();
  }
}

function startWebcam() {

  if ( !navigator )
    navigator = {};

  if ( !navigator.mediaDevices )
    navigator.mediaDevices = {};

  if ( !navigator.mediaDevices.getUserMedia ) {
    logOnConsole("[startWebcam] mediaDevices.getUserMedia non supportato, viene ridefinito");
    navigator.mediaDevices.getUserMedia = function( constraints ) {
      return new Promise( function( resolve, reject ) {
        var getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;

        if ( !getUserMedia )
          reject({
            message: "This browser doesn't support camera capture.",
            name: "BrowserSupportError"
          });

        else
          getUserMedia.call( navigator, constraints, resolve, reject );
      });
    }
  } else {
    logOnConsole("[startWebcam] mediaDevices.getUserMedia definito");
  }

  getStream();
}

function getStream() {
  if (window.stream) {
    window.stream.getTracks().forEach(function(track) {
      track.stop();
    });
  }

  var constraints = {
    video:  true
  };

  navigator.mediaDevices.getUserMedia(constraints).then(gotStream).catch(function(error){handleWebcamError(error)});
}

function gotStream(stream) {

  if(stream.getVideoTracks().length > 0)
    currentTrack = stream.getVideoTracks()[0];

  //Create an object URL for the video stream and
  // set it as src of our HTLM video element.
  video.src = window.URL.createObjectURL(stream);

  // Play the video element to start the stream.
  video.play();
  video.onplay = function() {
    showVideo();
  };
}

function handleWebcamError(error) {
  hideWebcamUI();
  $("#ppMissingWebcam").show();
  $("#refresh-webcam").show();
  logOnConsole('Error: ' + error.message);
}

function setWebcamButtonListeners() {

  $("#ppMethodTab a[href*='ppWebcam']").on("click", startWebcam);
  $("#ppMethodTab").not("href*='ppWebcam'").on("click", stopWebcam);

  take_photo_btn.addEventListener("click", function(e) {

    e.preventDefault();

    var snapshot = takeSnapshot();

    // Show image.
    image.setAttribute('src', snapshot);
    $("#snap").show();

    // Enable delete and save buttons
    $("#delete-photo").show();
    $("#send-photo").show();
    $("#take-photo").hide();

    // Set the href attribute of the download button to the snap url.
    send_photo_btn.href = snapshot;

    // Pause video playback of stream.
    video.pause();
  });

  delete_photo_btn.addEventListener("click", function(e){

    e.preventDefault();

    // Hide image.
    image.setAttribute('src', "");
    $("#snap").hide();

    // Disable delete and save buttons
    $("#delete-photo").hide();
    $("#send-photo").hide();
    $("#take-photo").show();

    // Resume playback of stream.
    video.play();
  });

  send_photo_btn.addEventListener("click", function(e){

    e.preventDefault();

    var hexString = image.getAttribute('src');
    var contentType = hexString.split(';')[0].split(':')[1];
    var blob = b64toBlob(hexString, contentType);
    blob.name = "doc-" + $.now() + ".png";
    var allegati = [blob];

    // carica l'allegato nella form
    var preUpload = browserAgent == "isIE" || browserAgent == "isEdge" ? prepareUpload_IE : prepareUpload;
    prepareFilesForUpload(allegati, preUpload, PP_MODALITA_WEBCAM);

    // Hide image.
    image.setAttribute('src', "");
    $("#snap").hide();

    // Disable delete and save buttons
    $("#delete-photo").hide();
    $("#send-photo").hide();
    $("#take-photo").show();

    // Resume playback of stream.
    video.play();

  });

  refresh_webcam_btn.addEventListener("click", function(e) {

    e.preventDefault();

    $("#ppMissingWebcam").hide();
    $("#refresh-webcam").hide();

    startWebcam();

  });
}

function showVideo(){
  // Display the video stream and the controls.
  hideWebcamUI();
  $("#camera-stream").show();
  $("#take-photo").show();
  $("#close-photo").show();
}

function takeSnapshot(){
  // Here we're using a trick that involves a hidden canvas element.
  var hidden_canvas = document.querySelector('canvas#snapshot'),
    context = hidden_canvas.getContext('2d');

  var width = video.videoWidth,
    height = video.videoHeight;

  if (width && height) {
    // Setup a canvas with the same dimensions as the video.
    hidden_canvas.width = width;
    hidden_canvas.height = height;

    // Make a copy of the current frame in the video on the canvas.
    context.drawImage(video, 0, 0, width, height);

    // Turn the canvas image into a dataURL that can be used as a src for our photo.
    return hidden_canvas.toDataURL('image/png');

  }
}

function hideWebcamUI(){
  // Helper function for clearing the app UI.

  $("#delete-photo").hide();
  $("#send-photo").hide();
  $("#take-photo").hide();
  $("#close-photo").hide();

  $("#camera-stream").hide();
  $("#snap").hide();

  $("#ppMissingWebcam").hide();
  $("#refresh-webcam").hide();
}

function stopWebcam() {
  if(currentTrack)
    currentTrack.stop();

  $("#ppMethodTab a").eq(0).tab('show');
}


// WEBSOCKET + LETTURA QR-CODE PAGO PA

function openModalQrCodePagoPa() {
  var msg = "";
  msg += "<div id='ppMobile' class='tab-pane'>";
  msg += "<div style='display: flex; align-items: center; justify-content: center; margin-bottom: 20px'>";
  msg += "<div class='col col-md-4 qrCodeFormVendita text-center' id='pagopaQrCode'></div>";
  msg += "<div class='col col-md-8 text-justify'>Scansiona con il telefono il QR code qui a sinistra e utilizza la pagina proposta per inquadrare il QR code dell'avviso, in questo modo i dati saranno automaticamente acquisiti sulla form di pagamento.<br />Per ragioni di sicurezza il QR code sar&agrave; rigenerato ad ogni ricarica, pertanto non potr&agrave; essere utilizzato quello di un pagamento precedente.<br />Qualora dovessero presentarsi problemi di invio, provare a chiudere e riaprire questa sezione con il tasto X in alto a destra e ad effettuare una nuova scansione.</div>";
  msg += "</div>";
  msg += "<div class='col-xs-12 text-center'><strong>Non chiudere questa finestra durante la scansione!</strong></div>";
  msg += "</div>";

  var dialogParams = {
    idContent : "qrCodePagoPa",
    title : "Leggi il QR code con il telefono",
    msg : msg,
    actionAnnulla : "chiudiWebSocket"
  }
  openGenericDialog(dialogParams);

  abilitaQrCodeScannerPagoPa(readQrCodePagoPa);
}

function readQrCodePagoPaFromInput(e, input) {
  if ( e.which === 13 ) { //blocca il tasto invio
    e.preventDefault();
  }
  var hasChanged = $(e.target).val() != $(e.target).data("previousValue");
  if(hasChanged && input.val().length >= 52) {
    splitQrCodePagoPa(input.val());
    aggiornaFrontMonitor($("div#prodotto_" + ID_PROD_PAGOPA));
  }
}

function abilitaQrCodeScannerPagoPa(postReadAction) {
  communicationId = idUtente + "_" + $.now();
  senderId = WSS_DESKTOP_SENDER_PREFIX + "_" + communicationId;

  initPagoPaQrCode(communicationId);
  initWebSocket(senderId, readQrCodePagoPa);
}

function initPagoPaQrCode(communicationId) {
  var qrLink = PAYMAT_BASE_URL + "/p/qrcr?cid=" + communicationId;
  logOnConsole("QrLink: " + qrLink);
  var qrCode = generaQrCode(qrLink, 500);
  $("#pagopaQrCode").empty();
  $("#pagopaQrCode").append(qrCode);
}

function readQrCodePagoPa(obj) {
  if (obj && obj.body && obj.body.content) {
    splitQrCodePagoPa(obj.body.content);
  } else {
    logOnConsole("Ricevuto messaggio senza content");
  }
}

function splitQrCodePagoPa(value) {
  logOnConsole("[splitQrCodePagoPa] QRcode Scanned: " + value);
  var datiAvviso = null;
  if(value.indexOf("|") !== -1)
    datiAvviso = value.split("|");
  else if(value.indexOf("§") !== -1)
    datiAvviso = value.split("§");
  else if(value.indexOf("PAGOPA") !== -1) {
    var separatore = value.substring("PAGOPA".length, "PAGOPA".length + 1);
    datiAvviso = value.split(separatore);
  }

  if(datiAvviso) {
    //campo 0 fisso PAGOPA
    //campo 1 versione es. 002
    //campo 2 numero avviso
    //campo 3 cf o gln ente creditore
    //campo 4 importo in centesimi
    $("#prodotto_" + ID_PROD_PAGOPA).find("input[name=codiceAvviso]").val(datiAvviso[2]);
    $("#prodotto_" + ID_PROD_PAGOPA).find("input[name=cfGnlEnte]").val(datiAvviso[3]);

    $("#annullaAzione").click();
  } else {
    openGenericErrorDialog("Il QR code non risulta ben formato. Inserire manualmente i dati dell'avviso.");
  }

  $("#pagoPaQrCodeScan").val("");
}

// WEBSOCKET POSTEPAY

function abilitaPPMobileCamera() {
  communicationId = idUtente + "_" + $.now();
  senderId = WSS_DESKTOP_SENDER_PREFIX + "_" + communicationId;

  initPPQrCode(communicationId);
  initWebSocket(senderId, handleWsMessagePp);
}

function initPPQrCode(communicationId) {
  var qrLink = PAYMAT_BASE_URL + "/p/mc?cid=" + communicationId;
  logOnConsole("QrLink: " + qrLink);
  var qrCode = generaQrCode(qrLink, 500);
  $("#ppQrCode").empty();
  $("#ppQrCode").append(qrCode);
}

function handleWsMessagePp(obj) {
  var allegati = getAllegatiFromWsMessage(obj);

  // carica l'allegato nella form
  var preUpload = browserAgent == "isIE" || browserAgent == "isEdge" ? prepareUpload_IE : prepareUpload;
  prepareFilesForUpload(allegati, preUpload, PP_MODALITA_MOBILE);
}

function getAllegatiFromWsMessage(obj) {
  var allegati = [];
  for(var i=0; i<obj.body.allegati.length; i++) {
    var allegato = obj.body.allegati[i];
    var contentType = allegato.hexString.split(';')[0].split(':')[1];
    var blob = b64toBlob(allegato.hexString, contentType);
    blob.name = allegato.filename;
    allegati[i] = blob;
  }
  return allegati;
}

// WEBSOCKET GENERIC

function initWebSocket(senderId, messageHandlingCallback) {

  mcWs = new WebSocket(WEBSOCKET_MOBILE_CAMERA);

  mcWs.onopen = function() {
    logOnConsole("[onopen] websocket aperta");

    // reset delle retry
    mcWsInRetry = false;
    mcWsReconnectCount = 0;
    if(mcWsReconnectionInterval) {
      clearInterval(mcWsReconnectionInterval);
      mcWsReconnectionInterval = null;
    }

    // messaggio di identificazione verso wsserver
    sendMessageToWebSocket(WSS_SERVICE_ID, 0, $.now(), senderId, null, mcWs);
  };

  mcWs.onclose = function() {
    logOnConsole("[onclose] Connessione interrotta - eseguo reconnect");
    mcWs = null;
    reconnectWebSocket(senderId, messageHandlingCallback);
  };

  mcWs.onerror = function(err) {
    logOnConsole("[onerror] Websocket onErr");
    if(mcWs.readyState != mcWs.OPEN) {
      try {
        mcWs.close();
      } catch(e) {
        logOnConsole("onerror] Errore nella close " + e);
      }
      mcWs = null;
      reconnectWebSocket(senderId, messageHandlingCallback);
    }
  };

  mcWs.onmessage = function (evt) {
    logOnConsole("[onmessage] ricevuto messaggio");
    var obj = JSON.parse(evt.data);
    logOnConsole(obj);
    messageHandlingCallback(obj);
  };

}

function reconnectWebSocket(senderId, messageHandlingCallback) {
  if(!mcWsInRetry) {
    mcWsInRetry = true;

    mcWsReconnectionInterval = setInterval(
      function() {
        mcWsReconnectCount++;

        if(mcWsReconnectCount < mcWsMaxRetryNumber) {
          // chiude eventuali connessioni appese
          if(mcWs) {
            try {
              mcWs.close();
              mcWs = null;
              logOnConsole("Socket chiusa");
            } catch (e) {
              logOnConsole("Errore nella chiusura della socket " + e);
            }
          }
          // reinizializza la connessione
          initWebSocket(senderId, messageHandlingCallback);

        } else {
          logOnConsole("[reconnectWebSocket] Superato il limite di retry");
          clearInterval(mcWsReconnectionInterval);
          mcWsReconnectionInterval = null;
        }
      },
      mcWsRetryInterval //tempo di attesa tra una retry e l'altra
    );
  } else {
    logOnConsole("[reconnectWebSocket] Sono gia' in reconnect retry ignoro la richiesta di reconnect");
  }
}

// body deve essere un oggetto, es body = { "allegati": allegati };
function sendMessageToWebSocket(serviceId, messageType, messageId, senderId, body, ws) {
  var message = buildMessageToWebSocket(serviceId, messageType, messageId, senderId, body);
  logOnConsole("[sendMessageToWebSocket]" + message);
  ws.send(message);
}

function buildMessageToWebSocket(serviceId, messageType, messageId, senderId, body) {
  var message = {"serviceId":serviceId, "messageType":messageType, "messageId":messageId, "senderId":senderId, "body": body};
  return JSON.stringify(message);
}

function chiudiWebSocket() {
  if(mcWs) {

    // messaggio di chiusura con i device connessi
    sendMessageToWebSocket(WSS_SERVICE_ID, 2, $.now(), senderId, null, mcWs);

    //disabilita l'handler impostato in precedenza per non scatenare riconnessioni
    mcWs.onclose = function () {};

    try {
      mcWs.close();
      mcWs = null;
      logOnConsole("[chiudiWebSocket] Socket chiusa");
    } catch (e) {
      logOnConsole("[chiudiWebSocket] Errore nella chiusura della socket " + e);
    }
  }
}

function logOnConsole(message) {
  if(JS_CONSOLE_LOGGING_ENABLED)
    console.log(message);
}

//UPLOAD IMMAGINI
var READY_TO_UPLOAD = false;
var filesArray = [];
var NUM_FILES = 2; // tarato su iliad, andrebbe generalizzato
var MAX_WIDTH = 2700;
var MAX_HEIGHT = 2300;
function abilitaUploadFiles(){

  var preUpload = browserAgent == "isIE" || browserAgent == "isEdge" ? prepareUpload_IE : prepareUpload;

  var input = document.getElementById('js-upload-files');
  if(input) {
    input.addEventListener('change', function(e){
      var files = e.target.files;
      prepareFilesForUpload(files, preUpload, PP_MODALITA_FORM);
      input.value = "";
    })

    if(browserAgent == "isIE"){
      input.addEventListener('blur', function(e){
        input.value = "";
      })
    }
  }

  var dropZone = document.getElementById('drop-zone');
  if(dropZone) {
    dropZone.ondrop = function(e) {
      e.preventDefault();
      $(this).removeClass("drop");
      var files = e.dataTransfer.files;
      prepareFilesForUpload(files, preUpload, PP_MODALITA_FORM);
    }

    dropZone.ondragover = function() {
      $(this).addClass("drop");
      return false;
    }

    dropZone.ondragleave = function() {
      $(this).removeClass("drop");
      return false;
    }
  }
}

// chiama il preUpload o il prepareUpload_PDF su un array di file
function prepareFilesForUpload(files, preUpload, tipoUpload) {
  for(var i=0; i<files.length;i++){
    var file = files[i];
    file["modalita"] = tipoUpload;
    if(file.type == "image/png" || file.type == "image/jpeg"){
      preUpload(file, i);
    }else if(file.type == "application/pdf"){
      prepareUpload_PDF(file, i);
    }
  }
}

//inserisce file in filesArray e crea miniatura
function prepareUpload(file, index) {

  if(filesArray.length < NUM_FILES){
    var timeStamp = Date.now()+"_"+index;
    file.id = timeStamp;
    if(file.size <= maxFileSize){
      filesArray.push(file);
      creaMiniatura(file, "file_"+timeStamp);
      var html = "<div id='file_"+timeStamp+"' class='list-group-item list-group-item-success'>"+ file.name + "<span onclick='removeFilesFromArray(\"" + timeStamp +"\", event)' class='pointer pull-right glyphicon glyphicon-trash'></span></div>";
      $("#uploadedFiles .list-group").append(html);
      scriviFilesMancanti();
    }else{
      resizeImgFile(file);
    }
  }else{
    var dialogParams = {
      idContent : "erroreVendita",
      title : "Errore",
      msg : "Impossibile caricare piu di "+ NUM_FILES +" files.<br />Il file <strong>" + file.name + "</strong> non sarà caricato.",
      type : "error"
    }
    openGenericDialog(dialogParams);
  }
}

//ridimesiona il file (immagine) e lotrasforma in stringa per iF. poi lo passa a setFileArray_IE che ne crea anche una miniatura
function prepareUpload_IE(file, index){
  if(filesArray.length < NUM_FILES){
    var timeStamp = Date.now()+"_"+index;

    fromFileToStringObj(file, timeStamp);

  }else{
    var dialogParams = {
      idContent : "erroreVendita",
      title : "impossibile caricare piu di "+ NUM_FILES +" files",
      msg : "Il file <strong>" + file.name + "</strong> non sarà caricato.",
      type : "error"
    }
    openGenericDialog(dialogParams);
  }
}


function prepareUpload_PDF(file, index){
  if(filesArray.length < NUM_FILES){
    var timeStamp = Date.now()+"_"+index;
    file.id = timeStamp;
    if(file.size <= maxFileSize){

      if(browserAgent == "isIE"){

        var reader = new FileReader();
        reader.readAsDataURL(file);

        reader.onload = function () {
          var fileObj = {id:timeStamp,string:reader.result, name:file.name, modalita:file["modalita"]};
          filesArray.push(fileObj);
        }

      }else{
        filesArray.push(file);
      }


      creaMiniatura_PDF(file, "file_"+timeStamp);
      var html = "<div id='file_"+timeStamp+"' class='list-group-item list-group-item-success'>"+ file.name + "<span onclick='removeFilesFromArray(\"" + timeStamp +"\", event)' class='pointer pull-right glyphicon glyphicon-trash'></span></div>";
      $("#uploadedFiles .list-group").append(html);
      scriviFilesMancanti();
    }else{
      var dialogParams = {
        idContent : "erroreVendita",
        title : "Il file <strong>" + file.name + "</strong> e' troppo grande.",
        msg : "Ridimensionare il file o provare a caricarlo con estensione .jpg o .png" ,
        type : "error"
      }
      openGenericDialog(dialogParams);
    }
  }else{
    var dialogParams = {
      idContent : "erroreVendita",
      title : "impossibile caricare piu di "+ NUM_FILES +" files",
      msg : "Il file <strong>" + file.name + "</strong> non sarà caricato.",
      type : "error"
    }
    openGenericDialog(dialogParams);
  }
}



var arrayToUpload = [];
var arrayMetadataToUpload = [];
function startUpload(){
  arrayToUpload = [];
  arrayMetadataToUpload = [];
  for(var i = 0; i < filesArray.length; i++){
    (function(file){
      var reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = function () {
        arrayMetadataToUpload.push(file["modalita"]);
        pushToArray(reader.result);
      };
      reader.onerror = function (error) {
        logOnConsole('Error: ' + error);
      };
    })(filesArray[i]);
  }
}

function startUpload_IE(){
  arrayToUpload = [];
  arrayMetadataToUpload = [];
  for(var i = 0; i < filesArray.length; i++){
    arrayMetadataToUpload.push(filesArray[i]["modalita"]);
    pushToArray(filesArray[i].string);
  }
}


function pushToArray(fileString){
  var k = "Immagine "+(arrayToUpload.length+1);
  var obj = {};
  obj[k] = fileString;
  arrayToUpload.push(obj);
  if(arrayToUpload.length == NUM_FILES){
    READY_TO_UPLOAD = true;
    //logOnConsole("invio immagini al server : "+ JSON.stringify(arrayToUpload));
  }else{
    //logOnConsole("numero file necessari : "+ NUM_FILES);
  }
}

function resizeImgFile(file, w, h){

  $("#drop-zone").addClass("onResize");

  var max_width = w || MAX_WIDTH;
  var max_height = h || MAX_HEIGHT;

  var reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onload = function () {
    var img = document.createElement("img");
    img.src = reader.result;
    img.onload = function(){
      var width = img.width;
      var height = img.height;
      var canvas = document.getElementById("resizer");

      if (width > height) {
        if (width > max_width) {
          height *= max_width / width;
          width = max_width;
        }
      }else{
        if (height > max_height) {
          width *= max_height / height;
          height = max_height;
        }
      }

      canvas.width = width;
      canvas.height = height;
      var ctx = canvas.getContext("2d");
      ctx.drawImage(img, 0, 0, width, height);

      canvas.toBlob(function(blob) {
        var resizedFile = new File([blob], file.name, {type: file.type, lastModified: Date.now()});
        resizedFile["modalita"] = file["modalita"];
        logOnConsole("ridimensionato file : " + resizedFile.name + " -- " + resizedFile.size);
        if(resizedFile.size > maxFileSize){
          resizeImgFile(resizedFile, max_width - 100, max_height - 100);
        }else{
          $("#drop-zone").removeClass("onResize");
          prepareUpload(resizedFile, "resized");
        }
      }, "image/jpeg", 0.75);

    }
  };
}

function creaMiniatura_PDF(file, id){
  var reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onload = function () {
    var frame = "<img src='/sites/all/themes/snaitech/images/pdf_icon.png'>";
    var cont = $(frame).wrap("<div class='miniaturaCont'></div>").parent();
    $("#"+id).prepend(cont);
    $("#"+id).find("img").on('dragstart', function(event) { event.preventDefault(); });
  }
}

function creaMiniatura(file, id){
  var reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onload = function () {
    var img = document.createElement("img");
    img.src = reader.result;
    img.onload = function(){
      var cont = $(img).wrap("<div class='miniaturaCont' onmouseenter='apriMiniaturaPreview(this)' onmouseleave='chiudiMiniaturaPreview(this)'></div>").parent();
      $("#"+id).prepend(cont);
    }
  };

}

function dragStartFalse(){
  return false;
}


function fromFileToStringObj(file, timeStamp){

  var max_width = 1200;
  var max_height = 800;

  $("#drop-zone").addClass("onResize");
  var reader = new FileReader();
  reader.readAsDataURL(file);

  reader.onload = function () {
    var img = document.createElement("img");
    img.src = reader.result;

    img.onload = function(){
      var width = img.width;
      var height = img.height;
      var canvas = document.getElementById("resizer");

      if (width > height) {
        if (width > max_width) {
          height *= max_width / width;
          width = max_width;
        }
      }else{
        if (height > max_height) {
          width *= max_height / height;
          height = max_height;
        }
      }

      canvas.width = width;
      canvas.height = height;
      var ctx = canvas.getContext("2d");
      ctx.drawImage(img, 0, 0, width, height);
      var newImgDataUrl = canvas.toDataURL("image/png");

      var fileObj = {id:timeStamp,string:newImgDataUrl, name:file.name, modalita: file.modalita};
      setFileArray_IE(fileObj);
    }
  };
  $("#drop-zone").removeClass("onResize");
}


//mette il file in filesArray e crea una mimiatura
function setFileArray_IE(fileObj){
  if(filesArray.length < NUM_FILES){
    filesArray.push(fileObj);
    var img = "<div class='miniaturaCont' onmouseenter='apriMiniaturaPreview(this)' onmouseleave='chiudiMiniaturaPreview(this)'><img src='" + fileObj.string + "'/></div>";
    var html = "<div id='file_"+fileObj.id+"' class='list-group-item list-group-item-success'>"+ img + fileObj.name + "<span onclick='removeFilesFromArray(\"" + fileObj.id +"\", event)' class='pointer pull-right glyphicon glyphicon-trash'></span></div>";
    $("#uploadedFiles .list-group").append(html);
    scriviFilesMancanti();
  }else{
    var dialogParams = {
      idContent : "erroreVendita",
      title : "impossibile caricare piu di "+ NUM_FILES +" files",
      msg : "Il file <strong>" + fileObj.name + "</strong> non sarà caricato.",
      type : "error"
    }
    openGenericDialog(dialogParams);
  }

}



function fromEuroToCents(amount){
  amount = amount.replace(".", ",");
  if(typeof amount == "string" && amount.indexOf(",") !== -1){
    var amountSplitted = amount.split(",");
    var interi = amountSplitted[0];
    var decimali = amountSplitted[1];
    if(decimali.length === 0)
      decimali = "00";
    if(decimali.length === 1)
      decimali = decimali + "0";
    if(decimali.length > 2)
      decimali = parseFloat("0." + decimali).toFixed(2).substring(2);
    while (interi.charAt(0) === '0')
      interi = interi.substr(1);
    if(interi === "" && decimali.charAt(0) === '0')
      decimali = decimali.substr(1);
    return interi + decimali;
  }
  return (amount * 100).toString();
}
function fromCentsToEuro(amount){
  if(typeof amount == "string"){
    amount = Number(amount);
  }
  return (amount/100).toFixed(2).replace(".", ",");
}

function b64toBlob(b64Data, contentType, fileName) {
  contentType = contentType || '';
  var sliceSize = 512;
  b64Data = b64Data.replace(/^[^,]+,/, '');
  b64Data = b64Data.replace(/\s/g, '');
  var byteCharacters = window.atob(b64Data);
  var byteArrays = [];

  for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    var slice = byteCharacters.slice(offset, offset + sliceSize);

    var byteNumbers = new Array(slice.length);
    for (var i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i);
    }

    var byteArray = new Uint8Array(byteNumbers);

    byteArrays.push(byteArray);
  }

  var blob = new Blob(byteArrays, {type: contentType}, fileName);
  return blob;
}

function scriviFilesMancanti(){
  var nFilesMancanti = NUM_FILES - filesArray.length;
  if(nFilesMancanti == 0){
    $("#drop-zone").html("OK");
    $("#drop-zone").removeClass("missing-files");
    $("#msgUploadPostepay").removeClass("error");
  }else if(nFilesMancanti == NUM_FILES){
    $("#drop-zone").html("Trascina i files qui");
  }else{
    $("#drop-zone").html("Necessario Upload di <span id='nFilesMancanti'>"+ nFilesMancanti + "</span> files mancanti");
  }
}

function removeFilesFromArray(timeStamp, event){

  var fileDom = $(event.target).parents(".list-group-item");
  for(var i=0;i<filesArray.length;i++){
    if(filesArray[i].id == timeStamp){
      filesArray.splice(i, 1);
    }
  }
  $(fileDom).remove();
  scriviFilesMancanti();
}

function apriMiniaturaPreview(imgCont){
  var clone = $(imgCont).clone();
  $(clone).find("img").attr("onmouseenter", "");
  $(clone).find("img").attr("onmouseleave", "");
  var html = "<div id='miniaturaPreview'>";
  html += $(clone).html();
  html += "</div>";
  $(imgCont).append(html);
  logOnConsole(imgCont);
}

function chiudiMiniaturaPreview(){
  $("#miniaturaPreview").remove();
}


function killEnterEvent(e) {
  if (e.which === 13) {
    e.preventDefault();
    return false;
  }
  return true;
}

function isValidDate(dateString) {
  // First check for the pattern
  //var regex_date = /^\d{4}\-\d{1,2}\-\d{1,2}$/;
  var regex_date = /^\d{1,2}\-\d{1,2}\-\d{4}$/;

  if(!regex_date.test(dateString))
  {
    return false;
  }

  // Parse the date parts to integers
  var parts   = dateString.split("-");
  var day     = parseInt(parts[0], 10);
  var month   = parseInt(parts[1], 10);
  var year    = parseInt(parts[2], 10);
  //alert("day:" + day);
  //alert("MONTH:" + month);
  // Check the ranges of month and year
  if(year < 1000 || year > 3000 || month == 0 || month > 12)
  {
    return false;
  }

  var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

  // Adjust for leap years
  if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
  {
    monthLength[1] = 29;
  }

  // Check the range of the day
  return day > 0 && day <= monthLength[month - 1];
}

function validateDate(idProdotto) {
  var esito = true;
  $('#prodotto_' + idProdotto + " .validateData").each(function(index,item){
    if(item.value && !isValidDate(item.value)) {
      var el = $(".categoria-prodotto-item[id='prodotto_"+idProdotto+"']");
      $(el).find("input[name='" + item.name + "']").parents(".form-group").addClass("has-error");
      apriErrorTooltip(el, "Data errata, formato corretto: gg-mm-aaaa");
      esito = false;
    }
  });

  return esito;
}

function validateMandatoryGroupFields(idProdotto) {
  var esito  = true;
  var emptyNameFileds = [];
  if(arrayOrMandatoryValidation.includes(idProdotto)) {
    var countMandatoryGroup = 0;
    var countEmptyValueMandatoryGroup = 0;
    $('#prodotto_' + idProdotto + ' .mandatoryGroup').each(function(index,item){
      countMandatoryGroup++;
      if(item.value.trim().length > 0) {
        countEmptyValueMandatoryGroup++;
      } else {
        emptyNameFileds.push(item.placeholder||item.name);
      }
    });
    if(countMandatoryGroup > 0 && countEmptyValueMandatoryGroup == 0 ) {
      esito = false;
      var msgDialog = "Valorizzare almeno uno dei seguenti campi:";
      for(var i=0; i < emptyNameFileds.length; i++) {
        msgDialog += "<br><span style='font-weight: bold; color: #000000;'>" + emptyNameFileds[i] + "</span>";
      }

      var dialogParams = {
        idContent : "vendita-container",
        title : "Campi da valorizzare",
        msg : msgDialog,
        type : "error"};
      openGenericDialog(dialogParams);
    }
  }
  return esito;
}

function base64ToArrayBuffer(base64) {
  var binaryString = window.atob(base64);
  var binaryLen = binaryString.length;
  var bytes = new Uint8Array(binaryLen);
  for (var i = 0; i < binaryLen; i++) {
    var ascii = binaryString.charCodeAt(i);
    bytes[i] = ascii;
  }
  return bytes;
}

//save Byte[] array and download
function saveByteArray(reportName, byte) {
  var blob = new Blob([byte], {type: 'application/pdf'});
  var link = document.createElement('a');
  link.href = window.URL.createObjectURL(blob);
  var fileName = reportName + ".pdf";
  link.download = fileName;
  link.click();
}

function recuperaDocumentoCamerale(idBrand,idTerminale,idTransazione) {

  $.post( "/src/web/action/vendita/execGetDocumentoCamerale.php",{idBrand: idBrand,idTerminale: idTerminale,idTransPaymat: idTransazione}, {responseType: 'arraybuffer'})
    .success(function (data) {
      var sampleArr = base64ToArrayBuffer(data);
      saveByteArray("File",sampleArr);
    });

}

/////////////////////////////////////

function showSepafinLoginPanel() {
  isSepafinLogged = false;
  var reqUrl = "/src/web/action/vendita/getSepafinAuth.php";
  addBgFrame();
  customAjaxCall(reqUrl, Math.random(), handleSepafinPanel, null, null);
}

function handleSepafinPanel(idProdotto, parsedData, params) {
  logOnConsole("Auth Sepafin");
  logOnConsole(parsedData);
  removeBgFrame();
  if(parsedData && parsedData.dati && parsedData.dati.risultato && parsedData.dati.risultato.resultcode == 100) {
    unlockSepafinProducts();
  } else {
    $("#sepafin_login_panel").show();
    lockSepafinProducts();
  }
}

function unlockSepafinProducts() {
  isSepafinLogged = true;
  $("#sepafin_login_panel").hide();
  $(".disabled-box").each(function() {
    $(this).removeClass("disabled-box");
  });
}

function lockSepafinProducts() {
  $("div[data-codiceFamiglia=" + COD_FAM_PRODOTTI_FINANZIARI + "]").addClass("disabled-box");
}

function openSepafinAuth() {
  var html = "";
  html += "<div class='col-xs-12' id='panelTitle'>Per effettuare un pagamento &egrave; necessario autenticarsi con le credenziali Yappay/Sepafin.<br /> Ti sconsigliamo di consentire al tuo browser di salvare queste informazioni</div>";
  html += "<div class='col-xs-12' style='margin-top: 10px'>Username: <input type='text' id='userSepafin' class='form-control' /></div>";
  html += "<div class='col-xs-12' id='panelFieldPassword' style='margin-top: 10px'>Password: <input type='password' autocomplete='new-password' id='passSepafin' class='form-control' /></div>";
  html += "<div class='row text-center'>";
  html += "<div class='col-xs-12' id='panelLinkResetPassword' style='margin-top: 15px;'><a href='javascript:void(0)' onclick='resetPassword()' style='text-color:'#f76f00;'>Reset Password</a></div></div>";
  html += "<div class='col-xs-12' id='errorSepafin' style='margin-top: 20px; display:none'><div id='errorSepafinText' class='alert alert-danger'></div></div>";
  html += "<div class='col-xs-12' id='successSepafin' style='margin-top: 20px; display:none'><div id='successSepafinText' class='alert alert-success'></div></div>";

  var dialogParams = {
    idContent : "loginSepafin",
    title : "Autenticazione servizi Yappay",
    msg : html,
    contentAsRow : true,
    action : "execSepafinAuth()",
    confirmText: "Autentica",
    blockChiusuraClickEsterno: true
  }

  openGenericDialog(dialogParams)
}

function resetPassword() {
  $("#errorSepafin").hide();
  $("#errorSepafinText").html("");
  $("#successSepafin").hide();
  $("#successSepafinText").html("");
  $("#panelTitle").html("Per effettuare il reset della password Yappay/Sepafin, inserire lo username");
  $("#panelFieldPassword").css("display", "none");
  $("#panelLinkResetPassword").css("display", "none");
  $("#conferma").attr('value', 'Reset');
  $("#conferma").html('Reset');
  $("#conferma").unbind('click');
  $("#conferma").attr("onclick","execSepafinResetPassword()");
}

function execSepafinResetPassword() {
  $("#errorSepafin").hide();
  $("#errorSepafinText").html("");
  $("#successSepafin").hide();
  $("#successSepafinText").html("");
  var username = $("#userSepafin").val();
  var reqUrl = "/src/web/action/vendita/execSepafinReset.php?username=" + username;
  addBgFrame();
  customAjaxCall(reqUrl, Math.random(), onSepafinResetPasswordExecuted, null, null);
}

function onSepafinResetPasswordExecuted(idProdotto, parsedData, params) {
  logOnConsole("onSepafinResetPasswordExecuted");
  logOnConsole(parsedData);
  if (parsedData.codice == '0') {
    $("#successSepafinText").html("&Egrave; stata inviata una e-mail all'indirizzo associato all'utenza");
    $("#successSepafin").show();
    $("#conferma").hide();
  } else {
    $("#errorSepafinText").html(parsedData.descrizione||parsedData.resultDesc);
    $("#errorSepafin").show();
  }
  removeBgFrame();
}

function execSepafinAuth() {
  $("#errorSepafin").hide();
  $("#errorSepafinText").html("");
  $("#successSepafin").hide();
  $("#successSepafinText").html("");
  var username = $("#userSepafin").val();
  var password = $("#passSepafin").val();
  var reqUrl = "/src/web/action/vendita/execSepafinAuth.php?username=" + username + "&password=" + password;
  addBgFrame();
  customAjaxCall(reqUrl, Math.random(), onSepafinLoginExecuted, null, null);
}

function onSepafinLoginExecuted(idProdotto, parsedData, params) {
  logOnConsole("onSepafinLoginExecuted");
  logOnConsole(parsedData);
  if (parsedData.dati) {
    unlockSepafinProducts();
    removeCustomDialog();
  } else {
    $("#errorSepafinText").html(parsedData.descrizione);
    $("#errorSepafin").show();
  }
  removeBgFrame();
}
function limitaNumeriEnableDefaultChars(event) {
  var charCode = !event.charCode ? event.which : event.charCode;
  // abilita frecce, canc e tasto tab di rimozione carattere e tab di navigazione.
  if(charCode == 8 || charCode == 9 || charCode == 37 || charCode == 38 || charCode == 39 || charCode == 40 || charCode == 46) {
    return true;
  }
  limitaNumeri(event);
}


















